<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Phòng Khám Phụ Sản 55 Yên Lãng</title>
    <style>
      table tr td {
        border: solid 1px #CCC;
      }
    </style>
</head>
<body>
    
</body>
</html><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    @include('frontend.Link.Link')

</head>
<body>

    @include('frontend.Header.Header')
    @if($calendar->count() < 0)
        <p>No Data</p>
    @else
    <br>
    <br>
    <br>
        @include('frontend.history_modal')
        <div class="col-lg-12">
                <div class="panel panel-default">
                <div class="panel-heading">
                Danh sách lịch sử khám
                </div>
                    <div class="panel-body">     
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>Stt</th>
                                    <th>Tên Khách hàng</th>
                                    <th>Phone</th>
                                    <th>Ngày đăng kí khám</th>
                                    <th>Ca khám</th>
                                    <th>Dịch vụ</th>
                                    <th>Trạng thái</th>
                                    <th>Khởi tạo lúc</th>
                                    <th>Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($calendar as $key =>$item)
                                <tr>
                                    <td>{{$key +1}}</td>
                                    <td class="customer_name">{{$item->customer->name}}</td>
                                    <td class="customer_phone">{{$item->customer->phone}}</td>
                                    <td class="day">{{$item->schedule->day}}</td>
                                    <td class="timeclass">{{$item->time->name}}</td>
                                    <td class="service_name">{{$item->services->name}}</td>
                                    <td class="status">{{$item->status()}}</td>
                                    <td>{{$item->created_at}}</td> 
                                    <td>
                                      @if($item->status == 2)
                                        <button data-toggle="modal" data-target="#myModal"  class="btn btn-info show" id="{{$item->id}}">Xem chi tiết</button> &nbsp;
                                      @endif
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $calendar->links() }}
                    </div>
                </div>
        </div>

      <script>
        $('.show').click(function(event){
          getDataModal(this.id, $(this).parent());
        });
      </script>

    @endif 
@include('frontend.Footer.Footer')
</body>
</html>
