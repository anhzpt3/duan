<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Phòng Khám Phụ Sản 55 Yên Lãng</title>
    @include('frontend.Link.Link')
</head>
<body>
@include('frontend.Header.Header')
<div class="about-us-page">
<div class="container">
			<h3 class="introduce-title">
			<a href="javascript:void(0)" class="smooth" title="">
					<span>Về chúng tôi</span>
				</a>
			</h3>
			<div class="row row10">
				<div class="col-lg-4 col-md-4 col10">
					<div class="introduce-image hv-light">
						<img src="images/doctor/pk.jpg" title="" alt="">
					</div>
				</div>
				<div class="col-lg-8 col-md-8 col10">
					<div class="introduce-desc s-content">
						<p>
						Phòng khám Sản phụ khoa 55 Yên Lãng là một trong những phòng khám sản phụ khoa uy tín tại khu vực Hà Nội
						và được nhiều bệnh nhân biết đến. Phòng khám góp một phần không nhỏ trong việc phục vụ, chăm sóc sức khỏe cộng đồng.
						Phòng khám Sản Phụ khoa 55 Yên Lãng do Bác sĩ Tạ Như Anh thành lập vào năm 2014, đến nay đã hoạt động được hơn 4 năm. 
						Trải qua nhiều năm xây dựng và phát triển, phòng khám là nơi thăm khám của rất nhiều bệnh nhân. 
						Trong công tác khám chữa bệnh, phòng khám đạt được một số thành tựu nổi bật.
						</p>
					</div>
				</div>
			</div>
		</div>
		<br>
		<br>
		<div class="container">
			<h3 class="introduce-title">
				<a href="javascript:void(0)" class="smooth" title="">
					<span>Quá trình phát triển</span>
				</a>
			</h3>
			<div class="about-us-content s-content">
				<p>Trang giới thiệu giúp khách hàng hiểu rõ hơn về cửa hàng của bạn. Hãy cung cấp thông tin cụ thể về việc kinh doanh, về cửa hàng, thông tin liên hệ. Điều này sẽ giúp khách hàng cảm thấy tin tưởng khi mua hàng trên website của bạn.</p>
				<p>Một vài gợi ý cho nội dung trang Giới thiệu:</p>
				<p>Bạn là ai</p>
				<p>Giá trị kinh doanh của bạn là gì</p>
				<p>Địa chỉ cửa hàng</p>
				<p>Bạn đã kinh doanh trong ngành hàng này bao lâu rồi</p>
				<p>Bạn kinh doanh ngành hàng online được bao lâu</p>
				<p>Đội ngũ của bạn gồm những ai</p>
				<p>Thông tin liên hệ</p>
				<p>Liên kết đến các trang mạng xã hội (Twitter, Facebook)</p>
				<p>Bạn có thể chỉnh sửa hoặc xoá bài viết này tại đây hoặc thêm những bài viết mới trong phần quản lý Trang nội dung.</p>
			</div>
		</div>
    </div>
    <div class="history-begin about-us-page">
		<div class="container">
			<h3 class="about-us-title">Lịch sử hình thành</h3>
			<div class="tree-diagram-history">
				<ul class="tree-diagram-list">
					<li class="tree-diagram-item">
						<div class="diagram-body">
							<div class="diagram-content">
								<h4 class="diagram-content-title">2014</h4>
								<div class="s-content">
									<p>Phòng khám chính thức đi vào hoạt động, bác sĩ đứng đầu phòng khám Tạ Như Anh</p>
									<p style="color:white">Mô tả thông tin có giá trị, những điểm nổi bật, lời giới thiệu hấp dẫn về sản phẩm dịch vụ, giúp người xem dễ dàng tìm thấy nội dung của bạn trong kết quả tìm kiếm.</p>
								</div>
							</div>
						</div>
					</li>
					<li class="tree-diagram-item">
						<div class="diagram-body">
							<div class="diagram-content">
								<h4 class="diagram-content-title">2016</h4>
								<div class="s-content">
									<p>Dần dần hoàn thiện, đạt chuẩn cơ sở vật chất, nâng cao chất lượng dịch vụ</p>
									<p style="color:white">Mô tả thông tin có giá trị, những điểm nổi bật, lời giới thiệu hấp dẫn về sản phẩm dịch vụ, giúp người xem dễ dàng tìm thấy nội dung của bạn trong kết quả tìm kiếm.</p>
								</div>
							</div>
						</div>
					</li>
					<li class="tree-diagram-item">
						<div class="diagram-body">
							<div class="diagram-content">
								<h4 class="diagram-content-title">2018</h4>
								<div class="s-content">
									<p>Phát triển mạnh mẽ cả về dich vụ, chất lượng</p>
									<p style="color:white">Mô tả thông tin có giá trị, những điểm nổi bật, lời giới thiệu hấp dẫn về sản phẩm dịch vụ, giúp người xem dễ dàng tìm thấy nội dung của bạn trong kết quả tìm kiếm.</p>
								</div>
							</div>
						</div>
					</li>
					<li class="tree-diagram-item">
						<div class="diagram-body">
							<div class="diagram-content">
								<h4 class="diagram-content-title">2020</h4>
								<div class="s-content">
									<p>Trở thành 1 trong những phòng khám thai uy tín hàng đầu Hà Nội được nhiều bệnh nhân tin tưởng đến thăm khám</p>
									<p style="color:white">Mô tả thông tin có giá trị, những điểm nổi bật, lời giới thiệu hấp dẫn về sản phẩm dịch vụ, giúp người xem dễ dàng tìm thấy nội dung của bạn trong kết quả tìm kiếm.</p>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
    </div>

@include('frontend.Footer.Footer')
    
</body>
</html>