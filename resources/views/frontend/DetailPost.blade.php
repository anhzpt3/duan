<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Phòng Khám Phụ Sản 55 Yên Lãng</title>
@include('frontend.Link.Link')
</head>
<body>
@include('frontend.Header.Header')

<div class="news-detail news-page">
		<div class="container">
			<div class="row row10">
				<div class="col-lg-8 col-md-8 col10">
					<h1 style="line-height: 27px; font-family: roboto;" class="news-detail-title">	{{$detailpost->title}}</h1>
					<div class="news-views-time">
						<div class="news-views">
							<i class="fa fa-eye" aria-hidden="true"></i>
							<span>6</span>
						</div>
						<div class="news-time">
							<i class="fa fa-clock-o" aria-hidden="true"></i>
							<span>6 tháng trước</span>
						</div>
					</div>
					<div class="news-detail-content s-content">
						{{$detailpost->content}}
						<img src="{{$detailpost->feature_image}}" alt="">
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-10">
					<h2 class="lastest-news-bigtitle"><span>Tin tức mới nhất</span></h2>
					<div class="lastest-news clearfix">
					@foreach($listPost3 as $key =>$item)
						<div class="lastest-news-item">
							<div class="lastest-news-image hv-light">
								<a href="{{route('baiviet',['id' => $item->slug])}}" class="c-img" title="">
									<img src="{{$item->feature_image}}">
								</a>
							</div>
							<div class="lastest-news-info">
								<h3 class="lastest-news-title text4line">
									<a href="{{route('baiviet',['id' => $item->slug])}}" class="smooth" title="">
										{{$item->title}}
									</a>
								</h3>
							</div>
						</div>
						@endforeach
					
						
					</div>
					<!-- <div class="news-page-posters">
						<div class="poster-item">
							<a href="#" class="hv-border-inline-bg">
								<img src="images/posters/1.png">
							</a>
						</div>
						<div class="poster-item">
							<a href="#" class="hv-border-inline-bg">
								<img src="images/posters/1.png">
							</a>
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
@include('frontend.Footer.Footer')
</body>
</html>