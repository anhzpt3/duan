<div class="home-service home-introduce __web-inspector-hide-shortcut__">
		<div class="container">
			<h3 class="introduce-title">
			<a href="javascript:void(0)" class="smooth" title="">
					<span>Quý khách chọn dịch vụ theo nhu cầu để đăng ký</span>
				</a>
			</h3>
			<div class="row row10">
			<br>
				<div class="col-lg-12 col-md-8 col10">
				
					<ul class="service-list">
					@foreach($service2 as $key =>$item)
						<li class="service-item">
							<div class="service-image">
									<img src="{{$item->image}}">
							</div>
							<h3 style="text-transform: uppercase;" class="service-name">
								{{$item->name}}
							</h3>
							<p class="service-name amx amx2">
								{{$item->content}}
								<h3 style="text-transform: uppercase; color:red" class="service-name">
								<a href="{{route('datlich',['id' => $item->id])}}" class="smooth" title=""> >> Đăng ký ngay << </a>
							</h3>
							</p>
							<h3 style="text-transform: uppercase; background: #2a7f49; color: white; padding: 10px; margin-top: 10px;" class="service-name">
								Chi Phí {{ number_format($item->price)}} vnd
							</h3>
						</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>