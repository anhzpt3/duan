@include('backend.layouts.style')
<body style="background:linear-gradient(to right, rgb(218, 210, 153), rgb(176, 218, 185));" id="body" class="img3">
<div class="container">
  <div class="row" style="margin-top: 10%">
    <div class="col-md-4"></div>
          <div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h1 class="card-title text-center"><b>ĐĂNG NHẬP<B></h1>
                </div>
                <div class="card-body">
                  <form action="{{route('post.login')}}" method="post" id="formDemo" novalidate="">
                  	@csrf
                      <div class="col-md">
                          <label class="bmd-label-floating">Số điện thoại</label>
                          <input type="text" name="phone"  class="form-control" required="">
                      </div>
                      <div class="col-md">
                          <label class="bmd-label-floating">Mật khẩu</label>
                          <input type="password" name="password" class="form-control" required="" >
                      </div>
                    <Br>
                    <button style="width:100%" type="submit" class="btn btn-primary ">Đăng nhập</button>
                    <a href="{{route('home')}}" style="width:100%" type="submit" class="btn btn-danger ">Quay lại</a>
                  </form>
                </div>
              </div>
          </div>
  </div>
</div>
<div class="container">
  @include('backend.layouts.message')
</div>
</body>
@include('backend.layouts.script')
