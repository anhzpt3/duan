<div style="margin-top: 70px;" class="modal fade" id="myModal" role="dialog">
    <div style="width: 80%;" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h1 class="card-title" id="title_form" style="color: #666; font: inherit; margin: 0;">
                  Hồ sơ khám ngày : <span id="day"></span> 
                </h1>
            </div>
            <div style="line-height: 25px; padding: 15px 15%;" class="modal-body">
                <div class="content" style="padding: 0 5%; font-family: 'Times New Roman', serif; font-size: 18px; color: #000;">
                  <img src="cssfrontend/images/logo/logo.png" style="display: block; margin: 0 auto; width: 220px;" alt="">
                  <hr style="border-top: 2px solid #000; width: 80%; margin: 10px auto">
                  <h1 id="service_name" class="text-center" style="font-size: 27px;font-weight: 800;color: #000;margin: 20px 0;text-transform: uppercase;"></h1>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="row">
                        <div class="col-sm-4"><b>Họ và tên:</b></div>
                        <div class="col-sm-8" id="customer_name"></div>
                      </div>
                      <div class="row">
                        <div class="col-sm-4"><b>Điện thoại:</b></div>
                        <div class="col-sm-8" id="customer_phone"></div>
                      </div>
                      <div class="row">
                        <div class="col-sm-4"><b>Ca khám:</b></div>
                        <div class="col-sm-8" id="timeclass"></div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="row">
                        <div class="col-sm-4"><b>Giới tính:</b></div>
                        <div class="col-sm-8">Nữ</div>
                      </div>
                      <div class="row">
                        <div class="col-sm-4"><b>Trạng thái:</b></div>
                        <div class="col-sm-8" id="status"></div>
                      </div>
                    </div>
                  </div>

                  <br>
                  <table id="table_ho_so" class="table table-bordered">
                    <style>
                      #table_ho_so td {border: 2px solid #000; color: #000;}
                      #table_ho_so ul {padding: 0;}
                      #table_ho_so li {list-style: square; margin-bottom: 10px;}
                      #table_ho_so tr>td:nth-child(1) {font-weight: bold;}
                      #table_ho_so tr>td:nth-child(2) {padding: 10px 0 0 30px !important;}
                    </style>
                    <tbody>
                      <tr>
                        <td style="width: 150px;">TỬ CUNG</td>
                        <td class="tu_cung_detail"></td>
                      </tr>
                      <tr>
                        <td>THAI</td>
                        <td class="thai_detail"></td>
                      </tr>
                      <tr>
                        <td>KẾT LUẬN</td>
                        <td class="ket_luan_detail"></td>
                      </tr>
                    </tbody>
                  </table>
                  <br><br><br>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

  function getDataModal (id_, column) {
    const setInfo = info_name => {
      $('#'+info_name).text(column.siblings(`.${info_name}`).text());
    }
    setInfo('day');
    setInfo('service_name');
    setInfo('customer_name');
    setInfo('customer_phone');
    setInfo('status');
    setInfo('timeclass');

    $.ajax({
      url: '{{route('apiv2')}}',
      type: 'POST',
      data: {id: id_ , _token: '{{csrf_token()}}'},
    })
    .done(function(data){
      let ho_so = {};
      try {
        ho_so = JSON.parse(data.content);
      } catch (error) {}

      $('#table_ho_so tbody tr td:nth-child(2)').html('');
      if (!jQuery.isEmptyObject(ho_so)) {
        //tu_cung_detail
        $('.tu_cung_detail').html(`
          <ul>
            <li>
              Trong tử cung ${!ho_so.co_hinh_anh_thai ? 'không': ''} có hình ảnh thai.
            </li>
            <li id="chieu_dai_co_tu_cung">
              Chiều dài cổ tử cung: ~ ${ho_so.chieu_dai_co_tu_cung} mm.
            </li>
          </ul>
        `);

        //thai_detail
        $('.thai_detail').html(`
          <ul>
            <li id="bpd">
              BPD ~ ${ho_so.bpd} mm (Đường kính lưỡng đỉnh).
            </li>
            <li id="ac">
              AC ~ ${ho_so.ac} mm (Chu vi vòng bụng).
            </li>
            <li id="fl">
              FL ~ ${ho_so.fl} mm (Chiều dài xương đùi).
            </li>
            <li id="nhip_tim">
              Tim thai, <b>nhịp tim: ${ho_so.nhip_tim} chu kỳ / 1 phút.</b>
            </li>
            <li id="cu_dong_thai">
              Cử động thai: ${ho_so.cu_dong_thai}.
            </li>
            <li id="ngoi_thai">
              <b>Ngôi thai: ${ho_so.cu_dong_thai}.</b>
            </li>
            <li id="do_day_cua_rau">
              <b>Độ dày của rau: ${ho_so.do_day_cua_rau} mm.</b>
            </li>
            <li id="dich_oi">
              Dịch ối: ${ho_so.dich_oi}.
            </li>
            <li id="chi_so_nuoc_oi">
              Chỉ số nước ối: ${ho_so.chi_so_nuoc_oi}.
            </li>
            <li id="du_kien_can_nang">
              <b>Dự kiến cân nặng: ${ho_so.du_kien_can_nang} gr.</b>
            </li>
          </ul>
        `);
        
        //ket_luan_detail
        $('.ket_luan_detail').html(`
          <ul>
            <li id="de_nghi_sang_loc">
              Đề nghị làm sàng lọc trước sinh Triple Test (tuần 16-18)
            </li>
            <li id="ket_luan">
              ${ho_so.ket_luan}.
            </li>
          </ul>
        `);

        //remove null result
        for (var key in ho_so) {
          if (ho_so.hasOwnProperty(key)) {
            const value = ho_so[key];
            if (!value || value === "") {
              $('#' + key).remove();
            }
          }
        }
      }

      
    })
  }

</script>
