<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Phòng Khám Phụ Sản 55 Yên Lãng</title>
@include('frontend.Link.Link')
<style>
    .checkbox-inline
    {
        margin:10px 0px !important;
    }
    #xx2
    {
        display:flex;
    }
    #xx2 p
    {
        color:red;
        margin-left:5px;
    }
    #ms .alert-sucsess
    {
    width: 100% !important;
    position: fixed !important;
    z-index: 2000 !important;
    text-align: center !important;
    margin-left: 0 !important;

    }
</style>
</head>
<body>
@include('frontend.Header.Header')
<br>
<br>

@if(!Auth::check())
<div class="container">
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div style=" font-weight: bold;" class="panel-heading">
            Đặt Lịch khám cho dịch vụ : {{$data->name}} -> Chi Phí {{number_format($data->price)}} vnd
        </div>
        <!-- /.panel-heading -->
          <div class="panel-body">
              <div class="col-lg-12 ">
                <form action="{{route('datlich.post')}}" method="post" id="formDemo" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label id="xx2">Tên Khách hàng <p>*</p></label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <label id="xx2">Email <p>*</p></label>
                        <input type="email" class="form-control" name="email">
                    </div>
                    <div class="form-group">
                        <label id="xx2">Phone <p>*</p></label>
                        <input type="number" class="form-control" name="phone">
                    </div>
                    <div class="form-group">
                        <label id="xx2">Ngày Sinh <p>*</p></label>
                        <input type="date" class="form-control" name="date">
                    </div>
                    <div class="form-group">
                        <label>Số CMT</label>
                        <input type="number" class="form-control" name="cmt">
                    </div>
                    <div class="form-group" hidden>
                        <label id="xx2">Dịch vụ <p>*</p></label>
                        <input type="text" class="form-control" name="service_id" value="{{$service->id}}">
                    </div>
                    <div class="form-group">
                        <label id="xx2">Chọn ngày khám <p>*</p></label>
                        <select name="clinic_schedule_id" class="form-control" id="schedule">
                            <option value=""></option>
                            @foreach($schedule as $key => $item)
                            <option value="{{$item->id}}" name="xxx" class = "x1">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                   <!-- / đổ vòng lặp này ô -->
                    <div name="xxx" id="choose" class="form-group">
                        
                    </div>
                    <div class="form-group" >
                        <label>Ghi chú</label>
                        <textarea cols="30" rows="10" class="form-control" name="message" placeholder="Thai của bạn bao nhiêu tuần tuổi? , bạn có ghi chú gì không ạ :)"></textarea>
                    </div>
                    <!--  -->
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" ><i class="fa fa-save"></i> Đặt Lịch </button>
                        <a class="btn btn-warning" href="{{route('servicenew')}}"><i class="fa fa-reply"></i>  Quay lại </a>   
                      </div>
                      <div style="border:1px solid red; padding:8px;" class="form-group">
                        <p><b>Lưu ý : Sau khi bạn đặt lịch, chúng tôi sẽ liên hệ với bạn qua số điện thoại để xác nhận đặt lịch khám sau ít phút</b></p>
                    </div>
                </form>
              </div>
           </div>
        </div>
    </div>
</div></div>
@else
<div class="container">
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
        Đặt Lịch khám cho dịch vụ : {{$data->name}} -> Chi Phí {{number_format($data->price)}} vnd
        </div>
        <!-- /.panel-heading -->
          <div class="panel-body">
              <div class="col-lg-12 ">
                <form action="{{route('datlichUser.post')}}" method="post" id="formDemo" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group" hidden>
                        <label id="xx2">Dịch vụ *</label>
                        <input type="text" class="form-control" name="service_id" value="{{$service->id}}">
                    </div>
                    <div class="form-group">
                        <label id="xx2">Chọn ngày khám <p>*</p></label>
                        <select name="clinic_schedule_id" class="form-control" id="schedule">
                            <option value=""></option>
                            @foreach($schedule as $key => $item)
                            <option value="{{$item->id}}" class = "x1">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                   <!-- / đổ vòng lặp này ô -->
                    <div id="choose" class="form-group">
                        
                    </div>
                    <!--  -->
                    <div class="form-group" >
                        <label>Ghi chú</label>
                        <textarea cols="30" rows="10" class="form-control" name="message" placeholder="Thai của bạn bao nhiêu tuần tuổi? , bạn có ghi chú gì không ạ :)"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" ><i class="fa fa-save"></i>Đặt Lịch </button>
                        <a class="btn btn-warning" href="{{route('servicenew')}}"><i class="fa fa-reply"></i>  Quay lại </a>   
                      </div>
                      <div style="border:1px solid red; padding:8px;" class="form-group">
                        <p><b>Lưu ý : Sau khi bạn đặt lịch, chúng tôi sẽ liên hệ với bạn qua số điện thoại để xác nhận đặt lịch khám sau ít phút</b></p>
                    </div>
                </form>
              </div>
           </div>
        </div>
    </div>
</div></div>
@endif
<script src="js/jquery-3.5.1.min.js"></script>
<script> 
    $('#schedule').change(function(event) {
        var idx = this.value;
        $.ajax({
            url: '{{route('apiv1')}}',
            type: 'POST',
            data: {id: idx , _token: '{{csrf_token()}}'},
        })
        .done(function(data) {
            if(data && data.length > 0) {
                $('#choose').html('');
                data.forEach(hour => {
                    $('#choose').append(`
                        <label class="checkbox-inline">
                            <input name="time" type="radio" class="xxx" value="${hour.id}"> ${hour.name}
                        </label>
                    `);
                })
            }
            else{
                $('#choose').append(`
                       <p><b>Lịch khám trong ngày hôm nay đã được đặt hết. Quý khách vui lòng chọn ngày khác. Xin cảm ơn !</b></p>
                    `);
            }
        })
        .fail(function() {
        })
    });
</script>
@include('frontend.Footer.Footer')
</body>
</html>
