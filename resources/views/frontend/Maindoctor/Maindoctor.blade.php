<div class="home-doctor home-introduce">
		<div class="container">
			<h3 class="introduce-title">
				<a href="#" class="smooth" title="">
					<span>Bác sỹ</span>
				</a>
			</h3>
			<div class="row row10">
				<div class="col-lg-5 col-md-5 col-sm-5 col10">
					<div class="doctor-avatar hv-scale">
						<img src="{{$doctor->image}}">
					</div>
					<div class="doctor-name-box">
							<p class="doctor-name-bg">
								<span>B.S</span> {{$doctor->name}}
							</p>
					</div>
				</div>
				<div class="col-lg-7 col-md-7 col-sm-7 col10">
					<div class="doctor-main-info">
						<h4 class="doctor-education">
							Bác Sĩ
						</h4>
						<h3 class="doctor-name">
						{{$doctor->name}}
						</h3>
					</div>
					<div class="doctor-item-detail">
						<p><b>Chức vụ:</b></p>
						<p>Phụ trách phòng khám phụ sản 55 Yên Lãng</p>
					</div>
					<div class="doctor-item-detail">
						<p><b>Quá trình đào tạo:</b></p>
						<p>Năm 2000: Tốt nghiệp trường Đại học Y Hà Nội .</p>
						<p>Năm 2003: Hoàn thành khóa học chuyên khoa chẩn đoán hình ảnh .</p>
						<p>Từng công tác tại khoa siêu âm phòng khám 125 Thái Thịnh Đống Đa Hà Nội .</p>
						<p>2014: Phụ trách chính tại phòng khám phụ sản 55 Yên Lãng</p>
						<p>2014: Phụ trách chính tại phòng khám phụ sản 55 Yên Lãng</p>
					</div>
					<div class="doctor-item-detail">
						<p><b>Thế mạnh chuyên môn:</b></p>
						<p>Kinh nghiệm chuyên môn hơn 20 năm trong lĩnh vực chẩn đoán hình ảnh</p>
					</div>
				</div>
			</div>
		</div>
	</div>