<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Phòng Khám Phụ Sản 55 Yên Lãng</title>
@include('frontend.Link.Link')
</head>
<body>
@include('frontend.Header.Header')
<div class="service-page">
		<div class="container">
			<div class="service-page-desc">
				
				<div class="container">
                        <div class="col-lg-12">
                        <h1 class="service-page-title">Đổi mật khẩu</h1>
                        </div>
                       <div class="col-lg-4">
                       <form action="{{route('doimatkhau.post')}}" method="post" id="formDemo"  enctype="multipart/form-data">
                            @csrf
                            <div class="form-group" >
                                <label >Nhập mật khẩu cũ *</label>
                                <input type="password" class="form-control" name="oldpass" value="">
                            </div>
                            <div class="form-group" >
                                <label >Nhập mật mới *</label>
                                <input type="password" class="form-control" name="password" value="">
                            </div>
                            <div class="form-group" >
                                <label >Nhập mật xác nhận mật khẩu *</label>
                                <input type="password" class="form-control" name="cppassword" value="">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit" ><i class="fa fa-save"></i> Lưu </button>
                            </div>
                        <form>
                       </div>
				</div>
			</div>
		</div>
	</div>
@include('frontend.Footer.Footer')
</body>
</html>