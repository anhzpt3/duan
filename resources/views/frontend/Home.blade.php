
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Phòng Khám Phụ Sản 55 Yên Lãng</title>
    @include('frontend.Link.Link')
	<style>
		@media only screen and (min-width: 1024px){
  .amx{
	  height:180px;
	  line-height:20px !important;
	  text-align: justify !important;
      font-weight: 100 !important;
	  overflow: hidden !important;
  }
}
@media only screen and (max-width: 768px){
  .amx2{
	  display:none !important;
  }
}
.s-content
{
	text-align: justify;
}
	</style>
</head>
<body>
    @include('frontend.Header.Header')
	@include('frontend.Slide.Slide')
	
    <div class="home-introduce">
		<div class="container">
			<h3 class="introduce-title">
			<a href="javascript:void(0)" class="smooth" title="">
					<span>Giới thiệu</span>
				</a>
			</h3>
			<div class="row row10">
				<div class="col-lg-4 col-md-4 col10">
					<div class="introduce-image hv-light">
						<img src="images/doctor/pk.jpg" title="" alt="">
					</div>
				</div>
				<div class="col-lg-8 col-md-8 col10">
					<div class="introduce-desc s-content">
						<p>
						Phòng khám Sản phụ khoa 55 Yên Lãng là một trong những phòng khám sản phụ khoa uy tín tại khu vực Hà Nội
						và được nhiều bệnh nhân biết đến. Phòng khám góp một phần không nhỏ trong việc phục vụ, chăm sóc sức khỏe cộng đồng.
						Phòng khám Sản Phụ khoa 55 Yên Lãng do Bác sĩ Tạ Như Anh thành lập vào năm 2014, đến nay đã hoạt động được hơn 4 năm. 
						Trải qua nhiều năm xây dựng và phát triển, phòng khám là nơi thăm khám của rất nhiều bệnh nhân. 
						Trong công tác khám chữa bệnh, phòng khám đạt được một số thành tựu nổi bật.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
    @include('frontend.Mainservice.Mainservice')
    @include('frontend.Maindoctor.Maindoctor')
    <div class="home-news">
		<div class="container">
			<div class="row row10">
				<div class="col-lg-8 col-md-8 col10">
					<h3 class="introduce-title">
					<a href="javascript:void(0)" class="smooth" title="">
							<span>Tin tức</span>
						</a>
					</h3>
					<div class="row row10">
						<div class="col-lg-7 col-md-7 col-sm-7 col10">
							<div class="hot-news-box">
								<div class="hot-news-image">
									<a href="{{route('baiviet',['id' => $listPost2->slug])}}" class="smooth" title="">
									<img src="{{$listPost2->feature_image}}">
									</a>
								</div>
							</div>
							<div class="hot-news-detail">
								<h4 class="hot-news-title">
									<a href="{{route('baiviet',['id' => $listPost2->slug])}}" class="smooth" title="">{{$listPost2->title}}</a>
								</h4>
								<!-- <p class="hot-news-desc text3line">{{$listPost2->content}}</p> -->
							</div>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-5 col10">
							<ul class="small-news">
							@foreach($listPost as $key =>$item)
								<li class="small-news-item">
									<div class="small-new-image hot-news-image">
										<a href="{{route('baiviet',['id' => $item->slug])}}" class="smooth" title="">
										<img src="{{$item->feature_image}}">
										</a>
									</div>
									<div class="small-news-info">
										<h5 class="small-news-title">
											<a href="{{route('baiviet',['id' => $item->slug])}}" class="smooth text4line" title="">
												{{$item->title}}
											</a>
										</h5>
										<p class="small-news-desc text4line">
										<!-- {{$item->content}} -->
										</p>
									</div>
								</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col10">
					<h3 class="introduce-title">
					<a href="javascript:void(0)" class="smooth" title="">
							<span>Video</span>
						</a>
					</h3>
					<div class="news-video">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/uC957W3NKvg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>

    @include('frontend.Usercoment.Usercoment')
	@include('frontend.Footer.Footer')
	
</body>
</html>