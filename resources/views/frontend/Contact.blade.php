<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Phòng Khám Phụ Sản 55 Yên Lãng</title>
@include('frontend.Link.Link')

</head>
<body>
@include('frontend.Header.Header')
<div class="google-maps">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.3035970924802!2d105.77785499786985!3d21.020534978583704!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313454ac865806f3%3A0xe622b11fb8df6b87!2sC%C3%B4ng+ty+TNHH+KEYSKY!5e0!3m2!1svi!2s!4v1489220781420" width="100%" height="400" frameborder="0" style="border:0;pointer-events: none;vertical-align:middle;" allowfullscreen=""></iframe>
    </div>
    <div class="contact-page news-page">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<h2 class="lastest-news-bigtitle contact-title"><span>Liên hệ với chúng tôi</span></h2>
					<form class="contact-form">
						<div class="row row5">
							<div class="form-group col-lg-6 col5">
								<label for="inputName">Họ và tên</label>
								<input type="text" class="form-control" id="inputName" placeholder="Họ và tên">
							</div>
							<div class="form-group col-lg-6 col5">
								<label for="inputEmail">Email</label>
								<input type="text" class="form-control" id="inputEmail" placeholder="Email">
							</div>
						</div>
						<div class="form-group">
							<label for="textArea">Nội dung</label>
							<textarea class="form-control" id="textArea" placeholder="Nội dung tin nhắn"></textarea>
						</div>
						<div class="text-center">
							<button type="submit" class="btn btn-success smooth">Gửi</button>
						</div>
					</form>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12">
					<h2 class="lastest-news-bigtitle contact-title"><span>Phụ Sản 55 Yên Lãng</span></h2>
					<div class="contact-item clearfix">
						<span><b>Điện thoại: </b></span>
						<p>024 6666 2016</p>
					</div>
					<div class="contact-item clearfix">
						<span><b>Hotline</b></span>
						<p>024 6666 2016</p>
					</div>
					<div class="contact-item clearfix">
						<span><b>Email: </b></span>
						<p>phusan55yenlang@gmail.com.vn</p>
					</div>
					<div class="contact-item clearfix">
						<span><b>Giờ mở cửa</b></span>
						<p>Thứ 2 - Thứ 6: 8h00 - 22h00 <br>

						Thứ 7 - Chủ nhật: 8h00 - 21h00</p>
					</div>
					<div class="contact-item clearfix">
						<span><b>Địa chỉ: </b></span>
						<p>Số 9 Duy Tân - Cầu Giấy - Hà Nội</p>
					</div>
				</div>
			</div>
		</div>
	</div>
@include('frontend.Footer.Footer')
</body>
</html>