<div class="home-grateful">
		<div class="container">
			<h3 class="introduce-title">
			<a href="javascript:void(0)" class="smooth" title="">
					<span>Góc tri ân</span>
				</a>
			</h3>
			<div class="row row10 home-grateful-slider">
				<div class="col-lg-4 col-md-4 col10">
					<div class="customer-info-box">
						<div class="customer-avatar hv-light">
							<img src="cssfrontend/images/customers/1.png" alt="">
						</div>
						<div class="customer-info">
							<h5 class="customer-name">Allison Argent</h5>
							<p>Diễn viên điện ảnh</p>
							<p>24 tuổi</p>
						</div>
					</div>
					<div class="grateful-detail-box">
						<div class="grateful-detail text4line">
							hospital Health có nhiều chính sách khám chữa bệnh rất hay nên tôi rất thích. Nhà tôi mới đăng ký Thẻ khám bệnh gia đình xong, thế là cả nhà tôi sẽ đi khám miễn phí cả năm.
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col10">
					<div class="customer-info-box">
						<div class="customer-avatar hv-light">
						<img src="cssfrontend/images/customers/1.png" alt="">
						</div>
						<div class="customer-info">
							<h5 class="customer-name">Allison Argent</h5>
							<p>Diễn viên điện ảnh</p>
							<p>24 tuổi</p>
						</div>
					</div>
					<div class="grateful-detail-box">
						<div class="grateful-detail text4line">
							hospital Health có nhiều chính sách khám chữa bệnh rất hay nên tôi rất thích. Nhà tôi mới đăng ký Thẻ khám bệnh gia đình xong, thế là cả nhà tôi sẽ đi khám miễn phí cả năm.
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col10">
					<div class="customer-info-box">
						<div class="customer-avatar hv-light">
						<img src="cssfrontend/images/customers/1.png" alt="">
						</div>
						<div class="customer-info">
							<h5 class="customer-name">Allison Argent</h5>
							<p>Diễn viên điện ảnh</p>
							<p>24 tuổi</p>
						</div>
					</div>
					<div class="grateful-detail-box">
						<div class="grateful-detail text4line">
							hospital Health có nhiều chính sách khám chữa bệnh rất hay nên tôi rất thích. Nhà tôi mới đăng ký Thẻ khám bệnh gia đình xong, thế là cả nhà tôi sẽ đi khám miễn phí cả năm.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>