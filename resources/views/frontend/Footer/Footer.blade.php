<footer>
		<div class="container">
			<div class="row row10">
				<div class="col-lg-4 col-md-4 col-sm-12 col10">
					<h2 class="footer-info-title">
						Phòng Khám Phụ Sản 55 Yên Lãng
					</h2>
					<ul class="footer-info-list">
						<li class="footer-info-item">
							Thứ 2 - Thứ 6: 8h00 - 22h00
						</li>
						<li class="footer-info-item">
							Thứ 7 - Chủ nhật: 8h00 - 21h00
						</li>
						<li class="footer-info-item">
							<strong>Địa chỉ: </strong>55 Yên Lãng Hà Nội 
						</li>
						<li class="footer-info-item">
							<strong>Email: </strong><a href="#" class="smooth" title="">phusan55yenlang@gmail.com</a>
						</li>
						<li class="footer-info-item">
							<strong>Điện thoại: </strong><a href="#" class="smooth" title="">012345678</a>
						</li>
					</ul>
				</div>
				<div class="col-lg-8 col-md-8 col10">
					<div class="row row10">
						<div class="col-lg-3 col-md-4 col-sm-4 col10">
							<h5 class="footer-info-title">
								Hỗ Trợ
							</h5>
							<ul class="footer-info-list">
								<li class="footer-info-item">
									<a href="#" class="smooth" title="">Trang Chủ</a>
								</li>
								<li class="footer-info-item">
									<a href="#" class="smooth" title="">Giới Thiệu</a>
								</li>
								<li class="footer-info-item">
									<a href="#" class="smooth" title="">Chi Tiết</a>
								</li>
								<li class="footer-info-item">
									<a href="#" class="smooth" title="">Dịch Vụ</a>
								</li>
								<li class="footer-info-item">
									<a href="#" class="smooth" title="">Liên Hệ</a>
								</li>
							</ul>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col10">
							<h5 class="footer-info-title">
								Về chúng tôi
							</h5>
							<ul class="footer-info-list">
								<li class="footer-info-item">
									<a href="#" class="smooth" title="">Chính sách bảo mật</a>
								</li>
								<li class="footer-info-item">
									<a href="#" class="smooth" title="">Ưu đãi</a>
								</li>
							</ul>
						</div>
						<div class="col-lg-4 col-md-12 col-sm-12 col10">
							<h5 class="footer-info-title">
								Map
							</h5>
							<div class="footer-social-links">
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14898.042334843478!2d105.8176812!3d21.0122468!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe2520503bef90eb3!2zUGjDsm5nIEtow6FtIFPhuqNuIFBo4bul!5e0!3m2!1svi!2s!4v1607917999736!5m2!1svi!2s" width="300" height="150" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </footer>
    <script src="cssfrontend/js/jquery-2.2.1.min.js" defer></script>
	<script src="cssfrontend/js/bootstrap.min.js" defer></script>
	<script src="cssfrontend/js/slick.min.js" defer></script>
	<script src="cssfrontend/js/script.js" defer></script>
	<script src="cssfrontend/js/jquery.validate.js" defer></script>
	<script src="cssfrontend/js/myvalidate.js" defer></script>

	<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script> CKEDITOR.replace('editor1'); </script>

	