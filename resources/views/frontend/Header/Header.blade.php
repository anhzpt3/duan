<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<style type="text/css">
	.error {
	color: red;
	}
</style>
</head>
<body>
<header>
		<!-- START HEADER TOP -->
		<div class="header-top">
			<div class="container">
				<div class="header-top-row">
					<div class="row">
						<div class="col-lg-8 col-md-6">
							<ul class="header-top-list">
								<li class="header-top-item">
									<i class="fa fa-envelope"></i>
									<span>Email:</span>
									phusan55yenlang@gmail.com.vn
								</li>
								<li class="header-top-item">
									<i class="fa fa-facebook-square"></i>
									<span>Thời gian làm việc từ 8h30 đến 19h30 các ngày trong tuần</span>
								</li>
							</ul>
						</div>
						<div class="col-lg-3 col-md-6">
							<ul class="header-top-list-right">
							@if(!Auth::check())
								<li class="header-top-item relative">
									<a href="javascript:void(0)" title="">Tài khoản</a>
									<ul class="account-list">
										<li class="account-item">
											<a href="{{route('login')}}" title="">Đăng nhập</a>
										</li>
									</ul>
								</li>
							@elseif(Auth::check() && Auth::user()->role)
								<li class="header-top-item relative">
									<a href="{{route('login')}}">
												@if(Auth::user())
												@if(Auth::user()->role >= 100) Quản trị
												@endif
												@if(Auth::user()->role < 100)  Khách hàng
												@endif
												@else
												bạn hãy xem lại tài khoản của mình -_-
												@endif
												: {{Auth::user()->phone}}</a>
									<ul class="account-list">
									@if(Auth::user()->role < 100)
										<li class="account-item">
											<a href="{{route('doimatkhau')}}" title="">Đổi mật khẩu</a>
										</li>
									@else
									@endif
										<li class="account-item">
											<a href="{{route('logout')}}" title="">Đăng Xuất</a>
										</li>
									</ul>
								</li>
							@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END HEADER TOP -->

		<!-- START HEADER-MIDDLE -->
		<div class="header-middle">
			<div class="container">
				<button type="button" id="show-megamenu" data-toggle="collapse" class="navbar-toggle smooth">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</button>
				<div class="row row10">
					<div class="col-lg-3 col-md-3 col10">
						<div class="header-logo">
							<a href="#" title="" class="smooth">
								<img src="cssfrontend/images/logo/logo.png" class="" alt="">
							</a>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col10">
						<div class="header-search">
							<form action="{{route('baiviet.search')}}">
							@csrf
								<div class="header-search-ip">
									<div class="header-search-text">
										<input type="text" name="keyword" class="form-control search-feild" placeholder="Nhập từ khóa....">
									</div>
									<button class="search-submit smooth" type="submit">
										<i class="fa fa-search" aria-hidden="true"></i>
									</button>
								</div>
								
							</form>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col10">
						<div class="header-hotline">		
							<p class="header-phone-number">Hotline 0978.777.844</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END HEADER-MIDDLE -->
		<!-- START HEADER-BOTTOM -->
		<div class="header-menu">
			<div class="container">
				<nav class="main-nav">
					<span id="remove-menu"><i class="fa fa-times" aria-hidden="true"></i></span>
					<ul>
						<li class="current-active">
							<a href="{{route('home')}}" title="">Trang chủ</a>
						</li>
						<li>
							<a href="intro" title="">Giới thiệu</a>
						</li>
						<li>
							<a href="baiviet" title="">Tin tức</a>
						</li>
						@if(Auth::check() && Auth::user()->role == 1 ||  !Auth::user())
						<li>
							<a href="servicenew" title="">Đặt lịch</a>
                        </li>
						@endif
						@if(Auth::check() && Auth::user()->role == 1){
						<li>
							<a href="Historyuser" title="">Lịch sử khám</a>
                        </li>
						@endif
                        <li>
							<a href="contact" title="">Liên hệ</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END HEADER-BOTTOM -->
	</header>
	@include('backend.layouts.message')
</body>
</html>
