<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Phòng Khám Phụ Sản 55 Yên Lãng</title>
@include('frontend.Link.Link')
</head>
<body>
@include('frontend.Header.Header')
<div class="service-page">
		<div class="container">
			<div class="service-page-desc">
				<h1 class="service-page-title">Chọn Dịch Vụ Và Đặt Lịch Khám</h1>
				<div class="s-content">
					<p>Quý khách vui lòng chọn dịch vụ khám theo nhu cầu.</p>
					<p>Dịch vụ chúng tôi luôn cung cấp các dịch vụ theo từng nhu cầu khách hàng. Ngày càng nhiều dịch vụ được đưa ra, nhằm một mục đích duy nhất đó là đem lại sự hài lòng cho khách hàng - những người đã và đang cần khám chữa bệnh trong thời gian nhanh nhất.</p>
				</div>
			</div>
			<div class="row row10">
				@foreach($data as $key =>$item)
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-xxs-12 col10">
					<div class="service-item">
						<div class="service-image">
							<a href="{{route('datlich',['id' => $item->id])}}" class="smooth hv-border-inline-bg" title="">
							<img src="{{$item->image}}">
							</a>
						</div>
						<h3 class="service-title">
							<a style="text-transform: uppercase;" href="#" class="smooth" title="">
								<h3>{{$item->name}}</h3>
							</a>
						</h3>
						<h3 style="background: #2a7f49; color: white; padding: 10px; margin-top: 10px;" class="service-title">
								Chi Phí {{ number_format($item->price)}} vnd
						</h3>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
@include('frontend.Footer.Footer')
</body>
</html>