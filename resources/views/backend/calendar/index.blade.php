@extends('backend.layouts.main')
@section('content')
<style>
#this{
  display: none; width: 70% ;position: fixed; z-index: 999;
}
.style-status
{
  color:white;
  margin:0px auto;
  border-radius: 5px;
  text-align: center;
  text-transform: uppercase;
}
</style>
<h1>Danh sách lịch khám</h1> 
    <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">
               Danh sách lịch khám theo ngày
               </div>
               <!-- // -->
                  @include('frontend.history_modal')
                  <!-- / -->
                  <div class="panel-body">               
                        <div style="display: flex;"> 
                          <p>Chọn ngày &nbsp;</p>
                          <form action="">
                            <div class="form-group" style="display:flex; padding : 0 px;">
                                <input type="date" class="form-control" id="date" value="{{$day}}" name="day">
                                <button class="btn btn-primary" type="submit" ><i class="fa fa-search"></i> </button>
                            </div>
                          </form>
                        </div>
                      <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th>Stt</th>
                                <th style="width: 10%;">Tên Khách hàng</th>
                                <th>Phone</th>
                                <th style="width: 11%;">Ngày đăng kí khám</th>
                                <th>Ca khám</th>
                                <th>Dịch vụ</th>
                                <th>Trạng thái</th>
                                <th>Ghi chú</th>
                                <th>Hành động</th>
                              </tr>
                            </thead>
                            <!-- // -->
                            <tbody>
@if($calendar->count() < 0)
     <p>No Data</p>
@else
                  @foreach($calendar as $key =>$item)
                    <tr>
                        <td>{{$key +1}}</td>
                        <td class="customer_name">{{$item->customer->name}}</td>
                        <td class="customer_phone">{{$item->customer->phone}}</td>
                        <td class="day">{{$item->schedule->day}}</td>
                        <td class="timeclass">{{$item->time->name}}</td>
                        <td class="service_name">{{$item->services->name}}</td>
                        <td class="status "><p class="style-status">{{$item->status()}}</p></td>
                        <td>{!! $item->message !!}</td> 
                        <td>
                            @if($item->status == 1)
                            <a class="btn btn-info" href="{{route('histories.create',['id' => $item->id])}}"><i class="fa fa-pencil-square-o"> Thêm hồ sơ</i></a> &nbsp;
                            <a class="btn btn-danger" href="{{route('calendar.destroyv2',['id' => $item->id])}}"  onclick="return confirm('Bạn muốn xóa lịch của khách hàng: {{$item->customer->name}}')"><i class="fa fa-trash-o"></i></a>
                            @endif
                            <select class="btn btn-info showxxx" name=""  id="select">
                                <option value="0"> Lịch sử khám cũ</option>
                                @foreach($item->histories as $key =>$i)
                                <option  value="{{$i->order->id}}" class="{{$i->order->id}}">Ngày: {{$i->order->schedule->day}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
                     {{ $calendar->links() }}
                @endif 
          </div>
      </div>
  </div>
  <!-- // bảng 2 -->
  <br>
            <div class="panel panel-default">
               <div class="panel-heading">Danh sách lịch khám 7 Ngày</div>
                  <div class="panel-body">               
                      <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th></th>
                                @foreach($schedules as $key =>$i)
                                <th class="">{{$i->day}}</th>
                                @endforeach
                              </tr>
                            </thead>
                            <!-- // -->
                            <tbody>
                            
                            @foreach($timeclass as $key =>$time)
                             <!-- 1 hàng -->
                                <tr>
                                  <td>{{$time->name}}</td>
                                      @foreach($schedules as $k =>$schedule)
                                      <!-- 1 ô --> 
                                      <td>
                                        @foreach($calendar7day as $i =>$order)
                                            @if($order->time_id == $time->id && $order->clinic_schedule_id == $schedule->id)
                                              <div style="text-align: center">
                                                {{$order->customer->name}} <br> <p class="style-status">{{$order->status()}}</p>
                                              </div>
                                            @endif
                                        @endforeach
                                      </td>
                                      @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                       <!-- // -->
                </div>
            </div>
<script>
  $('.showxxx').change(function(event){
    const id = this.value;
    if (id !== '0') {
      $("#myModal").modal();
      getDataModal(id, $(this).parent());
    }
    $(this).val('0');
  });
</script>

<script>
  var today = new Date();
  if (today.getMonth() < 9) {
    var month = '0'+  (today.getMonth()+1);
  }else{
    var month = today.getMonth()+1;
  }
  if (today.getDate() < 10 ) {
    var date = '0'+ (today.getDate());
  }else{
    var date = today.getDate();
  }
  var x = today.getFullYear()+'-'+ month +'-'+ date;

  if ($('#date').val() == '') {
    $('#date').val(x);
  }
</script>
<script>
var getColor = function(text) {
  if (text === "Đã khám") return '#4BB543';
  if (text === "Chờ khám") return '#ec971f';
  if (text === "hủy") return '#c9302c';
  return "";
};

$('.style-status').each(function(i, td) {
  var color = getColor($(td).html());
  $(td).css({
    "background": color
  });
});
</script>
</div>
@endsection
