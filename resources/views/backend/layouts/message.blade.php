@if(Session::has('success'))
<div id="ms">
	<div style="width: 50%; margin-left: 10%  ;position: fixed;z-index: 2000" class="alert alert-success" role="alert">
		<strong>Success</strong>{{ session::get('success') }}
		<button type="button" id="mss" class="btn-info pull-right" style="margin-top: -10px"><i class="fa fa-trash-o"></i></button>
	</div>
</div>
@endif
@if(Session::has('successCart'))
<div id="ms">
	<div style="width: 70%; margin: auto;" class="alert alert-success" role="alert">
		<strong></strong>{{ session::get('successCart') }}	
	</div>
</div>
@endif
@if(count($errors) >0)
<div id="ms">
	<div style="width: 50%; margin-left: 10%  ;position: fixed;z-index: 2000" class="alert alert-danger" role="alert">
		<strong>Errors:</strong>
		<ul>
			@foreach($errors->all() as $e)
				<li>{{$e}}</li>
			@endforeach
		</ul>
		<button type="button" id="mss" class="btn-info pull-right" style="margin-top: -10px">
		<i class="fa fa-trash-o"></i></button>
	</div>
</div>
@endif
@if(Session::has('loginFalse'))
<div id="ms">
	<div style="width: 50%; margin: auto;" class="alert alert-warning" role="alert">
		<strong>Errors:</strong>
		{{ session::get('loginFalse') }}
		<button type="button" id="mss" class="btn-info pull-right" style="margin-top: -10px"><i class="fa fa-trash-o"></i></button>		
	</div>
</div>
@endif
@if(Session::has('erroisMess'))
<div id="ms">
	<div style="width: 50%; margin-left: 10%  ;position: fixed;z-index: 2000" class="alert alert-warning" role="alert">
		<strong>Errors:</strong>
		{{ session::get('erroisMess') }}
		<button type="button" id="mss" class="btn-info pull-right" style="margin-top: -10px"><i class="fa fa-trash-o"></i></button>		
	</div>
</div>
@endif
<script src="js/jquery-3.5.1.min.js"></script>
<script>
	$('#mss').click(function(event){
    $("#ms").attr('hidden', '');});
</script>