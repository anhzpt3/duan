<!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="{{route('order')}}">
            <i class="fa fa-th"></i> <span>Danh sách đặt lịch</span>
          </a>
        </li>
        <li>
          <a href="{{route('calendar')}}">
            <i class="fa fa-th"></i> <span>Danh sách lịch khám</span>
          </a>
        </li>
        <!-- <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('doctor')}}"><i class="fa fa-user-md"></i>Bác Sĩ</a></li>
            <li><a href="{{route('customer')}}"><i class="fa fa-user"></i> Khách hàng</a></li>
            <li><a href="{{route('service')}}"><i class="fa fa-circle-o"></i> Dịch vụ</a></li>
            <li><a href="{{route('histories')}}"><i class="fa fa-circle-o"></i> Hồ sơ</a></li>
          </ul>
        </li> -->
          
            <li><a href="{{route('doctor')}}"><i class="fa fa-user-md"></i>Bác Sĩ</a></li>
            <li><a href="{{route('customer')}}"><i class="fa fa-user"></i> Khách hàng</a></li>
            <li><a href="{{route('service')}}"><i class="fa fa-circle-o"></i> Dịch vụ</a></li>
            <li><a href="{{route('histories')}}"><i class="fa fa-circle-o"></i> Hồ sơ</a></li>
          <!-- <a href="#">
            <i class="fa fa-dashboard"></i> <span>Setting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a> -->
            <li><a href="{{route('timeclass')}}"><i class="fa fa-circle-o"></i>Ca khám</a></li>
            <li><a href="{{route('user')}}"><i class="fa fa-circle-o"></i> Tài khoản</a></li>
      
        <li>
          <a href="{{route('post')}}">
            <i class="fa fa-th"></i> <span>Bài viết</span>
          </a>
        </li>
      </ul>