@extends('backend.layouts.main')
@section('content')
<h1>Thêm Ngày khám</h1>
<hr>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
          Thêm mới
        </div>
        <!-- /.panel-heading -->
          <div class="panel-body">
              <div class="col-lg-12 ">
                <form action="{{route('clinicschedules.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Ngày</label>
                        <input type="date" class="form-control" name="day">
                    </div>

                    @foreach($timeclass as $key => $item)
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="{{$item->id}}" value="1">
                        <label class="form-check-label" >{{$item->name}}</label>
                    </div>
                    @endforeach
                    <input type="button" id="checkall" value="Check All">
                    <input type="button" id="uncheck" value="UnCheck">
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" ><i class="fa fa-save"></i> Thêm mới </button>
                        <a class="btn btn-warning" href="{{route('timeclass')}}"><i class="fa fa-reply"></i>  Quay lại </a>   
                      </div>
                </form>
              </div>
           </div>
        </div>
    </div>

</div>
<script>
$('#checkall:button').click(function () {
    $('input:checkbox').attr('checked','checked');
});
$('#uncheck:button').click(function () {
    $('input:checkbox').removeAttr('checked');
});
</script>
@endsection