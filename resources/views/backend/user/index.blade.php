@extends('backend.layouts.main')
@section('content')
<h1>Danh sách tài khoản</h1>
<hr>
  <div class="col-lg-12 ui-12-search">
    <div class="panel panel-default">
        <div class="panel-heading">
          Tìm kiểm
        </div>
          <div class="panel-body">
              <div class="row">
                    <form action="">
                    @csrf
                      <div class="col-lg-8" style="display:flex;">
                          <label> Nhập số đt hoặc email</label>
                          <input type="text" name="keyword" class="form-control">
                      </div>
                      <div class="col-md-4">
                        <button class="btn btn-primary" type="submit" ><i class="fa fa-search"></i> Tìm kiếm </button>
                        <a href="{{route('user.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>  Thêm mới (admin) </a>
                      </div>
                    </form>
              </div>   
            </div>
        </div>
      </div>
@if($users->count() == 0 or $users == null)
     <p>No Data</p>
@else
    <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">
                Danh sách tài khoản
               </div>
                  <div class="panel-body">
                      <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th>stt</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Quyền hạn</th>
                                <th>option</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($users as $key =>$item)
                            <tr>
                                <td>{{$key +1}}</td>
                                <td>{{$item->phone}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->role}}</td>
                                <td></td>
                            </tr>
                               @endforeach
                            </tbody>
                        </table>
                        {{ $users->links() }}
                </div>
            </div>
    </div>
@endif 
</div>
@endsection