@extends('backend.layouts.main')
@section('content')
  <div class="col-lg-12 ui-12-search">
    <div class="panel panel-default">
        <div class="panel-heading">
          Tìm kiểm
        </div>
          <div class="panel-body">
              <div class="row">
                    <form action="">
                    @csrf
                      <div class="col-lg-10" style="display:flex;">
                          <input type="text" name="keyword" class="form-control" placeholder="Nhập từ khóa ...">
                      </div>
                      <div class="col-md-2">
                        <button class="btn btn-primary" type="submit" ><i class="fa fa-search"></i> Tìm kiếm </button>
                      </div>
                    </form>
              </div>   
            </div>
        </div>
      </div>
@if($listPost->count() == 0 or $listPost == null)
     <p>No Data</p>
@else
    <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">
                    <h3>Danh sách bài viết</h3>
               </div>
                  <div class="panel-body">
                        <!-- <label style="display: flex;"> 
                          <p>Show &nbsp;</p>
                          <form action="">
                            <select with="150px" name="showMore" onchange="this.form.submit()">
                              <option value='{{$listPost->count()}}'>{{$listPost->count()}}</option>
                              <option value='10'>10</option>
                              <option value='25'>25</option>
                              <option value='50'>50</option>
                              <option value='100'>100</option>
                            </select>
                          </form>
                        </label> -->
                      <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th>STT</th>
                                <th>Ảnh</th>
                                <th>Tên bài viết</th>
                                <th>Mô tả ngắn</th>
                                <th>Chi tiết</th>
                                <th style="text-align:center"><a href="{{route('post.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm mới </a></th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($listPost as $key =>$item)
                            <tr>
                                <td>{{$key +1}}</td>
                                <td><img src="{{$item->feature_image}}" width="150" ></td>
                                <td width="200">{{$item->title}} </td>
                                <td width="200">{{$item->slug}}</td>
                                <td >{{$item->content}}</td>
                                <td width="150px">
                                <a class="btn btn-info" href="{{route('post.edit',['id' => $item->id])}}"><i class="fa fa-gears"></i>Sửa</a> &nbsp;
                                <a class="btn btn-danger" href="{{route('post.destroy',['id' => $item->id])}}"  onclick="return confirm('Bạn có chắc muốn xóa bài viết không')"><i class="fa fa-trash-o"></i>Xóa</a>
                                </td>
                            </tr>
                               @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
    </div>
@endif 
</div>
@endsection