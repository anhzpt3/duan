@extends('backend.layouts.main')
@section('content')
<h1>Thêm mới bài viết</h1>
<hr>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
          Thêm mới
        </div>
        <!-- /.panel-heading -->
          <div class="panel-body">
              <div class="col-lg-12 ">
                <form action="{{route('post.store')}}" method="post" id="demoForm" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Tiêu đề</label>
                        <input type="text" class="form-control" name="title">
                    </div>
                    <div class="form-group">
                        <label>Đường dẫn</label>
                        <input type="text" class="form-control" name="slug">
                    </div>
                    <div class="form-group">
                        <label>Ảnh</label>
                        <input type="file" class="form-control" name="feature_image">
                    </div>
                    <div class="form-group">
                        <label>Nội dung</label>
                        <textarea name="content" class="form-control"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" value="Register" ><i class="fa fa-save"></i> Thêm mới </button>
                        <a class="btn btn-warning" href="{{route('post')}}"><i class="fa fa-reply"></i>  Quay lại </a>   
                      </div>
                </form>
              </div>
           </div>
        </div>
    </div>

</div>
@endsection
