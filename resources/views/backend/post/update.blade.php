@extends('backend.layouts.main')
@section('content')
<h1>Sửa bài viết</h1>
<hr>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
          Chỉnh sửa
        </div>
        <!-- /.panel-heading -->
          <div class="panel-body">
              <div class="col-lg-12 ">
                <form action="{{route('post.update',['id' => $model->id])}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Tên bài viết</label>
                        <input type="text" class="form-control" name="title" value="{{old('title',$model->title)}}">
                    </div>
                    <div class="form-group">
                        <label>Đường dẫn</label>
                        <input type="text" class="form-control" name="slug" value="{{old('slug',$model->slug)}}">
                    </div>
                    <div class="form-group">
                        <label>Chi tiết</label>
                        <textarea cols="30" rows="10" class="form-control" name="content" id="editor1">{{old('content',$model->content)}}</textarea>
                    </div>
                    <div class="form-group">
                        <img src="{{$model->feature_image}}" width="200" >
                    </div>
                    <div class="form-group">
                        <label>Ảnh</label>
                        <input type="file" class="form-control" name="feature_image"> 
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" ><i class="fa fa-save"></i> Lưu </button>
                        <a class="btn btn-warning" href="{{route('post')}}"><i class="fa fa-reply"></i>  Quay lại </a>   
                      </div>
                </form>
              </div>
           </div>
        </div>
    </div>

</div>
@endsection