@extends('backend.layouts.main')
@section('content')
<h1>Thêm khách hàng</h1>
<hr>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
          Thêm mới
        </div>
        <!-- /.panel-heading -->
          <div class="panel-body">
              <div class="col-lg-12 ">
                <form action="{{route('customer.store')}}" method="post" id="formDemo" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Tên Khách hàng</label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email">
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="number" class="form-control" name="phone">
                    </div>
                    <div class="form-group">
                        <label>Ngày Sinh</label>
                        <input type="date" class="form-control" name="date">
                    </div>
                    <div class="form-group">
                        <label>Số CMT</label>
                        <input type="number" class="form-control" name="cmt">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" ><i class="fa fa-save"></i> Thêm mới </button>
                        <a class="btn btn-warning" href="{{route('customer')}}"><i class="fa fa-reply"></i>  Quay lại </a>   
                      </div>
                </form>
              </div>
           </div>
        </div>
    </div>

</div>
@endsection