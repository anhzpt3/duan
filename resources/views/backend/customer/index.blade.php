@extends('backend.layouts.main')
@section('content')
<h1>Danh sách khách hàng</h1>
<hr>
  <div class="col-lg-12 ui-12-search">
    <div class="panel panel-default">
        <div class="panel-heading">
          Tìm kiểm
        </div>
          <div class="panel-body">
              <div class="row">
                    <form action="">
                    @csrf
                      <div class="col-lg-8" style="display:flex;">
                          <label> Nhập số đt hoặc email</label>
                          <input type="text" name="keyword" class="form-control">
                      </div>
                      <div class="col-md-4">
                        <button class="btn btn-primary" type="submit" ><i class="fa fa-search"></i> Tìm kiếm </button>
                        <a href="{{route('customer.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>  Thêm mới </a>
                      </div>
                    </form>
              </div>   
            </div>
        </div>
      </div>
@if($customers->count() == 0 or $customers == null)
     <p>No Data</p>
@else
    <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">
                Danh sách tài khoản
               </div>
                  <div class="panel-body">
                        <label style=""> 
                          <p>Show &nbsp;</p>
                          <form action="">
                            <select with="50px" name="showMore" onchange="this.form.submit()">
                              <option value='{{$customers->count()}}'>{{$customers->count()}}</option>
                              <option value='10'>10</option>
                              <option value='25'>25</option>
                              <option value='50'>50</option>
                              <option value='100'>100</option>
                            </select>
                          </form>
                          <p> &nbsp;&nbsp;trên tổng : {{$total}} khách hàng</p> 
                        </label>
                      <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th>stt</th>
                                <th>Tên khách hàng</th>
                                <th>Email</th>
                                <th>Số ĐT</th>
                                <th>Ngày sinh</th>
                                <th>Option</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($customers as $key =>$item)
                            <tr>
                                <td>{{$key +1}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->phone}}</td>
                                <td>{{$item->date}}</td>
                                <td></td>
                            </tr>
                               @endforeach
                            </tbody>
                        </table>
                        {{ $customers->links() }}
                </div>
            </div>
    </div>
@endif 
</div>
@endsection