@extends('backend.layouts.main')
@section('content')

    <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">
                <h2>
        <!-- Lịch trực tại phòng khám bệnh của bác sĩ  -->
                Danh sách ca khám theo ngày 
                </h2>
                    <form action="">
                            <select with="50px" name="keyword" onchange="this.form.submit()">
                              <option value='7'>7 ngày tới</option>
                              <option value='1'>hôm nay</option>
                              <option value='3'>3 ngày tới</option>
                              <option value='7'>7 ngày tới</option>
                              <option value='15'>15 ngày tới</option>
                              <option value='30'>30 ngày tới</option>
                            </select>
                      </form>
                      <a href="{{route('clinicschedules.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>  Thêm ngày khám </a>
               </div>
                  <div class="panel-body">
                      <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th>stt</th>
                                <th>Ngày</th>
                                <th>Số ca khám đã tạo</th>
                                <th>Chi tiết</th>
                                <th>Các ca khám đã được đặt lịch</th>
                                <th style="text-align:Center">Hành động</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($clinicSchedule as $key =>$item)
                            <tr>
                                <td>{{$key +1}}</td>
                                <td style="width:150px">{{$item->day}}</td>
                                <td  style="width:150px">{{$item->count}}</td>
                                <td >{{$item->time_name}}</td>
                                <td>{{$item->booking}}</td>
                                <td style="width:200px; text-align:center">
                                <!-- <a class="btn btn-info" href=""><i class="fa fa-gears"></i></a> &nbsp; -->
                                <a class="btn btn-danger" href=""  onclick="return confirm('Bạn có chắc không')"><i class="fa fa-trash-o"></i> Xóa</a>
                                </td>
                            </tr>
                               @endforeach
                            </tbody>
                        </table>
                        {{ $clinicSchedule->links() }}
                </div>
            </div>
    </div>

<hr>
@if($timeclass->count() == 0 or $timeclass == null)
     <p>No Data</p>
@else
    <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">
                Danh sách ca khám
               </div>
                  <div class="panel-body">
                      <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th>Stt</th>
                                <th>Tên</th>
                                <th>Giờ bắt đầu</th>
                                <th>Giờ kết thúc</th>
                                <th>Ghi chú</th>
                                <th>Hành động</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($timeclass as $key =>$item)
                            <tr>
                                <td>{{$key +1}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->time_start}}</td>
                                <td>{{$item->time_end}}</td>
                                <td>{{$item->message}}</td>
                                <td>
                                <!-- <a class="btn btn-info" href=""><i class="fa fa-gears"></i></a> &nbsp; -->
                                <a class="btn btn-danger" href=""  onclick="return confirm('Bạn có chắc không')"><i class="fa fa-trash-o"></i> Xóa</a>
                                </td>
                            </tr>
                               @endforeach
                            </tbody>
                        </table>
                        {{ $timeclass->links() }}
                </div>
            </div>
    </div>
@endif 
</div>
@endsection