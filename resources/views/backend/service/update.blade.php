@extends('backend.layouts.main')
@section('content')
<h1>Sửa dịch vụ</h1>
<hr>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
          Chỉnh sửa
        </div>
        <!-- /.panel-heading -->
          <div class="panel-body">
              <div class="col-lg-12 ">
                <form action="{{route('service.update',['id' => $model->id])}}" method="post" id="formDemo" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Tên dịch vụ</label>
                        <input type="text" class="form-control" name="name" value="{{old('name',$model->name)}}">
                    </div>
                    <div class="form-group">
                        <label>Giá</label>
                        <input type="number" class="form-control" name="price" value="{{old('name',$model->price)}}">
                    </div>
                    <div class="form-group">
                        <label>Mô tả ngắn</label>
                        <input type="text" class="form-control" name="short_desc" value="{{old('name',$model->short_desc)}}">
                    </div>
                    
                    <div class="form-group">
                        <label>Chi tiết</label>
                        <textarea cols="30" rows="5" class="form-control" name="content">{{old('content',$model->content)}}</textarea>
                    </div>
                    <div class="form-group">
                        <img src="{{$model->image}}" width="100" >
                    </div>
                    <div class="form-group">
                        <label>Ảnh</label>
                        <input type="file" class="form-control" name="image"> 
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" ><i class="fa fa-save"></i> Lưu </button>
                        <a class="btn btn-warning" href="{{route('service')}}"><i class="fa fa-reply"></i>  Quay lại </a>   
                      </div>
                </form>
              </div>
           </div>
        </div>
    </div>

</div>
@endsection