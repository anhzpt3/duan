@extends('backend.layouts.main')
@section('content')
<h1>Danh sách dịch vụ</h1>
<hr>
  <div class="col-lg-12 ui-12-search">
    <div class="panel panel-default">
        <div class="panel-heading">
          Tìm kiểm
        </div>
          <div class="panel-body">
              <div class="row">
                    <form action="">
                    @csrf
                      <div class="col-lg-8" style="display:flex;">
                          <label> Tên dịch vụ</label>
                          <input type="text" name="keyword" class="form-control">
                      </div>
                      <div class="col-md-4">
                        <button class="btn btn-primary" type="submit" ><i class="fa fa-search"></i> Tìm kiếm </button>
                       
                      </div>
                    </form>
              </div>   
            </div>
        </div>
      </div>
@if($services->count() == 0 or $services == null)
     <p>No Data</p>
@else
    <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">
                Danh sách dịch vụ
               </div>
                  <div class="panel-body">
                        <label style=""> 
                          <p>Show &nbsp;</p>
                          <form action="">
                            <select with="50px" name="showMore" onchange="this.form.submit()">
                              <option value='{{$services->count()}}'>{{$services->count()}}</option>
                              <option value='10'>10</option>
                              <option value='25'>25</option>
                              <option value='50'>50</option>
                              <option value='100'>100</option>
                            </select>
                          </form>
                          <p> &nbsp;&nbsp;trên tổng : {{$total}} dịch vụ</p> 
                        </label>
                      <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th>Stt</th>
                                <th>Ảnh</th>
                                <th>Tên dịch vụ</th>
                                <th>Giá</th>
                                <th>Mô tả ngắn</th>
                                <th>Chi tiết</th>
                                <th style="Text-align:center"> <a href="{{route('service.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>   Thêm mới </a></th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($services as $key =>$item)
                            <tr>
                                <td width="50px">{{$key +1}}</td>
                                <td width="200px"><img src="{{$item->image}}" width="50" ></td>
                                <td width="150px">{{$item->name}}</td>
                                <td>{{$item->price}}</td>
                                <td width="150px">{{$item->short_desc}}</td>
                                <td width="250px">{{$item->content}}</td>
                                <td style="text-align:center" width="200px">
                                
                                <a class="btn btn-info" href="{{route('service.edit',['id' => $item->id])}}"><i class="fa fa-gears"></i> Sửa</a> &nbsp;
                                <a class="btn btn-danger" href="{{route('service.destroy',['id' => $item->id])}}"  onclick="return confirm('Bạn có chắc muốn xóa dịch vụ: {{$item->name}} không ?')">
                                <i class="fa fa-trash-o"></i> Xóa</a>
                                </td>
                            </tr>
                               @endforeach
                            </tbody>
                        </table>
                        {{ $services->links() }}
                </div>
            </div>
    </div>
@endif 
</div>
@endsection