@extends('backend.layouts.main')
@section('content')
<h1>Danh sách Hồ sơ</h1>
<hr>
  <div class="col-lg-12 ui-12-search">
    <div class="panel panel-default">
        <div class="panel-heading">
          Tìm kiểm
        </div>
          <div class="panel-body">
              <div class="row">
                    <form action="">
                    @csrf
                      <div class="col-lg-8" style="display:flex;">
                          <label> Nhập số đt hoặc tên</label>
                          <input type="text" name="keyword" class="form-control">
                      </div>
                      <div class="col-md-4">
                        <button class="btn btn-primary" type="submit" ><i class="fa fa-search"></i> Tìm kiếm </button>
                        <!-- <a href="{{route('customer.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>  Thêm mới </a> -->
                      </div>
                    </form>
              </div>   
            </div>
        </div>
      </div>
@if($histories->count() == 0 )
     <p>No Data</p>
@else
    <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">
                Danh sách Hồ sơ
               </div>
                  <div class="panel-body">
                        <label style=""> 
                          <p>Show &nbsp;</p>
                          <form action="">
                            <select with="50px" name="showMore" onchange="this.form.submit()">
                              <option value='{{$histories->count()}}'>{{$histories->count()}}</option>
                              <option value='10'>10</option>
                              <option value='25'>25</option>
                              <option value='50'>50</option>
                              <option value='100'>100</option>
                            </select>
                          </form>
                          <p> &nbsp;&nbsp;trên tổng : {{$total}} Hồ sơ</p> 
                        </label>
                      <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th>stt</th>
                                <th>Tên khách hàng</th>
                                <th>Số điện thoại</th>
                                <th>Dịch vụ</th>
                                <th>Ngày khám</th>
                                <th>Option</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($histories as $key =>$item)
                            <tr>
                                <td>{{$key +1}}</td>
                                <td>{{$item->customer->name}}</td>
                                <td>{{$item->customer->phone}}</td>
                                <td>{{$item->order->services->name}}</td>
                                <td>{{$item->order->schedule->day}}</td>
                                <td>
                                  <a class="btn btn-info" href="{{route('histories.edit',['id' => $item->id])}}"><i class="fa fa-gears"></i> Chi tiết và Sửa</a> &nbsp;
                                  
                                </td>
                            </tr>
                               @endforeach
                            </tbody>
                        </table>
                        {{ $histories->links() }}
                </div>
            </div>
    </div>
@endif 
</div>
@endsection