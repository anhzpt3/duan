@extends('backend.layouts.main')
@section('content')
<h1>Thêm hồ sơ </h1>
<hr>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
          Thêm mới hồ sơ của khách hàng
        </div>
        <!-- /.panel-heading -->
          <div class="panel-body">
              <div class="col-lg-12 ">
                <h4><b>Họ và tên: </b> {{$customer->name}}</h4>
                <h4><b>Số điện thoại: </b> {{$customer->phone}}</h4>
                <hr>
                <form id="form_ho_so" action="{{route('histories.store',['id' => $model->id])}}" method="post" id="formDemo" enctype="multipart/form-data">
                  @csrf

                  <div hidden>
                    <div class="form-group">
                        <label>customer_id</label>
                        <input type="text" class="form-control" name="customer_id"  value="{{old('customer_id',$model->customer_id)}}">
                    </div>
                    <div class="form-group">
                        <label>order_list_id</label>
                        <input type="text" class="form-control" name="order_list_id"  value="{{old('order_list_id',$model->id)}}">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-6">
                      <h3>Siêu âm thai 4D</h3>
                      <br>

                      <div class="form-group form-check">
                        <input name="co_hinh_anh_thai" type="checkbox" value="on" class="form-check-input" id="co_hinh_anh_thai">
                        <label class="form-check-label" for="co_hinh_anh_thai" style="user-select: none;">
                          Trong tử cung có hình ảnh thai
                        </label>
                      </div>
                      <div class="form-group">
                        <label for="chieu_dai_co_tu_cung" class="col-form-label">Chiều dài cổ tử cung: ~</label>&nbsp;
                        <input name="chieu_dai_co_tu_cung" type="number" class="form-control-plaintext" min="0" id="chieu_dai_co_tu_cung" style="width: 70px;">
                        <span class="unit">mm</span>
                      </div>

                      <div class="form-group">
                        <label class="col-form-label"><h4><b>Kết luận:</b></h4> </label>&nbsp;
                        <textarea name="ket_luan" type="text" class="form-control" rows="8" style="width: 70%; resize: none; font-weight:600; "></textarea>
                      </div>
                      <br>
                      <div class="form-group form-check">
                        <input name="de_nghi_sang_loc" type="checkbox" class="form-check-input" id="de_nghi_sang_loc">
                        <label class="form-check-label" for="de_nghi_sang_loc" style="user-select: none;">
                          Đề nghị làm sàng lọc trước sinh Triple Test (tuần 16-18)
                        </label>
                      </div>
                      <!-- <div class="form-group form-check">
                        <input name="hen_kiem_tra_lai" type="checkbox" class="form-check-input" id="hen_kiem_tra_lai">
                        <label class="form-check-label" for="hen_kiem_tra_lai" style="user-select: none;">
                          Hẹn kiểm tra lại
                        </label>
                      </div> -->

                      <div class="form-group">
                        <label>Chọn ngày tái khám</label>
                        <select name="clinic_schedule_id" class="form-control" id="schedule" style="width: 70%;">
                            <option value="0">- Trống -</option>
                            @foreach($schedule as $key => $item)
                            <option value="{{$item->id}}" class = "x1">{{$item->name}}</option>
                            @endforeach
                        </select>
                      </div>
                      <div id="choose" class="form-group" style="width: 70%;">
                          <!-- đổ ca khám ra đây -->
                      </div>

                    </div>

                    <div id="thong_tin_thai" class="col-lg-6">
                      <h3>Thông tin thai</h3>
                      <br>
                      <div class="form-group">
                        <label class="col-form-label">BPD: ~</label>&nbsp;
                        <input name="bpd" type="number" class="form-control-plaintext" min="0" style="width: 70px;">
                        <span class="unit">mm (Đường kính lưỡng đỉnh)</span>
                      </div>
                      <div class="form-group">
                        <label class="col-form-label">AC: ~</label>&nbsp;
                        <input name="ac" type="number" class="form-control-plaintext" min="0" style="width: 70px;">
                        <span class="unit">mm (Chu vi vòng bụng)</span>
                      </div>
                      <div class="form-group">
                        <label class="col-form-label">FL: ~</label>&nbsp;
                        <input name="fl" type="number" class="form-control-plaintext" min="0" style="width: 70px;">
                        <span class="unit">mm (Chiều dài xương đùi)</span>
                      </div>
                      <div class="form-group">
                        <label class="col-form-label">Tim thai, nhịp tim: ~</label>&nbsp;
                        <input name="nhip_tim" type="number" class="form-control-plaintext" min="0" style="width: 70px;">
                        <span class="unit">chu kỳ / 1phút</span></span>
                      </div>
                      <div class="form-group">
                        <label class="col-form-label">Cử động thai: </label>&nbsp;
                        <select name="cu_dong_thai" class="form-control-plaintext" style="width: 110px;">
                          <option value="" default></option>
                          <option value="Tốt" default>Tốt</option>
                          <option value="Bình thường">Bình thường</option>
                          <option value="Yếu">Yếu</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label class="col-form-label">Ngôi thai: </label>&nbsp;
                        <select name="ngoi_thai" class="form-control-plaintext" style="width: 110px;">
                          <option value="" default></option>
                          <option value="Ổn định" default>Ổn định</option>
                          <option value="Chưa ổn định">Chưa ổn định</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label class="col-form-label">Độ dày của rau: </label>&nbsp;&nbsp;
                        <input name="do_day_cua_rau" type="number" class="form-control-plaintext" min="0" style="width: 70px;">
                        <span class="unit">mm</span>
                      </div>
                      <div class="form-group">
                        <label class="col-form-label">Dịch ối: </label>&nbsp;
                        <select name="dich_oi" class="form-control-plaintext" style="width: 110px;">
                          <option value="" default></option>
                          <option value="Tốt" default>Tốt</option>
                          <option value="Bình thường">Bình thường</option>
                          <option value="Yếu">Yếu</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label class="col-form-label">Chỉ số nước ối: </label>&nbsp;
                        <select name="chi_so_nuoc_oi" class="form-control-plaintext" style="width: 110px;">
                          <option value="" default></option>
                          <option value="Tốt" default>Tốt</option>
                          <option value="Bình thường">Bình thường</option>
                          <option value="Yếu">Yếu</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label class="col-form-label">Dự kiến cân nặng: </label>&nbsp;&nbsp;
                        <input name="du_kien_can_nang" type="number" class="form-control-plaintext" min="0" style="width: 70px;">
                        <span class="unit">gr</span>
                      </div>
                    </div>
                  </div>

                  <input type="hidden" name="content" id="content" value="">

                  <br>
                  <button id="them_ho_so" type="submit" class="btn btn-primary margin-r-5" style="min-width: 100px;">
                    Thêm hồ sơ
                  </button>
                  <a class="btn btn-warning" href="{{route('calendar')}}" style="min-width: 100px;">
                    <i class="fa fa-reply"></i>  Quay lại 
                  </a>
                </form>

              </div>
           </div>
        </div>
    </div>
    <script>
      $('#schedule').change(function(event) {
          var idx = this.value;
          $('#choose').html('');
          if (idx !== 0) {
            $.ajax({
              url: '{{route('apiv1')}}',
              type: 'POST',
              data: {id: idx , _token: '{{csrf_token()}}'},
            })
            .done(function(data) {
                if(data && data.length > 0) {
                    data.forEach((hour, index) => {
                        $('#choose').append(`
                            <label class="checkbox-inline" style="${index===0?'margin-left:10px;':''}">
                                <input name="time" type="radio" class="xxx" value="${hour.id}"> ${hour.name}
                            </label>
                        `);
                    })
                }
            })
            .fail(function() {
            })
          }
      });

      $('#them_ho_so').click(e => {
        e.preventDefault();
        let ho_so = {};

        $("#form_ho_so input, #form_ho_so textarea, #form_ho_so select").each(function(){
          const item = $(this);
          const name = item.attr('name');
          if (item.is(':checkbox')) {
            ho_so[name] = item.prop('checked');
          } else {
            ho_so[name] = item.val();
          }
        });

        delete ho_so._token;
        delete ho_so.content;
        delete ho_so.clinic_schedule_id;
        $('#content').val(JSON.stringify(ho_so));
        $('#form_ho_so').submit();
      })
    </script>
</div>
@endsection
