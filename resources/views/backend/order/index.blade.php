@extends('backend.layouts.main')
@section('content')
<h1>Danh sách đặt lịch</h1>
<hr>
  <div class="col-lg-12 ui-12-search">
    <div class="panel panel-default">
        <div class="panel-heading">
          Tìm kiểm
        </div>
          <div class="panel-body">
              <div class="row">
                    <form action="">
                    @csrf
                      <div class="col-lg-8" style="display:flex;">
                          <label> Nhập tên, số điện thoại khách hàng</label>
                          <input type="text" name="keyword" class="form-control">
                      </div>
                      <div class="col-md-4">
                        <button class="btn btn-primary" type="submit" ><i class="fa fa-search"></i> Tìm kiếm </button>
                      </div>
                    </form>
              </div>   
            </div>
        </div>
      </div>
@if(!$orders)
     <p>No Data</p>
@else
    <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">
                Danh sách đặt lịch
               </div>
                  <div class="panel-body">
                          <p>Tổng : {{$total}} khách hàng đặt lịch chưa xác nhận</p> 
                        <div style=""> 
                          <p>Hiển thị đặt lịch trong vòng &nbsp;</p>
                          <form action="">
                            <select with="50px" name="day" onchange="this.form.submit()">
                              <option value='7'>7 ngày tới</option>
                              <option value='0'>Hôm nay</option>
                              <option value='3'>3 ngày tới</option>
                              <option value='15'>15 ngày tới</option>
                              <option value='30'>30 ngày</option>
                            </select>
                          </form>
                        </div>
                      <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th>Stt</th>
                                <th>Tên Khách hàng</th>
                                <th>Phone</th>
                                <th>Ngày đăng kí khám</th>
                                <th>Ca khám</th>
                                <th>Dịch vụ</th>
                                <th>Trạng thái</th>
                                <th>Khởi tạo lúc</th>
                                <th><a href="{{route('order.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>   Thêm mới </a></th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($orders as $key =>$item)
                            <tr>
                                <td>{{$key +1}}</td>
                                <td>{{$item->customer->name}}</td>
                                <td>{{$item->customer->phone}}</td>
                                <td>{{$item->schedule->day}}</td>
                                <td>{{$item->time->name}}</td>
                                <td>{{$item->services->name}}</td>
                                <td><p style="background: #ec971f; text-align: center; border-radius:5px; color:white">{{$item->status()}}</p></td>
                                <td>{{$item->created_at}}</td> 
                                <td>
                                <a class="btn btn-info" href="{{route('calendar.status',['id' => $item->id])}}" onclick="return confirm('Bạn có chắc xác nhận cho khách hàng : {{$item->customer->name}}')"><i class="fa fa-pencil-square-o"></i> Xác nhận đặt lịch</a> &nbsp;
                                <a class="btn btn-danger" href="{{route('calendar.destroy',['id' => $item->id])}}"  onclick="return confirm('Bạn muốn xóa lịch của khách hàng: {{$item->customer->name}}')"><i class="fa fa-trash-o"></i> Xóa</a>
                                </td>
                            </tr>
                               @endforeach
                            </tbody>
                        </table>
                        {{ $orders->links() }}
                </div>
            </div>
    </div>
@endif 
</div>
@endsection