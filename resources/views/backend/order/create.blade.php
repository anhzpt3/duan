@extends('backend.layouts.main')
@section('content')
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <select id="changer" class="form-control">
                <option value="0">-Thêm Lịch Khám-</option>
                <option value="1">- Khách hàng cũ -</option>
                <option value="-1">- Khách hàng mới -</option>
            </select>
        </div>
    </div>
</div>
<hr>
<div class="row" id="form2" style="display:none">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
          Thêm mới
        </div>
          <div class="panel-body">
              <div class="col-lg-12 ">
                <form action="{{route('order.store')}}" method="post" id="formDemo" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Tên Khách hàng</label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email">
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="number" class="form-control" name="phone">
                    </div>
                    <div class="form-group">
                        <label>Ngày Sinh</label>
                        <input type="date" class="form-control" name="date">
                    </div>
                    <div class="form-group">
                        <label>Số CMT</label>
                        <input type="number" class="form-control" name="cmt">
                    </div>
                    <div class="form-group">
                        <label>Dịch vụ</label>
                        <select name="service_id" class="form-control">
                            <option value="">-Hãy chọn Dịch vụ-</option>
                            @foreach($service as $key => $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Chọn ngày khám</label>
                        <select name="clinic_schedule_id" class="form-control" id="schedule">
                            <option value=""></option>
                            @foreach($schedule as $key => $item)
                            <option value="{{$item->id}}" class = "x1">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                   <!-- / đổ vòng lặp này ô -->
                    <div id="choose1" class="form-group">
                        
                    </div>
                    <div class="form-group" >
                        <label>Ghi chú</label>
                        <textarea cols="30" rows="10" class="form-control" name="message" id="editor1" placeholder="Thai của bạn bao nhiêu tuần tuổi? , bạn có ghi chú gì không ạ :)"></textarea>
                    </div>
                    <!--  -->
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" ><i class="fa fa-save"></i> Thêm đặt lịch </button>
                        <a class="btn btn-warning" href="{{route('order')}}"><i class="fa fa-reply"></i>  Quay lại </a>   
                    </div>
                </form>
              </div>
           </div>
        </div>
    </div>
</div>
<div class="row" id="form1" style="display:none">
  <div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
          Thêm mới 
        </div>
          <div class="panel-body">
              <div class="col-lg-12 ">
                <form action="{{route('order.storev2')}}" method="post" id="formDemo" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Chọn khách hàng </label>
                        <select name="customer_id" class="form-control">
                            <option value="">-chọn -</option>
                            @foreach($customer as $key => $item)
                            <option value="{{$item->id}}">{{$item->name}} || {{$item->phone}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Dịch vụ</label>
                        <select name="service_id" class="form-control">
                            <option value="">-Hãy chọn Dịch vụ-</option>
                            @foreach($service as $key => $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Chọn ngày khám</label>
                        <select name="clinic_schedule_id" class="form-control" id="schedule_2">
                            <option value=""></option>
                            @foreach($schedule as $key => $item)
                            <option value="{{$item->id}}" class = "x1">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                   <!-- / đổ vòng lặp này ô -->
                    <div id="choose2" class="form-group">
                        
                    </div>
                    <div class="form-group" >
                        <label>Ghi chú</label>
                        <textarea cols="30" rows="10" class="form-control" name="message"  placeholder="Thai của bạn bao nhiêu tuần tuổi? , bạn có ghi chú gì không ạ :)"></textarea>
                    </div>
                    <!--  -->
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" ><i class="fa fa-save"></i> Thêm đặt lịch </button>
                        <a class="btn btn-warning" href="{{route('order')}}"><i class="fa fa-reply"></i>  Quay lại </a>   
                    </div>
                </form>
              </div>
           </div>
        </div>
    </div>
</div>
<script> 
    $('#changer').change(function(event){
        var xxx = this.value;
        if (xxx == 0) {
            $('#form1').css("display","none");
            $('#form2').css("display","none");
        }if(xxx == 1) {
            $("#form1").toggle(100);
            $('#form2').css("display","none");
        }if(xxx == -1) {
            $("#form2").toggle(100);
            $('#form1').css("display","none");
        }
    });
    $('#schedule').change(function(event) {
        var id1 = this.value;
        $.ajax({
            url: '{{route('apiv1')}}',
            type: 'POST',
            data: {id: id1 , _token: '{{csrf_token()}}'},
        })
        .done(function(data) {
            if(data && data.length > 0) {
                $('#choose1').html('');
                data.forEach(hour => {
                    $('#choose1').append(`
                        <label class="checkbox-inline">
                            <input name="time" type="radio" class="xxx" value="${hour.id}"> ${hour.name}
                        </label>
                    `);
                })
            }
        })
        .fail(function() {
        })
    });
    $('#schedule_2').change(function(event) {
        var id2 = this.value;
        $.ajax({
            url: '{{route('apiv1')}}',
            type: 'POST',
            data: {id: id2 , _token: '{{csrf_token()}}'},
        })
        .done(function(data) {
            if(data && data.length > 0) {
                $('#choose2').html('');
                data.forEach(hour => {
                    $('#choose2').append(`
                        <label class="checkbox-inline">
                            <input name="time" type="radio" class="xxx" value="${hour.id}"> ${hour.name}
                        </label>
                    `);
                })
            }
        })
        .fail(function() {
        })
    });
</script>
@endsection