-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 15, 2020 lúc 12:56 PM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `api_csm`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `clinic_schedules`
--

CREATE TABLE `clinic_schedules` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `day` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Lịch khám bệnh';

--
-- Đang đổ dữ liệu cho bảng `clinic_schedules`
--

INSERT INTO `clinic_schedules` (`id`, `name`, `day`, `created_at`, `updated_at`) VALUES
(21, '16/12', '2020-12-16', '2020-12-10 14:15:23', '2020-12-10 14:15:23'),
(22, '17/12', '2020-12-17', '2020-12-10 18:32:09', '2020-12-10 18:32:09'),
(23, '18/12', '2020-12-18', '2020-12-10 18:32:43', '2020-12-10 18:32:43'),
(25, '27/12', '2020-12-27', '2020-12-10 21:12:29', '2020-12-10 21:12:29'),
(26, '15/12', '2020-12-15', '2020-12-10 23:09:52', '2020-12-10 23:09:52'),
(27, '12/12', '2020-12-12', '2020-12-11 03:51:59', '2020-12-11 03:51:59'),
(28, '13/12', '2020-12-13', '2020-12-13 00:25:52', '2020-12-13 00:25:52'),
(29, '14/12/2020', '2020-12-14', '2020-12-13 02:20:03', '2020-12-13 02:20:03'),
(30, '19/12/2020', '2020-12-19', '2020-12-13 02:20:42', '2020-12-13 02:20:42'),
(31, '20/12/2020', '2020-12-20', '2020-12-13 10:36:09', '2020-12-13 10:36:09'),
(32, '21/12/2020', '2020-12-21', '2020-12-13 10:36:32', '2020-12-13 10:36:32'),
(33, '22/12/2020', '2020-12-22', '2020-12-13 10:37:03', '2020-12-13 10:37:03'),
(34, '23/12/2020', '2020-12-23', '2020-12-13 10:39:52', '2020-12-13 10:39:52'),
(35, '24/12/2020', '2020-12-24', '2020-12-13 10:40:04', '2020-12-13 10:40:04'),
(36, '25/12/2020', '2020-12-25', '2020-12-13 10:40:39', '2020-12-13 10:40:39'),
(37, '26/12/2020', '2020-12-26', '2020-12-13 10:40:53', '2020-12-13 10:40:53'),
(38, '28/12/2020', '2020-12-28', '2020-12-13 10:41:14', '2020-12-13 10:41:14'),
(39, '29/12/2020', '2020-12-29', '2020-12-13 10:41:53', '2020-12-13 10:41:53'),
(41, '30/12', '2020-12-30', '2020-12-13 10:42:29', '2020-12-13 10:42:29'),
(42, '31/12', '2020-12-31', '2020-12-13 10:42:47', '2020-12-13 10:42:47'),
(43, '1/1/2021', '2021-01-01', '2020-12-14 01:53:28', '2020-12-14 01:53:28'),
(44, 'Quang Minh', '2020-11-30', '2020-12-14 01:58:23', '2020-12-14 01:58:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `clinic_schedule_timeclass`
--

CREATE TABLE `clinic_schedule_timeclass` (
  `id` int(11) NOT NULL,
  `clinic_schedule_id` int(11) DEFAULT NULL,
  `timeclass_id` int(11) DEFAULT NULL,
  `is_booking` tinyint(4) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `clinic_schedule_timeclass`
--

INSERT INTO `clinic_schedule_timeclass` (`id`, `clinic_schedule_id`, `timeclass_id`, `is_booking`, `created_at`, `updated_at`) VALUES
(41, 21, 1, 0, '2020-12-10 14:15:23', '2020-12-10 14:15:23'),
(42, 21, 2, 0, '2020-12-10 14:15:23', '2020-12-10 14:15:23'),
(43, 21, 3, 0, '2020-12-10 14:15:23', '2020-12-10 14:15:23'),
(44, 21, 11, 0, '2020-12-10 14:15:23', '2020-12-10 14:15:23'),
(45, 21, 12, 0, '2020-12-10 14:15:23', '2020-12-10 14:15:23'),
(46, 21, 13, 0, '2020-12-10 14:15:23', '2020-12-10 14:15:23'),
(47, 21, 14, 0, '2020-12-10 14:15:23', '2020-12-10 14:15:23'),
(48, 21, 15, 0, '2020-12-10 14:15:23', '2020-12-11 01:13:49'),
(49, 21, 16, 1, '2020-12-10 14:15:23', '2020-12-10 20:59:17'),
(50, 22, 1, 1, '2020-12-10 18:32:09', '2020-12-10 20:49:34'),
(51, 22, 2, 0, '2020-12-10 18:32:09', '2020-12-10 18:32:09'),
(52, 22, 3, 0, '2020-12-10 18:32:09', '2020-12-10 20:34:49'),
(53, 22, 11, 0, '2020-12-10 18:32:09', '2020-12-10 18:32:09'),
(54, 22, 12, 0, '2020-12-10 18:32:09', '2020-12-10 18:32:09'),
(55, 22, 13, 0, '2020-12-10 18:32:09', '2020-12-10 18:32:09'),
(56, 22, 14, 0, '2020-12-10 18:32:09', '2020-12-10 18:32:09'),
(57, 22, 15, 1, '2020-12-10 18:32:09', '2020-12-12 00:08:32'),
(58, 22, 16, 1, '2020-12-10 18:32:09', '2020-12-11 01:28:45'),
(59, 23, 1, 1, '2020-12-10 18:32:43', '2020-12-10 20:48:58'),
(60, 23, 2, 1, '2020-12-10 18:32:43', '2020-12-10 23:34:05'),
(61, 23, 3, 0, '2020-12-10 18:32:43', '2020-12-11 02:34:02'),
(62, 23, 11, 0, '2020-12-10 18:32:43', '2020-12-10 18:32:43'),
(63, 23, 12, 1, '2020-12-10 18:32:43', '2020-12-12 02:15:11'),
(64, 23, 13, 1, '2020-12-10 18:32:43', '2020-12-12 00:05:14'),
(65, 23, 14, 1, '2020-12-10 18:32:43', '2020-12-12 00:02:02'),
(66, 23, 15, 1, '2020-12-10 18:32:43', '2020-12-11 23:15:20'),
(67, 25, 1, 0, '2020-12-10 21:12:29', '2020-12-10 21:12:29'),
(68, 25, 2, 0, '2020-12-10 21:12:29', '2020-12-10 21:12:29'),
(69, 25, 3, 0, '2020-12-10 21:12:29', '2020-12-10 21:12:29'),
(70, 25, 11, 0, '2020-12-10 21:12:29', '2020-12-10 21:12:29'),
(71, 25, 12, 0, '2020-12-10 21:12:29', '2020-12-10 21:12:29'),
(72, 25, 13, 0, '2020-12-10 21:12:29', '2020-12-10 21:12:29'),
(73, 25, 14, 0, '2020-12-10 21:12:29', '2020-12-10 21:12:29'),
(74, 25, 15, 1, '2020-12-10 21:12:29', '2020-12-13 07:16:46'),
(75, 25, 16, 0, '2020-12-10 21:12:29', '2020-12-10 21:12:29'),
(76, 26, 1, 0, '2020-12-10 23:09:52', '2020-12-10 23:09:52'),
(77, 26, 2, 0, '2020-12-10 23:09:52', '2020-12-10 23:09:52'),
(78, 26, 3, 0, '2020-12-10 23:09:52', '2020-12-10 23:09:52'),
(79, 26, 11, 0, '2020-12-10 23:09:52', '2020-12-10 23:09:52'),
(80, 26, 12, 0, '2020-12-10 23:09:52', '2020-12-10 23:09:52'),
(81, 26, 13, 0, '2020-12-10 23:09:52', '2020-12-10 23:09:52'),
(82, 26, 14, 0, '2020-12-10 23:09:52', '2020-12-10 23:09:52'),
(83, 26, 15, 1, '2020-12-10 23:09:52', '2020-12-14 10:00:11'),
(84, 26, 16, 1, '2020-12-10 23:09:52', '2020-12-12 23:11:43'),
(85, 27, 1, 0, '2020-12-11 03:51:59', '2020-12-11 03:51:59'),
(86, 28, 1, 1, '2020-12-13 00:25:52', '2020-12-13 07:26:52'),
(87, 28, 2, 0, '2020-12-13 00:25:52', '2020-12-13 00:25:52'),
(88, 28, 3, 0, '2020-12-13 00:25:52', '2020-12-13 00:25:52'),
(89, 28, 11, 0, '2020-12-13 00:25:52', '2020-12-13 00:25:52'),
(90, 28, 12, 0, '2020-12-13 00:25:52', '2020-12-13 00:25:52'),
(91, 28, 13, 0, '2020-12-13 00:25:52', '2020-12-13 00:25:52'),
(92, 28, 14, 0, '2020-12-13 00:25:52', '2020-12-13 00:25:52'),
(93, 28, 15, 1, '2020-12-13 00:25:52', '2020-12-13 01:26:24'),
(94, 28, 16, 1, '2020-12-13 00:25:52', '2020-12-13 00:27:06'),
(95, 29, 1, 1, '2020-12-13 02:20:03', '2020-12-14 01:43:27'),
(96, 29, 2, 0, '2020-12-13 02:20:03', '2020-12-13 02:20:03'),
(97, 29, 3, 0, '2020-12-13 02:20:03', '2020-12-13 02:20:03'),
(98, 29, 11, 0, '2020-12-13 02:20:03', '2020-12-13 02:20:03'),
(99, 29, 12, 0, '2020-12-13 02:20:03', '2020-12-13 02:20:03'),
(100, 29, 13, 0, '2020-12-13 02:20:03', '2020-12-13 02:20:03'),
(101, 29, 14, 0, '2020-12-13 02:20:03', '2020-12-13 02:20:03'),
(102, 29, 15, 0, '2020-12-13 02:20:03', '2020-12-13 02:20:03'),
(103, 29, 16, 1, '2020-12-13 02:20:03', '2020-12-13 17:34:36'),
(104, 30, 1, 0, '2020-12-13 02:20:42', '2020-12-13 02:20:42'),
(105, 30, 2, 0, '2020-12-13 02:20:42', '2020-12-13 02:20:42'),
(106, 30, 3, 0, '2020-12-13 02:20:42', '2020-12-13 02:20:42'),
(107, 30, 11, 0, '2020-12-13 02:20:42', '2020-12-13 02:20:42'),
(108, 30, 12, 0, '2020-12-13 02:20:42', '2020-12-13 02:20:42'),
(109, 30, 13, 0, '2020-12-13 02:20:42', '2020-12-13 02:20:42'),
(110, 30, 14, 0, '2020-12-13 02:20:42', '2020-12-13 02:20:42'),
(111, 30, 15, 0, '2020-12-13 02:20:42', '2020-12-13 02:20:42'),
(112, 30, 16, 0, '2020-12-13 02:20:42', '2020-12-13 02:20:42'),
(113, 31, 1, 0, '2020-12-13 10:36:09', '2020-12-13 10:36:09'),
(114, 31, 2, 0, '2020-12-13 10:36:09', '2020-12-13 10:36:09'),
(115, 31, 3, 0, '2020-12-13 10:36:09', '2020-12-13 10:36:09'),
(116, 31, 11, 0, '2020-12-13 10:36:09', '2020-12-13 10:36:09'),
(117, 31, 12, 0, '2020-12-13 10:36:09', '2020-12-13 10:36:09'),
(118, 31, 13, 0, '2020-12-13 10:36:09', '2020-12-13 10:36:09'),
(119, 31, 14, 0, '2020-12-13 10:36:09', '2020-12-13 10:36:09'),
(120, 31, 15, 0, '2020-12-13 10:36:09', '2020-12-13 10:36:09'),
(121, 31, 16, 0, '2020-12-13 10:36:09', '2020-12-13 10:36:09'),
(122, 32, 1, 0, '2020-12-13 10:36:32', '2020-12-13 10:36:32'),
(123, 32, 2, 0, '2020-12-13 10:36:32', '2020-12-13 10:36:32'),
(124, 32, 3, 0, '2020-12-13 10:36:32', '2020-12-13 10:36:32'),
(125, 32, 11, 0, '2020-12-13 10:36:32', '2020-12-13 10:36:32'),
(126, 32, 12, 0, '2020-12-13 10:36:32', '2020-12-13 10:36:32'),
(127, 32, 13, 0, '2020-12-13 10:36:32', '2020-12-13 10:36:32'),
(128, 32, 14, 0, '2020-12-13 10:36:32', '2020-12-13 10:36:32'),
(129, 32, 15, 0, '2020-12-13 10:36:32', '2020-12-13 10:36:32'),
(130, 32, 16, 0, '2020-12-13 10:36:32', '2020-12-13 10:36:32'),
(131, 33, 1, 0, '2020-12-13 10:37:03', '2020-12-13 10:37:03'),
(132, 33, 2, 0, '2020-12-13 10:37:03', '2020-12-13 10:37:03'),
(133, 33, 3, 0, '2020-12-13 10:37:03', '2020-12-13 10:37:03'),
(134, 33, 11, 0, '2020-12-13 10:37:03', '2020-12-13 10:37:03'),
(135, 33, 12, 0, '2020-12-13 10:37:03', '2020-12-13 10:37:03'),
(136, 33, 13, 0, '2020-12-13 10:37:03', '2020-12-13 10:37:03'),
(137, 33, 14, 0, '2020-12-13 10:37:03', '2020-12-13 10:37:03'),
(138, 33, 15, 0, '2020-12-13 10:37:03', '2020-12-13 10:37:03'),
(139, 33, 16, 0, '2020-12-13 10:37:03', '2020-12-13 10:37:03'),
(140, 34, 1, 0, '2020-12-13 10:39:52', '2020-12-13 10:39:52'),
(141, 34, 2, 0, '2020-12-13 10:39:52', '2020-12-13 10:39:52'),
(142, 34, 3, 0, '2020-12-13 10:39:52', '2020-12-13 10:39:52'),
(143, 34, 11, 0, '2020-12-13 10:39:52', '2020-12-13 10:39:52'),
(144, 34, 12, 0, '2020-12-13 10:39:52', '2020-12-13 10:39:52'),
(145, 34, 13, 0, '2020-12-13 10:39:52', '2020-12-13 10:39:52'),
(146, 34, 14, 0, '2020-12-13 10:39:52', '2020-12-13 10:39:52'),
(147, 34, 15, 0, '2020-12-13 10:39:52', '2020-12-13 10:39:52'),
(148, 34, 16, 0, '2020-12-13 10:39:52', '2020-12-13 10:39:52'),
(149, 35, 1, 0, '2020-12-13 10:40:04', '2020-12-13 10:40:04'),
(150, 35, 2, 0, '2020-12-13 10:40:04', '2020-12-13 10:40:04'),
(151, 35, 3, 0, '2020-12-13 10:40:04', '2020-12-13 10:40:04'),
(152, 35, 11, 0, '2020-12-13 10:40:04', '2020-12-13 10:40:04'),
(153, 35, 12, 0, '2020-12-13 10:40:04', '2020-12-13 10:40:04'),
(154, 35, 13, 0, '2020-12-13 10:40:04', '2020-12-13 10:40:04'),
(155, 35, 14, 0, '2020-12-13 10:40:04', '2020-12-13 10:40:04'),
(156, 35, 15, 0, '2020-12-13 10:40:04', '2020-12-13 10:40:04'),
(157, 35, 16, 0, '2020-12-13 10:40:04', '2020-12-13 10:40:04'),
(158, 36, 1, 0, '2020-12-13 10:40:39', '2020-12-13 10:40:39'),
(159, 36, 2, 0, '2020-12-13 10:40:39', '2020-12-13 10:40:39'),
(160, 36, 3, 0, '2020-12-13 10:40:39', '2020-12-13 10:40:39'),
(161, 36, 11, 0, '2020-12-13 10:40:39', '2020-12-13 10:40:39'),
(162, 36, 12, 0, '2020-12-13 10:40:39', '2020-12-13 10:40:39'),
(163, 36, 13, 0, '2020-12-13 10:40:39', '2020-12-13 10:40:39'),
(164, 36, 14, 0, '2020-12-13 10:40:39', '2020-12-13 10:40:39'),
(165, 36, 15, 0, '2020-12-13 10:40:39', '2020-12-13 10:40:39'),
(166, 36, 16, 0, '2020-12-13 10:40:39', '2020-12-13 10:40:39'),
(167, 37, 1, 0, '2020-12-13 10:40:53', '2020-12-13 10:40:53'),
(168, 37, 2, 0, '2020-12-13 10:40:53', '2020-12-13 10:40:53'),
(169, 37, 3, 0, '2020-12-13 10:40:53', '2020-12-13 10:40:53'),
(170, 37, 11, 0, '2020-12-13 10:40:53', '2020-12-13 10:40:53'),
(171, 37, 12, 0, '2020-12-13 10:40:53', '2020-12-13 10:40:53'),
(172, 37, 13, 0, '2020-12-13 10:40:53', '2020-12-13 10:40:53'),
(173, 37, 14, 0, '2020-12-13 10:40:53', '2020-12-13 10:40:53'),
(174, 37, 15, 0, '2020-12-13 10:40:53', '2020-12-13 10:40:53'),
(175, 37, 16, 0, '2020-12-13 10:40:53', '2020-12-13 10:40:53'),
(176, 38, 1, 0, '2020-12-13 10:41:14', '2020-12-13 10:41:14'),
(177, 38, 2, 0, '2020-12-13 10:41:14', '2020-12-13 10:41:14'),
(178, 38, 3, 0, '2020-12-13 10:41:14', '2020-12-13 10:41:14'),
(179, 38, 11, 0, '2020-12-13 10:41:14', '2020-12-13 10:41:14'),
(180, 38, 12, 0, '2020-12-13 10:41:14', '2020-12-13 10:41:14'),
(181, 38, 13, 0, '2020-12-13 10:41:14', '2020-12-13 10:41:14'),
(182, 38, 14, 0, '2020-12-13 10:41:14', '2020-12-13 10:41:14'),
(183, 38, 15, 0, '2020-12-13 10:41:14', '2020-12-13 10:41:14'),
(184, 38, 16, 0, '2020-12-13 10:41:14', '2020-12-13 10:41:14'),
(185, 39, 1, 0, '2020-12-13 10:41:53', '2020-12-13 10:41:53'),
(186, 39, 2, 0, '2020-12-13 10:41:53', '2020-12-13 10:41:53'),
(187, 39, 3, 0, '2020-12-13 10:41:53', '2020-12-13 10:41:53'),
(188, 39, 11, 0, '2020-12-13 10:41:53', '2020-12-13 10:41:53'),
(189, 39, 12, 0, '2020-12-13 10:41:53', '2020-12-13 10:41:53'),
(190, 39, 13, 0, '2020-12-13 10:41:53', '2020-12-13 10:41:53'),
(191, 39, 14, 0, '2020-12-13 10:41:53', '2020-12-13 10:41:53'),
(192, 39, 15, 0, '2020-12-13 10:41:53', '2020-12-13 10:41:53'),
(193, 39, 16, 0, '2020-12-13 10:41:53', '2020-12-13 10:41:53'),
(194, 41, 1, 0, '2020-12-13 10:42:29', '2020-12-13 10:42:29'),
(195, 41, 2, 0, '2020-12-13 10:42:29', '2020-12-13 10:42:29'),
(196, 41, 3, 0, '2020-12-13 10:42:29', '2020-12-13 10:42:29'),
(197, 41, 11, 0, '2020-12-13 10:42:29', '2020-12-13 10:42:29'),
(198, 41, 12, 0, '2020-12-13 10:42:29', '2020-12-13 10:42:29'),
(199, 41, 13, 0, '2020-12-13 10:42:29', '2020-12-13 10:42:29'),
(200, 41, 14, 0, '2020-12-13 10:42:29', '2020-12-13 10:42:29'),
(201, 41, 15, 0, '2020-12-13 10:42:29', '2020-12-13 10:42:29'),
(202, 41, 16, 0, '2020-12-13 10:42:29', '2020-12-13 10:42:29'),
(203, 42, 1, 0, '2020-12-13 10:42:47', '2020-12-13 10:42:47'),
(204, 42, 2, 0, '2020-12-13 10:42:47', '2020-12-13 10:42:47'),
(205, 42, 3, 0, '2020-12-13 10:42:47', '2020-12-13 10:42:47'),
(206, 42, 11, 0, '2020-12-13 10:42:47', '2020-12-13 10:42:47'),
(207, 42, 12, 0, '2020-12-13 10:42:47', '2020-12-13 10:42:47'),
(208, 42, 13, 0, '2020-12-13 10:42:47', '2020-12-13 10:42:47'),
(209, 42, 14, 0, '2020-12-13 10:42:47', '2020-12-13 10:42:47'),
(210, 42, 15, 0, '2020-12-13 10:42:47', '2020-12-13 10:42:47'),
(211, 42, 16, 0, '2020-12-13 10:42:47', '2020-12-13 10:42:47'),
(212, 43, 15, 0, '2020-12-14 01:53:28', '2020-12-14 01:53:28'),
(213, 43, 16, 0, '2020-12-14 01:53:28', '2020-12-14 01:53:28'),
(214, 44, 1, 0, '2020-12-14 01:58:23', '2020-12-14 01:58:23'),
(215, 44, 2, 0, '2020-12-14 01:58:23', '2020-12-14 01:58:23'),
(216, 44, 3, 0, '2020-12-14 01:58:23', '2020-12-14 01:58:23'),
(217, 44, 11, 0, '2020-12-14 01:58:23', '2020-12-14 01:58:23'),
(218, 44, 12, 0, '2020-12-14 01:58:23', '2020-12-14 01:58:23'),
(219, 44, 13, 0, '2020-12-14 01:58:23', '2020-12-14 01:58:23'),
(220, 44, 14, 0, '2020-12-14 01:58:23', '2020-12-14 01:58:23'),
(221, 44, 15, 0, '2020-12-14 01:58:23', '2020-12-14 01:58:23'),
(222, 44, 16, 0, '2020-12-14 01:58:23', '2020-12-14 01:58:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date DEFAULT NULL,
  `cmt` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`id`, `name`, `email`, `phone`, `date`, `cmt`, `user_id`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 'nguyen huu van', 'duyvan@gmail.com', '0978894832', '2020-06-06', '123212343', 0, '1', '2020-12-07 19:41:08', '2020-12-08 23:42:18'),
(2, 'tuyennqph06237@fpt.edu.vn', 'admin@a.a', '0397872940', '2020-01-04', '123456765', 0, '0', '2020-12-07 20:24:10', '2020-12-07 20:24:10'),
(3, 'adfasdfasdf', 'tranhai9447@gmail.com', '3563456345634', '1969-02-02', '00221515', 0, '0', '2020-12-08 00:28:18', '2020-12-08 00:28:18'),
(4, 'quang minh', 'admin@222222a.a', '12121212', '2020-07-09', '1234324', 0, '0', '2020-12-08 01:05:54', '2020-12-08 01:05:54'),
(6, 'nguyen quang tuyen 2', 'adminwewe@a.a', '43433434', '2020-07-09', '1224242', 0, '0', '2020-12-08 02:16:51', '2020-12-08 02:16:51'),
(7, 'nguyen quang tuyen', 'tuyennguyencp99@gmail.com', '0989998989', '2020-09-01', '12928321', 0, '1', '2020-12-08 02:31:27', '2020-12-08 02:49:03'),
(8, 'hoang the anh', 'anh\\', '0397822946', NULL, '1020202', 54, '0', '2020-12-08 02:50:05', '2020-12-08 02:50:05'),
(9, 'the anh', 'theanh@gmail.com', '123443', NULL, '121221', 59, '0', '2020-12-08 02:50:51', '2020-12-08 02:50:51'),
(10, 'the anh demo', 'th@gmail.com', '0909090909', '2020-12-05', '22222222', 60, '0', '2020-12-08 02:53:27', '2020-12-08 02:53:27'),
(11, 'tuyen demo', 'dsdas@gmail.com', '23873828', '2020-12-13', '22222222', 61, '0', '2020-12-08 02:58:08', '2020-12-08 02:58:08'),
(12, 'nguyen quang tuyen', 'adádamin@a.a', '094434334', '2020-04-02', '111111', 0, '1', '2020-12-08 03:19:40', '2020-12-08 23:41:50'),
(13, 'eqweqweqwe', 'thhest@gmail.com', '0397874946', '2020-01-01', '213123123', 0, '1', '2020-12-08 23:06:48', '2020-12-08 23:41:53'),
(14, 'tuyen dzai', 'ad1111min@a.a', '12121212', '2020-01-01', '12321212', 0, '0', '2020-12-08 23:08:10', '2020-12-08 23:08:10'),
(15, 'tuyen dep trai1', 'adm111in@a.a', '231313', '2020-03-03', '121212', 0, '1', '2020-12-08 23:09:33', '2020-12-08 23:59:05'),
(16, 'hoang anh', 'hoajg@gmail.com', '23423423', '2020-07-09', '12121212', 0, '1', '2020-12-08 23:39:41', '2020-12-08 23:43:01'),
(17, 'tuyennqph06237@fpt.edu.vn', 'tuyennguyencp99@gmail.com', '0397872123', '2020-01-08', '2323', 0, '0', '2020-12-09 01:11:19', '2020-12-09 01:11:19'),
(18, 'tao là tuyến', 'taolatuyen@gmail.com', '0967793300', '2020-01-08', '2323', 0, '1', '2020-12-09 01:12:46', '2020-12-09 02:13:16'),
(19, 'Hoàng Anh', 'anhzpt3@gmail.com', '0339315877', '1999-11-24', '12121212', 0, '0', '2020-12-10 02:18:16', '2020-12-10 02:18:16'),
(20, 'Hoàng Anh2', 'anhzpt32@gmail.com', '02222222222', '1999-01-01', '1212', 0, '0', '2020-12-10 03:22:19', '2020-12-10 03:22:19'),
(21, 'Hoàng Anh', 'anhssszpt3@gmail.com', '099999999', '1999-01-01', '11111111111111', 0, '0', '2020-12-10 04:22:23', '2020-12-10 04:22:23'),
(22, 'Hoàng Anh', 'testt3@gmail.com', '012345612', '1999-01-01', '1234566', 0, '0', '2020-12-10 04:23:06', '2020-12-10 04:23:06'),
(23, 'Hoàng Anhdddd', 'anhzpt3@gmail.com', '12122122212', '1999-01-01', '111221221', 0, '0', '2020-12-10 04:30:57', '2020-12-10 04:30:57'),
(24, 'Trung truy', 'truy@gmail.com', '1234555554', '1980-06-12', '123444444', 76, '0', '2020-12-10 08:09:19', '2020-12-10 08:09:19'),
(25, 'Trung truy2', 'truy2@gmail.com', '0122333456', '1999-03-12', '121221212', 77, '0', '2020-12-10 08:10:25', '2020-12-10 08:10:25'),
(26, 'Hoàng Anhxxx', 'anhzpt3@gmail.com', '12121212', '1999-02-12', '12222222', 78, '0', '2020-12-10 08:20:37', '2020-12-10 08:20:37'),
(28, 'Quang Minh', 'anhzpt3@gmail.com', '1234444444', '1997-01-01', '111111111', 80, '0', '2020-12-10 20:58:19', '2020-12-10 20:58:19'),
(29, 'Quang Minh x2', 'Minhbede@gmail.com', '123444442222', '1997-01-01', '111111111', 81, '0', '2020-12-10 21:00:00', '2020-12-10 21:00:00'),
(30, 'dịch vụ xóa 1', 'anhzpt3@gmail.com', '3', '2020-12-08', '2', 82, '0', '2020-12-10 21:14:33', '2020-12-10 21:14:33'),
(31, 'Quang Tuyến', 'tuyenssss3@gmail.com', '012282639939', '1999-01-01', '12121212', 83, '0', '2020-12-10 23:34:05', '2020-12-10 23:34:05'),
(32, 'Quang Tuyến x2', 'tuyenx2@gmail.com', '123333333', '1999-01-01', '1221222', 84, '0', '2020-12-10 23:35:12', '2020-12-10 23:35:12'),
(33, 'Phụ nữ Giấu Tên', 'phunugiauten@gmail.com', '0999999922', '1996-11-11', '101299774', 85, '0', '2020-12-11 01:28:46', '2020-12-11 01:28:46'),
(34, 'Phụ nữ Giấu Tên 12/12', 'phunugiauten2@gmail.com', '0123456789', '1998-12-12', '111111111111', 86, '0', '2020-12-11 23:15:20', '2020-12-11 23:15:20'),
(35, 'Đàn ông giấu tên 14h', 'anhzpt3@gmail.com', '01639315877', '1999-11-21', '101294828', 87, '0', '2020-12-12 00:02:02', '2020-12-12 00:02:02'),
(36, 'đàn ông Giấu Tên 12/12', 'anhzpt3@gmail.com', '012345677', '1999-11-21', '123456789', 89, '0', '2020-12-12 00:08:32', '2020-12-12 00:08:32'),
(37, 'Hoàng Anh', 'anhzpt3@gmail.com', '0123456777', '1999-12-12', '1234567899', 90, '0', '2020-12-12 02:15:11', '2020-12-12 02:15:11'),
(38, 'Tuyến ndnđn', 'tuyensss@gmail.com', '0123123123', '1999-12-12', '111111111', 91, '0', '2020-12-13 01:26:24', '2020-12-13 01:26:24'),
(39, 'tiến dũng', 'tiendung@gmail.com', '012344321', '1999-01-01', '111111222', 92, '0', '2020-12-13 07:26:52', '2020-12-13 07:26:52'),
(40, 'Quang tuyen', 'anhzpt3@gmail.com', '0974842866', '2020-12-01', '0123456789', 93, '0', '2020-12-14 01:43:27', '2020-12-14 01:43:27');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `doctor`
--

CREATE TABLE `doctor` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'images/images.png	',
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `doctor`
--

INSERT INTO `doctor` (`id`, `name`, `email`, `phone`, `info`, `image`, `user_id`, `is_delete`, `created_at`, `updated_at`) VALUES
(2, 'Tạ Như Anh', 'TaNhuAnh@gmail.com', '0333333332', 'đây là bác sĩ', 'images/doctor/bs.jpeg	', '34', '0', '2020-11-18 05:05:38', '2020-11-18 05:15:42'),
(3, 'Đỗ Quang Minh', 'minhminh1111@gmail.com', '03333333332', 'đây là bác sĩ', 'images/doctor/5fb5102f78b7b-IMG20170810143343_1.jpg', '35', '0', '2020-11-18 05:14:39', '2020-11-18 05:21:47');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `feedback`
--

CREATE TABLE `feedback` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `histories`
--

CREATE TABLE `histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_list_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `histories`
--

INSERT INTO `histories` (`id`, `customer_id`, `order_list_id`, `content`, `is_delete`, `created_at`, `updated_at`) VALUES
(3, '28', '19', 'Nội dung hồ sơ khách hàng 1', '0', '2020-12-11 03:59:17', '2020-12-11 03:59:17'),
(4, '37', '28', 'Nội dung hồ sơ khách hàng 2', '0', '2020-12-12 02:20:54', '2020-12-12 02:20:54'),
(5, '38', '31', 'Nội dung hồ sơ khách hàng 3', '0', '2020-12-13 01:49:53', '2020-12-13 01:49:53'),
(7, '37', '30', 'Nội dung hồ sơ khách hàng 4', '0', '2020-12-13 07:17:26', '2020-12-13 07:17:26'),
(8, '39', '33', 'nội dung khám 5', '0', '2020-12-13 07:28:08', '2020-12-13 07:28:08'),
(13, '40', '35', 'noidungkham', '0', '2020-12-14 02:16:31', '2020-12-14 02:16:31'),
(14, '37', '34', '<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Tử cung</td>\r\n			<td>\r\n			<ul>\r\n				<li>Trong buồng trứng c&oacute; 1 thai</li>\r\n				<li>Chiều d&agrave;i cổ tử cung</li>\r\n			</ul>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Thai</td>\r\n			<td>\r\n			<ul>\r\n				<li>Tim thai b&igrave;nh thường</li>\r\n				<li>Dự kiến c&acirc;n nặng 100gr</li>\r\n			</ul>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Kết Luận</td>\r\n			<td>\r\n			<ul>\r\n				<li>Thai b&igrave;nh thường</li>\r\n			</ul>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>', '0', '2020-12-14 02:42:25', '2020-12-15 00:18:06');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `histories_login_ad`
--

CREATE TABLE `histories_login_ad` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` datetime NOT NULL,
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `histories_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `is_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `incurred`
--

CREATE TABLE `incurred` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `histories_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `incurred`
--

INSERT INTO `incurred` (`id`, `histories_id`, `name`, `price`, `is_delete`, `created_at`, `updated_at`) VALUES
(9, '3', 'Mua thêm 1 vỉ sữa', '200000', '0', '2020-12-11 03:59:17', '2020-12-11 03:59:17');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_07_11_080611_create_posts_table', 1),
(4, '2019_07_11_081637_alter_table_posts_add_column_status', 1),
(5, '2019_07_24_032234_create_categories_table', 1),
(6, '2019_07_24_032353_alter_table_posts_add_cate_id_column', 1),
(7, '2019_08_06_075253_alter_table_users_add_column_role_id', 1),
(8, '2019_08_09_082001_create_products_table', 1),
(9, '2019_08_09_082120_alter_table_products_add_cate_id_column', 1),
(10, '2019_08_22_045601_alter_table_products_add_price_column', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_list`
--

CREATE TABLE `order_list` (
  `id` int(11) NOT NULL,
  `time_id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT 14,
  `customer_id` int(11) NOT NULL,
  `clinic_schedule_id` int(11) NOT NULL,
  `message` text DEFAULT '',
  `is_delete` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `order_list`
--

INSERT INTO `order_list` (`id`, `time_id`, `service_id`, `customer_id`, `clinic_schedule_id`, `message`, `is_delete`, `created_at`, `updated_at`, `status`) VALUES
(19, 1, 14, 28, 22, 'notthing', 0, '2020-12-10 20:58:19', '2020-12-11 03:59:17', 2),
(20, 16, 14, 29, 21, 'notthing', 0, '2020-12-10 21:00:00', '2020-12-13 01:22:56', 1),
(21, 15, 14, 30, 21, 'notthing', 0, '2020-12-10 21:14:33', '2020-12-11 01:13:01', 4),
(22, 2, 14, 31, 23, 'notthing', 0, '2020-12-10 23:34:05', '2020-12-10 23:34:05', 0),
(23, 3, 14, 32, 23, 'notthing', 0, '2020-12-10 23:35:12', '2020-12-11 02:34:02', 4),
(24, 16, 14, 33, 22, 'notthing', 0, '2020-12-11 01:28:46', '2020-12-11 01:29:09', 1),
(25, 15, 16, 34, 23, 'notthing', 0, '2020-12-11 23:15:20', '2020-12-11 23:51:14', 1),
(26, 14, 14, 35, 23, 'notthing', 0, '2020-12-12 00:02:02', '2020-12-12 00:02:02', 0),
(27, 15, 15, 36, 22, 'notthing', 0, '2020-12-12 00:08:32', '2020-12-12 00:08:32', 0),
(28, 12, 115, 37, 23, 'notthing', 0, '2020-12-12 02:15:11', '2020-12-12 02:20:54', 2),
(29, 16, 14, 37, 26, NULL, 0, '2020-12-12 23:11:43', '2020-12-12 23:11:43', 0),
(30, 16, 14, 37, 28, NULL, 0, '2020-12-13 00:27:06', '2020-12-13 01:22:48', 2),
(31, 15, 14, 38, 28, '<p>trống</p>', 0, '2020-12-13 01:26:24', '2020-12-13 01:49:53', 2),
(32, 15, 14, 37, 25, 'lịch tái khám ', 0, '2020-12-13 07:17:26', '2020-12-13 07:17:26', 1),
(33, 1, 15, 39, 28, NULL, 0, '2020-12-13 07:26:52', '2020-12-13 07:27:43', 2),
(34, 16, 14, 37, 29, NULL, 0, '2020-12-13 17:34:36', '2020-12-14 02:42:25', 2),
(35, 1, 14, 40, 29, NULL, 0, '2020-12-14 01:43:27', '2020-12-14 02:16:31', 2),
(36, 15, 115, 37, 26, NULL, 0, '2020-12-14 10:00:11', '2020-12-14 10:00:27', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feature_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `feature_image`, `content`, `status`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 'Tại sao cần làm xét nghiệm nhóm máu ở bà bầu?', 'Tại-sao-cần-làm-xét-nghiệm-nhóm-máu-ở-bà-bầu?', 'images/posts/2.jpg', 'Khi xét nghiệm nhóm máu ở bà bầu cho kết quả là Rh(-) ở người mẹ, người cha có Rh(+) thì con của cặp vợ chồng này có thể gặp vấn đề về sức khỏe.\r\n\r\nNếu không có những bất thường khác, sự bất ', '1', '0', NULL, NULL),
(122, 'Bà bầu có uống được C sủi không? Lợi ích khi bổ sung vitamin C cho bà bầu', 'ba-bau-co-uong-duoc-c-sui-khong-loi-ich-khi-bo-sung-vitamin-c-cho-ba-bau', 'images/posts/3.jpg', 'Cả mẹ và bé đều cần vitamin C hàng ngày vì nó cần thiết để cơ thể tạo ra collagen, một loại protein cấu trúc là thành phần của sụn, gân, xương và da. Dựa trên các nghiên cứu trên động vật, một số nhà nghiên cứu tin rằng sự thiếu hụt vitamin C ở trẻ sơ sinh có thể làm giảm sự phát triển trí não.', '1', '0', '2020-12-03 02:28:38', '2020-12-03 02:28:52'),
(123, 'Bà bầu có uống được C sủi không? Lợi ích khi bổ sung vitamin C cho bà bầu', 'ba-bau-co-uong-duoc-c-sui-khong-loi-ich-khi-bo-sung-vitamin-c-cho-ba-bau', 'images/posts/4.jpg', 'Cả mẹ và bé đều cần vitamin C hàng ngày vì nó cần thiết để cơ thể tạo ra collagen, một loại protein cấu trúc là thành phần của sụn, gân, xương và da. Dựa trên các nghiên cứu trên động vật, một số nhà nghiên cứu tin rằng sự thiếu hụt vitamin C ở trẻ sơ sinh có thể làm giảm sự phát triển trí não.', '1', '0', '2020-12-03 02:28:38', '2020-12-03 02:28:50'),
(124, 'Bà bầu béo phì ảnh hưởng trí thông minh con trai', 'ba-bau-beo-phi-anh-huong-tri-thong-minh-con-trai', 'images/posts/5.jpg', 'Mẹ béo phì trong thai kỳ, bé trai sinh ra sẽ vận động kém hơn trẻ khác lúc 3 tuổi, IQ cũng thấp hơn 5 điểm khi lên 7.   Đây là kết quả nghiên cứu của các nhà khoa học Đại học Texas và Đại học Columbia sau khi khảo sát 368 bà mẹ và con của họ. Những phụ nữ này được yêu cầu báo cáo cân nặng trong ba tháng thứ hai và ba tháng cuối thai kỳ. Kỹ năng vận động và chỉ số IQ của những đứa trẻ năm ba và 7 tuổi cũng được kiểm tra kỹ lưỡng.   \"Điều đặc biệt là ngay cả khi sử dụng các đánh giá phát triển phù hợp với lứa tuổi khác nhau, mối liên hệ này vẫn tồn tại ở thời thơ ấu và trung niên, tức những tác động không biến mất theo thời gian\", theo Elizabeth Widen, tác giả nghiên cứu.    Kết quả vẫn đúng sau khi xét yếu tố như trình độ giáo dục người mẹ, trẻ có bị sinh non hay không. Tuy nhiên, môi trường giáo dục trẻ ở nhà - như tương tác với cha mẹ, trẻ có được tặng sách, đồ chơi hay không - có thể giảm tác động tiêu cực của bệnh béo phì ở mẹ bầu tới con trai. \"Tuy nhiên, ảnh hưởng này tới IQ trẻ thấp hơn so với khả năng vận động\", Elizabeth bổ sung.  Kết quả nghiên cứu mới được công bố trên BMC Pediatrics.', '1', '0', '2020-12-03 02:33:52', '2020-12-03 02:33:52'),
(125, 'Vitamin và khoáng chất cần thiết cho bà bầu', 'vitamin-va-khoang-chat-can-thiet-cho-ba-bau', 'images/posts/6.jpg', 'trong một ngày để thai nhi phát triển, hạn chế nguy cơ sẩy thai, sinh non.   Mẹ bầu cần cung cấp đầy đủ vitamin và khoáng chất mỗi ngày, ít nhất 10 loại. Chế độ dinh dưỡng cân đối sẽ giảm nguy cơ thiếu axit folic (vitamin B9) - một thành phần tham gia vào quá trình tạo máu; hạn chế thiếu máu do thiếu sắt. Nếu vi chất dinh dưỡng không đủ, nhất là kẽm sẽ tác động xấu đến sự phát triển và chức năng của hầu hết các tế bào miễn dịch, tế bào T...  Dưới đây là một số vitamin và khoáng chất mẹ bầu cần bổ sung trước và trong khi mang thai.  Canxi  Canxi cần thiết cho quá trình tạo xương của thai nhi, chức năng thần kinh và sự đông máu. Nếu mẹ bầu không bổ sung đủ canxi trong thai kỳ sẽ ảnh hưởng phát triển chiều cao của bé, tăng nguy cơ còi xương, mẹ bầu có khả năng bị tiền sản giật, loãng xương, hư răng... Phụ nữ có thai cần 1.200 mg canxi mỗi ngày, bà mẹ cho con bú là 1.300 mg; cao hơn mức bình thường.  Khẩu phần ăn của mẹ bầu cần nhiều thực phẩm chứa canxi như hải sản (tôm, cua, sò...), bơ, cải xanh, sữa và các sản phẩm từ sữa. Tép ăn cả vỏ, cá nhỏ để nhừ xương có chứa nhiều canxi có lợi cho mẹ bầu. Sữa nên uống hàng ngày. Phụ nữ mang thai có thể bổ sung canxi qua đường uống theo chỉ dẫn của bác sĩ.  Sắt  Sắt tham gia tạo huyết cầu tố (hemoglobin), tạo yếu tố miễn dịch, hô hấp tế bào và hỗ trợ khả năng nhận thức của con người. Nếu thiết sắt dễ dẫn đến thiếu máu, bào thai chậm phát triển, sinh non, tăng tỷ lệ tỷ vong của mẹ và con. Biểu hiện thiếu sắt thời kỳ đầu là thường mệt mỏi, giảm trí nhớ, da kém hồng hào; nặng hơn có thể rụng tóc, khó thở khi gắng sức, tim đập nhanh...  Thời điểm tốt nhất để bổ sung sắt là 3 tháng trước mang thai. Theo khuyến cáo của Tổ chức Y tế Thế giới (WHO), mỗi ngày thai phụ cần bổ sung 30-60 mg sắt mỗi ngày. Mẹ bầu cho con bú hoàn toàn trong 6 tháng đầu, có thể bổ sung sắt giống khi mang thai.  Bà bầu được khuyến khích ăn nhiều loại thực phẩm chứa sắt như thịt bò, thịt lợn, cá thu, trứng, đậu, rau xanh... Nếu uống viên sắt cần cách xa thời điểm uống bổ sung canxi vì sắt rất khó hấp thu và canxi làm cản trở quá trình này; không uống cùng trà, cà phê. Mẹ bầu nên uống sắt khi đói, uống cùng với vitamin C để tăng khả năng hấp thu.', '1', '0', '2020-12-03 02:35:08', '2020-12-03 02:35:41'),
(126, 'Bà bầu không nên ăn khoai tây chiên', 'ba-bau-khong-nen-an-khoai-tay-chien', 'images/posts/3.jpg', 'Thai phụ ăn nhiều khoai tây chiên có thể tăng lượng axit linoleic, gây hại cho bản thân và em bé trong bụng.  Axit linoleic là axit béo omega 6 có trong dầu thực vật và khoai tây chiên. Mỗi ngày, cơ thể chỉ cần một lượng rất nhỏ axit linoleic. Hàm lượng axit linoleic quá cao có thể gây những triệu chứng viêm và nguy cơ bệnh tim, nhất là đối với phụ nữ mang thai.  Khoai tây chiên chứa nhiều axit linoleic nên không tốt .  Các nhà khoa học đã thí nghiệm trên chuột mẹ đang mang thai và phát hiện những thay đổi tiêu cực khi chúng nạp quá nhiều axit linoleic trong 10 tuần. Cụ thể, protein gây viêm trong gan và protein làm tăng co bóp tử cung, hormone tăng trưởng giảm.   Từ kết quả trên, các nhà khoa học khuyến cáo phụ nữ mang thai hạn chế ăn khoai tây chiên để không làm tăng axit linoleic, kiểm soát lượng đường và muối vào cơ thể.', '1', '0', '2020-12-03 02:36:52', '2020-12-03 02:37:45'),
(127, 'Thói quen xấu mẹ bầu tưởng tốt', 'thoi-quen-xau-me-bau-tuong-tot', 'images/posts/4.jpg', 'Thai phụ không nên đi bộ quá mức, giảm cà phê, hạn chế tiếp xúc các thiết bị điện tử và wifi.  Để thai nhi được phát triển tốt nhất, mẹ bầu nên từ bỏ những thói quen không tốt.  Không nên đi bộ quá mức  Đi bộ là một bài tập an toàn với nhiều lợi ích như cải thiện sức khỏe tim mạch và mang lại vóc dáng đẹp, giảm nguy cơ mắc bệnh tiểu đường thai kỳ và tiền sản giật, giảm căng thẳng, hạn chế mất ngủ, giúp co giãn cơ vùng bụng, khung xương chậu tốt để chuyển dạ dễ dàng hơn.  Thai phụ không nên lạm dụng đi bộ quá nhiều mà cần \"đi đúng - đi đủ\".  - Đi đúng  Giữ thẳng người sao cho trọng lượng cơ thể dồn đều trên hai chân. Mắt nhìn thẳng về phía trước, cằm giữ thẳng, song song với mặt đất, toàn thân thư giãn, không đi bộ quá nhanh, đi nhẹ nhàng và chắc chắn.  Không nên đi bộ trong điều kiện khắc nghiệt, trời quá nóng hoặc mưa to, khói bụi nhiều.  Giai đoạn 3 tháng đầu thai kỳ hãy lựa chọn cho mình loại giày thấp, vừa chân, cổ giày cao vừa đủ ôm, không đi nhanh.  Giai đoạn 3 tháng tiếp theo điều chỉnh tư thế mỗi lần đi bộ sao cho hông chuyển động chậm hơn, cẩn thận khi xoay người và giữ dáng người thẳng.  Giai đoạn 3 tháng cuối nên đi bộ cùng người thân, chọn những địa điểm gần nhà, địa hình dễ di chuyển.  - Đi đủ  Nếu trước khi mang thai mẹ bầu đã thường xuyên đi bộ thì hãy tiếp tục thói quen này. Tuy nhiên chỉ nên đi bộ với khoảng thời gian tối đa một giờ và hãy chia thành nhiều lần trong ngày.  Nếu chưa đi bộ trước đi mang thai, các mẹ bầu nên bắt đầu bằng việc đi dạo chậm, 15-30 phút mỗi ngày, một tuần đi 3 lần. Nếu không tham gia các môn thể thao khác trong quá trình mang thai, nên đi bộ 2 lần mỗi ngày.  Lưu ý trong những tháng đầu của thai kỳ, không nên đi bộ quá nhiều vì sẽ có thể dẫn đến tình trạng tử cung gò nhiều, gây những trường hợp không mong muốn như sảy thai và sinh non.', '1', '0', '2020-12-03 02:40:55', '2020-12-03 02:40:55'),
(128, 'Thực phẩm dễ ảnh hưởng đến thai nhi đó', 'thuc-pham-de-anh-huong-den-thai-nhi', 'images/posts/5.jpg', 'Đu đủ, rau bồ ngót, gan động vật, thịt tái sống... bà bầu nên hạn chế ăn.  Chuyên gia dinh dưỡng Nguyễn Mộc Lan cho biết nhiều thực phẩm tốt cho sức khỏe nhưng không thích hợp với bà bầu.   Đu đủ, dứa  Đu đủ có tác dụng kích thích tuyến sữa và cung cấp nhiều đạm cho phụ nữ sau sinh. Tuy nhiên với bà bầu, đu đủ xanh chứa chất papain hoạt động giống như hormone prostaglandin và oxytocin gây co thắt tử cung. Ăn đu đủ xanh thai phụ cũng có thể bị phù và xuất huyết, dẫn tới sinh non.  Dứa cũng có nguy cơ gây sảy thai do chứa bromelain làm mềm, co thắt tử cung. Bác sĩ thường khuyên thai phụ tránh ăn hoặc uống nước dứa.  Rau bồ ngót Rau ngót chứa chất papaverin gây ra hiện tượng giãn cơ trơn của tử cung, có thể dẫn đến sảy thai, tiêu chảy. Bà bầu ăn hơn 30 g lá rau ngót tươi có nguy cơ bị sảy thai. Nếu thai phụ có tiền sử sảy thai liên tục, sinh non hay hiếm muộn không nên ăn canh rau ngót, uống nước ép lá rau ngót chưa qua chế biến.  Ngải cứu  Ngải cứu có công dụng giảm nhức mỏi, lưu thông máu, giảm đau bụng.  Tuy nhiên, một số nghiên cứu cho thấy ăn ngải cứu trong 3 tháng đầu thai kỳ sẽ làm tăng nguy cơ tử cung chảy máu, co thắt, dẫn đến sảy thai hoặc sinh non. Vì vậy, nếu có ý định ăn ngải cứu để dưỡng thai, mẹ bầu nên tham khảo ý kiến bác sĩ chuyên khoa. Bà bầu có tiền sử sảy thai hoặc sinh non không nên ăn nhiều ngải cứu.  Rau má  Rau má có nhiều lợi ích về sức khỏe, thanh nhiệt cơ thể. Do tính lợi tiểu nên rau má giúp hạ huyết áp, điều trị ngộ độc sắn.  Rau má lại là một trong những thực phẩm được cảnh báo với bà bầu. Phụ nữ uống rau má khó có cơ hội thụ thai. Bà bầu uống nước rau má có thể dẫn đến sảy thai, đầy bụng.  Trà, cà phê  Trà và cà phê đều chứa caffeine, giúp cơ thể hưng phấn. Tuy nhiên, mức caffeine cao có thể làm tăng nguy cơ sảy thai ở bà bầu, thai nhi nhẹ cân, đẻ khó. Caffeine có thể tác động quá mức lên hệ tim mạch thai phụ.  Caffeine còn có trong chocolate, một số loại nước giải khát, nước uống tăng lực. Một số thuốc cũng chứa caffeine, các bà bầu cần lưu ý.  Gan động vật và ruột non  Gan động vật tồn dư nhiều chất độc có thể gây hại cho phụ nữ mang thai. Ruột non là bộ phận dễ nhiễm ký sinh trùng đường ruột.  Gan ruột cũng nhiều cholesterol, ảnh hưởng xấu đến thai nhi. Người bình thường cũng được các chuyên gia dinh dưỡng khuyến cáo hạn chế ăn gan, ruột động vật.  Một số loại cá  Cá nhiều đạm và axit béo omega 3 có lợi cho bà bầu. Tuy nhiên, một số loại cá ở tầng sâu có nguy cơ nhiễm kim loại thủy ngân như cá mập, cá ngừ đại dương, cá kiếm, cá thu, cá kình... thai phụ nên hạn chế ăn vì có thể ảnh hưởng đến não bộ thai nhi.  Món tái, sống  Bà bầu nên tránh hoàn toàn những món như sushi, tái, lòng đỏ trứng, bởi nếu chưa được chế biến kỹ chúng dễ nhiễm ký sinh trùng Toxoplasmosis, nguy cơ sảy thai, thai chết lưu...', '1', '0', '2020-12-03 02:42:50', '2020-12-13 08:37:19');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `short_desc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `services`
--

INSERT INTO `services` (`id`, `name`, `image`, `price`, `short_desc`, `content`, `is_delete`, `created_at`, `updated_at`) VALUES
(14, 'Siêu âm thai 3d', 'images/service/2.jpg', 300000, 'tieu de 1', 'Siêu âm thai 3d', '0', '2020-11-20 01:53:17', '2020-11-20 01:53:33'),
(15, 'Siêu âm thai 2d', 'images/service/3.jpg', 250000, 'ádasd', 'Siêu âm thai 2d', '0', '2020-11-20 02:19:55', '2020-12-06 20:33:19'),
(16, 'Siêu âm màu', 'images/service/4.jpg', 200000, 'Siêu âm màu', 'Siêu âm màu.  Siêu âm màu. Siêu âm màu.  Siêu âm màu. Siêu âm màu. Siêu âm màu. Siêu âm màu. Siêu âm màu. Siêu âm màu.', '0', '2020-11-20 02:19:56', '2020-12-14 00:01:53'),
(115, 'Siêu Âm Đặc Biệt', 'images/service/5fd2f12102cce-1d.jpg', 400000, 'Siêu Âm Đặc Biệt', 'Dịch vụ siêu âm đặc biệt Dịch vụ siêu âm đặc biệt Dịch vụ siêu âm đặc biệt', '0', '2020-12-10 08:38:36', '2020-12-14 00:00:39');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `service_history`
--

CREATE TABLE `service_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `histoty_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `setting`
--

CREATE TABLE `setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_footer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maps` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `timeclass`
--

CREATE TABLE `timeclass` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_start` int(191) NOT NULL,
  `time_end` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `timeclass`
--

INSERT INTO `timeclass` (`id`, `name`, `time_start`, `time_end`, `message`, `created_at`, `updated_at`) VALUES
(1, 'ca 10h-10h30', 10, '10.30', NULL, NULL, '2020-12-05 04:10:14'),
(2, 'ca 9h-9h30', 9, '9.30', NULL, NULL, '2020-12-03 00:32:47'),
(3, 'ca 8h-8h30', 8, '8.30', '', NULL, NULL),
(11, 'ca 11h-11h30', 11, '11.30', NULL, NULL, '2020-12-05 04:10:14'),
(12, 'ca 13h-13h30', 13, '13.30', NULL, NULL, '2020-12-03 00:32:47'),
(13, 'ca 14h-14h30', 14, '14.30', '', NULL, NULL),
(14, 'ca 15h-15h30', 15, '15.30', NULL, NULL, '2020-12-05 04:10:14'),
(15, 'ca 16h-16h30', 16, '16.30', NULL, NULL, '2020-12-03 00:32:47'),
(16, 'ca 17h-17h30', 17, '17.30', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT 1,
  `is_delete` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `email`, `phone`, `email_verified_at`, `password`, `role`, `is_delete`, `remember_token`, `created_at`, `updated_at`) VALUES
(30, 'admin@gmail.com', '866238', NULL, '$2y$10$IxHDWTo/ZSfzNkQ.ketmGuV1igcGvjBN5bwyjIVGkv5udsdPahRWC', 100, '0', NULL, NULL, NULL),
(31, 'anh@gmail.com', '12345678', NULL, '$2y$10$t/zv0pwc5mpflzFD1gCp4uUszyOPzcfhVDLwA2CGKS7qnk5X4Jgfq', 100, '0', NULL, '2020-10-12 02:23:31', '2020-10-12 02:23:31'),
(33, 'tuyennguyencp99@gmail.com', '033877788', NULL, '$2y$10$STAenrES3otC9qQssC59p.JlEYzhD7JTobdAtbgK8l8XkLHBb7/b2', 1, '1', NULL, '2020-11-18 02:43:24', '2020-12-03 01:30:59'),
(34, 'minhminh2@gmail.com', '0333333332', NULL, '$2y$10$PDS6JjkEiPqiQEMf99gL9OGOlBHl61Unib7CWQtbHPY6TZfd/i9Sa', 100, '0', NULL, '2020-11-18 05:05:38', '2020-11-18 05:15:42'),
(35, 'minhminh1111@gmail.com', '03333333332', NULL, '$2y$10$6vQQNJ/SwkXtRwHHHPBTPeJjbWwalvIW1ZukTSoY4NiLhEsbOIDPm', 1, '1', NULL, '2020-11-18 05:14:39', '2020-11-18 05:21:47'),
(36, NULL, '03333333', NULL, '$2y$10$JSyjjdDSlq96QkRxEYtPY.irzLkq0HrBHTNeEJLF2Zeh8JQQG3WNC', 1, '1', NULL, '2020-11-21 03:00:40', '2020-12-03 01:30:44'),
(37, 'tuyenngussssyencp99@gmail.com', '0397872946', NULL, '$2y$10$/He6Ax3kSgMiNeq1NkRJ/eKDD7OVPC8LeL5MCWBxfbm8bDWPOu1C.', 1, '0', NULL, '2020-11-21 03:02:03', '2020-11-21 03:02:03'),
(39, 'tuyenngussssyencp99@gmail.com', '0397872942', NULL, '$2y$10$N6ApWSdJAaGbxBeGidpM8.ITn5gmt.5TAPfVfHGhjwZpLRWr/BGFS', 1, '0', NULL, '2020-11-21 03:02:23', '2020-11-21 03:02:23'),
(40, 'tuyenngussssyencp99@gmail.com', '0397872921', NULL, '$2y$10$6jj0Nry.Ae6rvTFyvB775.bd/y8tMtOALZNCLeB6lKhIufAmdQcDm', 1, '0', NULL, '2020-11-21 03:02:30', '2020-11-21 03:02:30'),
(42, 'tuyenngussssyencp99@gmail.com', '11111', NULL, '$2y$10$EDuLAZqzY3RMc5zzIkOmFO.8bFnRPf1y8.wDpSFkrSRoEBSQZG576', 1, '1', NULL, '2020-11-21 03:02:55', '2020-12-03 01:30:47'),
(44, 'ab@gmail.com.vn', '0397872943', NULL, '$2y$10$yBcYfuyE6YeJxZ9W40gC0uRgF91hHxQksejKfvookTFtRXH2rUmSO', 1, '1', NULL, '2020-12-01 06:24:53', '2020-12-03 01:30:48'),
(46, 'minh@gmail.com', '1234432', NULL, '$2y$10$JnDGBzqcd/BBMbc2zxQW3O5uh.YEEmX/mOuCDJXj9hwuPwxWVL.4m', 1, '1', NULL, '2020-12-02 21:29:56', '2020-12-03 01:30:50'),
(47, 'admsssin@a.a', '0397122946', NULL, '$2y$10$lo7reZEYAIgeh7tvaztL9.9ut1zTNH7ENKXiNN8NOHN77MI62OH/G', 1, '1', NULL, '2020-12-03 01:29:39', '2020-12-03 01:31:07'),
(52, '1admin@a.a', '1397872946', NULL, '$2y$10$AGBjZG4mbGQwC.xyAD9tHu7HFAuvA5TklElzQSuXP4/T7MfNjFSz2', 1, '0', NULL, '2020-12-03 01:33:47', '2020-12-03 01:33:47'),
(53, 'aádasddsmin@a.a', '0397272946', NULL, '$2y$10$GTmgnauDd2252iC7Zt.4Z.01ks88FQzPFIeedb5hooRQo5QGW7mGK', 1, '0', NULL, '2020-12-03 01:39:53', '2020-12-03 01:39:53'),
(54, 'anh\\', '0397822946', NULL, '$2y$10$A9qoSyqwAJGUSy3u95Nmy.i6sdIKE1VqDsx5NTLuyiOln3Up/bewi', 1, '0', NULL, '2020-12-08 02:50:05', '2020-12-08 02:50:05'),
(59, 'theanh@gmail.com', '123443', NULL, '$2y$10$.O84noQXhoL/xcMokfTd3.7lFlc/nqljbuetd7R6oPnyfNKEr3FnO', 1, '0', NULL, '2020-12-08 02:50:51', '2020-12-08 02:50:51'),
(60, 'th@gmail.com', '0909090909', NULL, '$2y$10$KMZoQQjunR4SJb2umbP19eSrU9LAlKV8BXpeazK/Re60WReWoFaKa', 1, '0', NULL, '2020-12-08 02:53:27', '2020-12-08 02:53:27'),
(61, 'dsdas@gmail.com', '23873828', NULL, '$2y$10$cu0jOjEHCzVYTDrsuMKv1e0ZfWQ/xW2ZulmtwE463DFIHyto3Bv5.', 1, '0', NULL, '2020-12-08 02:58:08', '2020-12-08 02:58:08'),
(65, 'admin@a.a', '0397872333', NULL, '$2y$10$N9Fvl5mG6QtgSzzcXlZYJOtyWfMS29d06CROHZxdW5mAEErXjNHNG', 1, '0', NULL, '2020-12-09 00:46:22', '2020-12-09 00:46:22'),
(66, 'ab@gmail.com.vn', '0397872111', NULL, '$2y$10$piE27dAwZ5UI.sOIbRDvkePtxlFdM5ljf/ZWR7mo1omf71aoD4wuW', 1, '0', NULL, '2020-12-09 00:47:25', '2020-12-09 00:47:25'),
(67, 'ab@gmail.com.vn', '1197872946', NULL, '$2y$10$s3vpXBUQk6SS3CcvpXLxienhSJMQ/RgLwPnYZAysveRsM0jVChgcq', 1, '0', NULL, '2020-12-09 01:04:30', '2020-12-09 01:04:30'),
(68, 'inpyzxe@gmail.com', '03978729422', NULL, '$2y$10$8YvxTSoYf7qKrj6.B9fw3ew0OAStahmv7W.8WgBTs2dxXzWO8nWVK', 1, '0', NULL, '2020-12-09 01:06:28', '2020-12-09 01:06:28'),
(69, 'tuyennguyencp99@gmail.com', '0397872123', NULL, '$2y$10$zqXj6sXeBHneULEBn4d1Hu/78vbWf6Ezx7eZNSX1mVlI9IOvQ4EQ2', 1, '0', NULL, '2020-12-09 01:11:19', '2020-12-09 01:11:19'),
(70, 'taolatuyen@gmail.com', '0967793300', NULL, '$2y$10$73Xe6sBWY9sILbJfzjT5Huaq78BrJnMpAF2ZkeLmr8XYEWNcDfixO', 1, '0', NULL, '2020-12-09 01:12:46', '2020-12-09 01:12:46'),
(71, 'anhzpt3@gmail.com', '0339315877', NULL, '$2y$10$FcPzMbhmGn.fn0VkvyfsxuDet/dsH1nlgoAJE5vjWmPmICJb.NwkO', 1, '0', NULL, '2020-12-10 02:18:16', '2020-12-10 02:18:16'),
(72, 'anhzpt32@gmail.com', '02222222222', NULL, '$2y$10$oK0eVl5SVNsgSXvUBaHC0.2e9t6VSBsH4AuwcrFb3UYDKOphixHtS', 1, '0', NULL, '2020-12-10 03:22:19', '2020-12-10 03:22:19'),
(73, 'anhssszpt3@gmail.com', '099999999', NULL, '$2y$10$ZjsEj0SaZSwUDlx.Ibzus.wbIteC.KCoryfIluNziv1srBSuESzlG', 1, '0', NULL, '2020-12-10 04:22:23', '2020-12-10 04:22:23'),
(74, 'testt3@gmail.com', '012345612', NULL, '$2y$10$FtWHTs5.EsMAbdTWmtw1meiapCdHL6ncYWuc1ub.PPoOBfn0UStJK', 1, '0', NULL, '2020-12-10 04:23:06', '2020-12-10 04:23:06'),
(75, 'anhzpt3@gmail.com', '12122122212', NULL, '$2y$10$ZYhcTHHh7Lrp3GmBjdzLVuDk2eJflMKASsuZ1xRM3yJ1cThJQ4nn6', 1, '0', NULL, '2020-12-10 04:30:57', '2020-12-10 04:30:57'),
(76, 'truy@gmail.com', '1234555554', NULL, '$2y$10$etOA1tMU3WquYOwW9FkMpuvghAqIrPkz/T4iGHEPRmwelgZ.fjc3u', 1, '0', NULL, '2020-12-10 08:09:19', '2020-12-10 08:09:19'),
(77, 'truy2@gmail.com', '0122333456', NULL, '$2y$10$vY2v0e.m5v5fC0GtHwogteXw.DorO5eXEwu1x4HgtEfUWpL4R3VoW', 1, '0', NULL, '2020-12-10 08:10:25', '2020-12-10 08:10:25'),
(78, 'anhzpt3@gmail.com', '12121212', NULL, '$2y$10$OCwz.7Zh5HBaJE.0Ed1cPebmv7QEY6mXN0SbJ.ho84qeENjCbG9Zi', 1, '0', NULL, '2020-12-10 08:20:37', '2020-12-10 08:20:37'),
(80, 'anhzpt3@gmail.com', '1234444444', NULL, '$2y$10$oQGNpXdcx5wVnho6Hnf2wOuSZcXUZOyqgnN5CWpbHlTQz9HwH7IhO', 1, '0', NULL, '2020-12-10 20:58:19', '2020-12-10 20:58:19'),
(81, 'Minhbede@gmail.com', '123444442222', NULL, '$2y$10$7nN4/PXq6DZI8j.pjgpqk.xvbG02N5dts69TBgGr.lh9F6cPBvr6O', 1, '0', NULL, '2020-12-10 21:00:00', '2020-12-10 21:00:00'),
(82, 'anhzpt3@gmail.com', '3', NULL, '$2y$10$prVCWeJKzgcRASbR1qEXkOjpfNzs0fxZVu4qyZ2.zGWc6PiRJQH8G', 1, '0', NULL, '2020-12-10 21:14:33', '2020-12-10 21:14:33'),
(83, 'tuyenssss3@gmail.com', '012282639939', NULL, '$2y$10$ghay8w/wsOUfT/97WSG2Wu29CPNHO74Kt.1S/DZRyW8zbokbe38DK', 1, '0', NULL, '2020-12-10 23:34:05', '2020-12-10 23:34:05'),
(84, 'tuyenx2@gmail.com', '123333333', NULL, '$2y$10$m/DYolHpnJs9FoxE8jprkeG8USPydiD8e9UrQzixloMD6SgCCZTKK', 1, '0', NULL, '2020-12-10 23:35:12', '2020-12-10 23:35:12'),
(85, 'phunugiauten@gmail.com', '0999999922', NULL, '$2y$10$dEOfoYgxGOhHuPq8GEGWpOP5swAFwZpr6o1qk3Ysu40VCbxTnKFCq', 1, '0', NULL, '2020-12-11 01:28:46', '2020-12-11 01:28:46'),
(86, 'phunugiauten2@gmail.com', '0123456789', NULL, '$2y$10$a.vEP/wcTrbfZqxZ3t4K0uzgJQs1EW94qHZkqsqxymqrGR/MpQdLe', 1, '0', NULL, '2020-12-11 23:15:20', '2020-12-11 23:15:20'),
(87, 'anhzpt3@gmail.com', '01639315877', NULL, '$2y$10$YbGcONI5I0QIZiiRSPxLJeUp5E7seg6HlWHYGtq7ZNMWxNdHOorx6', 1, '0', NULL, '2020-12-12 00:02:02', '2020-12-12 00:02:02'),
(89, 'anhzpt3@gmail.com', '012345677', NULL, '$2y$10$UdykivZ6pdx/yTVAxxE0POV5z7plKRtR5tCePAKgeZA7uV/QxzOCu', 1, '0', NULL, '2020-12-12 00:08:32', '2020-12-12 00:08:32'),
(90, 'anhzpt3@gmail.com', '0123456777', NULL, '$2y$10$.KSYBRVi2cJE6.GgLPX0q.k4t7ISoc0Iayy5fn1fSPMylfd3nMuZi', 1, '0', NULL, '2020-12-12 02:15:11', '2020-12-12 02:15:11'),
(91, 'tuyensss@gmail.com', '0123123123', NULL, '$2y$10$PAgiCpxl4LXeDML4aY0CgORAPL6dhA6hdcnETZL1Ha7GFRb3vMkcS', 1, '0', NULL, '2020-12-13 01:26:24', '2020-12-13 01:26:24'),
(92, 'tiendung@gmail.com', '012344321', NULL, '$2y$10$0khF3zaAch.qOOUXjcI03OvjygOokLLHu1QgQXW0/8zv6hndDkh8W', 1, '0', NULL, '2020-12-13 07:26:52', '2020-12-13 07:26:52'),
(93, 'anhzpt3@gmail.com', '0974842866', NULL, '$2y$10$nm8/4hjoOxF02GEyrbjFFuEW10l9sSjXJtZxsQcLbDOVSzZ8mvsVS', 1, '0', NULL, '2020-12-14 01:43:27', '2020-12-14 01:43:27');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `clinic_schedules`
--
ALTER TABLE `clinic_schedules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `day` (`day`);

--
-- Chỉ mục cho bảng `clinic_schedule_timeclass`
--
ALTER TABLE `clinic_schedule_timeclass`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clinic_schedule_id` (`clinic_schedule_id`),
  ADD KEY `timeclass` (`timeclass_id`);

--
-- Chỉ mục cho bảng `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `histories`
--
ALTER TABLE `histories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `histories_login_ad`
--
ALTER TABLE `histories_login_ad`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `incurred`
--
ALTER TABLE `incurred`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `service_history`
--
ALTER TABLE `service_history`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `timeclass`
--
ALTER TABLE `timeclass`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `clinic_schedules`
--
ALTER TABLE `clinic_schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT cho bảng `clinic_schedule_timeclass`
--
ALTER TABLE `clinic_schedule_timeclass`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;

--
-- AUTO_INCREMENT cho bảng `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT cho bảng `doctor`
--
ALTER TABLE `doctor`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `histories`
--
ALTER TABLE `histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `incurred`
--
ALTER TABLE `incurred`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `order_list`
--
ALTER TABLE `order_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT cho bảng `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT cho bảng `timeclass`
--
ALTER TABLE `timeclass`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
