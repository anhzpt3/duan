 $(document).ready(function() {

        //Khi bàn phím được nhấn và thả ra thì sẽ chạy phương thức này
        $("#formDemo").validate({
          rules: {
            name: {
             required : true,  
           },
           phone: {
            required : true,               
          },
          short_desc:{
            required: true,
          },
          date: {
            required : true,               
          },
          image: {
            required : true,
          },
          clinic_schedule_id: {
            required : true,
          },
          time2: {
            required : true,
          },
          detail: {
            required : true,
          },
          product_name: {
            required : true,
          },
          email: {
            required: true,
            email: true,
          },
          phone: {
            required: true,
            phone: true,
          },
          password: {
           required : true,  
           minlength: 5,
           maxlength: 20,
         },
         noidung: {
           required : true,  
         },
         fullname: {
           required : true,  
         },
         confirm_password: {
           required : true,  
          
         },
         service_id:{
           required: true,
         },
         customer_id:{
           required: true,
         },
         price:{
          required: true,
        },
        content:{
          required: true,
        },
        oldpass:{
          required: true,
        },
        cppassword:{
          required: true,

        },
       },
       messages: {
        cppassword:{
          required:"Không được để trống"
        },
        oldpass:{
          required:"Không được để trống"
        },
        price:{
            required:"Vui lòng nhập giá"
        },
        content:{
          required:"Vui lòng nhập nội dung"
      },
        short_desc:{
          required: "Vui lòng nhập mô tả"
        },
        customer_id:{
          required:"vui lòng chọn khách hàng",
        },
        service_id: {
          required: "Vui lòng chọn dịch vụ",
        },
        name: {
          required:  "Vui lòng nhập tên",

        },
        phone: {
          required:  "Vui lòng nhập số điện thoại",

        },
        date: {
          required:  "Vui lòng chọn ngày sinh",

        },
        image: {
          required:  "Vui lòng chọn file",
        },
        clinic_schedule_id: {
          required:  "Vui lòng chọn ngày khám",              
        },
        time2: {
          required:  "Vui lòng chọn ca khám",              
        },
        product_name: {
          required:  "Vui lòng nhập tên sản phẩm",              
        },
        detail: {
          required:  "Vui lòng nhập mô tả sản phẩm",              
        },
        email: {
          required: "Vui lòng nhập email",
          email: "Vui lòng nhập đúng dạng email",
        },
        password: {
         required : "Vui lòng nhập mật khẩu",
         minlength: "Mật khẩu phải từ 6 đến 20 kí tự",
         maxlength: "Mật khẩu phải từ 6 đến 20 kí tự",
       },
       noidung: {
         required : "Vui lòng nhập nội dung",
       },
       fullname: {
         required : "Vui lòng nhập tên",
       },
       confirm_password: {
           required : "Vui lòng xác nhận mật khẩu",  
           equalTo: "Xác nhận sai mật khẩu", 
         },
     }
   });
        
      });

 $('.btn-remove').on('click', function(){
  var removeUrl = $(this).attr('linkurl');
        // var conf = confirm('Bạn có chắc chắn muốn xoá danh mục này không?');
        // if(conf){
        //  window.location.href = removeUrl;
        // }
        swal({
          title: "Cảnh báo",
          text: "Bạn có chắc chắn muốn xoá danh mục này không?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            window.location.href = removeUrl;
          } 
        });
      });