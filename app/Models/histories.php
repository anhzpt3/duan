<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class histories extends Model
{
    protected $table ='histories';
    protected $fillable = ['customer_id','order_list_id','content','is_delete'];

    public function order() {
        return $this->hasOne(order_list::class, "id", "order_list_id");
    }

    public function customer() {
        return $this->hasOne(customer::class, "id", "customer_id");
    }

    // public function schedule() {
    //     return $this->hasOne(ClinicSchedule::class, "id", "clinic_schedule_id");
    // }
}
