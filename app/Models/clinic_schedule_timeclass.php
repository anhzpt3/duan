<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class clinic_schedule_timeclass extends Model
{
    protected $table = "clinic_schedule_timeclass";
    protected $fillable = [
        "clinic_schedule_id",
        "timeclass_id",
        "is_booking",
    ];
    public function time() {
        return $this->hasOne(time_calendar::class, "id", "timeclass_id");
    }
}
