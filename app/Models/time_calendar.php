<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class time_calendar extends Model
{
    protected $table ='timeclass';
    protected $fillable = ['name','time_start','time_end','message'];

    public function times()
    {
        return $this->belongsToMany(ClinicSchedule::class, 'clinic_schedule_timeclass', 'timeclass_id', 'clinic_schedule_id')->withPivot(["is_booking"])->withTimestamps();
    }
}
