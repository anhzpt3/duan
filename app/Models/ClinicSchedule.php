<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ClinicSchedule extends Model
{
    protected $table = "clinic_schedules";

    protected $fillable = [
        "name",
        "day",
    ];

    // public function service() {
    //     return $this->hasOne(services::class, "id", "service_id");
    // }

    public function times()
    {
        return $this->belongsToMany(time_calendar::class, 'clinic_schedule_timeclass', 'clinic_schedule_id', 'timeclass_id')->withPivot(["is_booking"])->withTimestamps();
    }
}
