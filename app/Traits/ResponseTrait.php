<?php


namespace App\Traits;


trait ResponseTrait
{
    public function setResponse($data = null, $message = "Success", $code = 0) {
        return response()->json([
            "error_code" => $code,
            "message" => $message,
            "data" => $data
        ]);
    }
}
