<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class AdminRole
{
    /**
     * quản trị viên role >=100 mới có thể thực hiện tiếp request
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        if(Auth::check() && Auth::user()->role >= 100){
            
            return $next($request);
        }
        return redirect()->route('forbidden');
    }
}