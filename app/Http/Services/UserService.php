<?php
namespace App\Http\Services;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Hash;

class UserService{
    function __construct(User $user){
        $this->user = $user;
    }

    public function show($kw){
        if(!$kw || empty($kw)){
            return $this->user->where('is_delete','=',0)->orderBy('id','desc')->paginate(10);;
		}else{
            $data =  $this->user->where([['phone', 'like', "%$kw%"],
                                ['is_delete', '=', '0'],])
                            ->orderBy('id','desc')
                            ->paginate(10);
            $data->withPath("?keyword=$kw");
            return $data;
		}
    }

    public function getAll(){
        return $this->user->where('is_delete','=',0)->get();
    }

    public function store($data){
        if ($data->password == "" ||$data->password == null) {
            $data->password = 123456;
        }
		$data->password = hash::make($data->password);
        return $data->save();
    }

    public function findnew_id(){
        return $this->user->where([['id', '>', 0],['is_delete', '=', '0'],])
                             ->orderBy('id','desc')
                             ->first();
    }

    public function find($id){
		return $this->user->find($id);
    }
}
?>