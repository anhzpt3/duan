<?php
namespace App\Http\Services;

use App\Models\doctor;
use Illuminate\Support\Facades\DB;

class DoctorService{
    function __construct(doctor $doctor){
        $this->doctor = $doctor;
    }

    public function show($kw){
        if(!$kw || empty($kw)){
              return $this->doctor->where('is_delete','=',0)->orderBy('id','desc')
                                  ->paginate(10);
            }else{
              $data =  $this->doctor->where([['name', 'like', "%$kw%"],
                                  ['is_delete', '=', '0'],])
                                  ->orWhere([['email', 'like', "%$kw%"],
                                    ['is_delete', '=', '0'],])
                                  ->orderBy('id','desc')
                                  ->paginate(10);
              return $data;
            }
      }

    public function getAll(){
        return $this->doctor->where('is_delete','=',0)->get();
    }

    public function find($id){
		return $this->doctor->find($id);
    }
    
    public function store($data){
        if(!empty($data->image)){
          $filename = $data->image->getClientOriginalName();
          $filename = str_replace(' ', '-', $filename);
          $filename = uniqid() . '-' . $filename;
          $path = $data->image->storeAs('doctor', $filename);
          $data->image = "images/$path";
        }
        return $data->save();
    }

    public function update($data,$img){
        if(!empty($img)){
            $filename = $img->getClientOriginalName();
            $filename = str_replace(' ', '-', $filename);
            $filename = uniqid() . '-' . $filename;
            $path = $img->storeAs('doctor', $filename);
            $data->feature_image = "images/$path";
        }
        return  $data->save();
    }
    
    public function checkExists($id){
        return $this->doctor->where([['user_id', '=', $id],['is_delete', '=', '0'],])
        ->exists();
    }

    public function findUser_id($id){
		return $this->doctor->where([['user_id', '=', $id],['is_delete', '=', '0'],])->first();
    }

    public function findnew_id(){
        return $this->doctor->where([['id', '>', 0],['is_delete', '=', '0'],])
                             ->orderBy('id','desc')
                             ->first();
    }

    
}
?>