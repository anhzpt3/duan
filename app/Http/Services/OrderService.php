<?php
namespace App\Http\Services;

use App\Models\order_list;
use App\Models\ClinicSchedule;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class OrderService{
    function __construct(order_list $order_list,ClinicSchedule $clinic_schedules){
        $this->order_list = $order_list;
        $this->clinic_schedules = $clinic_schedules;
    }

    public function show_calendar_day($day){
        $now = Carbon::now('Asia/Ho_Chi_Minh')->toDateString();
        if(!$day || empty($day) || $day == null){
            $schedule = $this->clinic_schedules->where('day','=',$now)->get();
            $array = array();
            foreach($schedule as $key =>$value){
            $array[] = $value->id;
            }
            $data = $this->order_list->whereIn('clinic_schedule_id',$array)
                                    ->where(function($query){
                                        $query->where([['is_delete','0'],['status',1],])
                                            ->orwhere([['is_delete','0'],['status',2],])
                                            ->orwhere([['is_delete','0'],['status',4],]);
                                    })
                                    ->paginate(10);
            return $data;
        }else{
            $schedule = $this->clinic_schedules->where('day','=',$day)->get();
            $array = array();
            foreach($schedule as $key =>$value){
            $array[] = $value->id;
            }
            $data = $this->order_list->whereIn('clinic_schedule_id',$array)
                                    ->where(function($query){
                                        $query->where([['is_delete','0'],['status',1],])
                                            ->orwhere([['is_delete','0'],['status',2],])
                                            ->orwhere([['is_delete','0'],['status',4],]);
                                    })
                                    ->paginate(10);
            return $data;
        }
    }

    public function show_calendar7day(){
        $now = Carbon::now('Asia/Ho_Chi_Minh')->toDateString();
        $sevendaylate =  Carbon::now('Asia/Ho_Chi_Minh')->addDay(7)->toDateString();
        $schedule = $this->clinic_schedules->whereBetween('day',[$now, $sevendaylate])->get();
            $array = array();
            foreach($schedule as $key =>$value){
                $array[] = $value->id;
            }
            $data = $this->order_list->whereIn('clinic_schedule_id',$array)
                                    ->where(function($query){
                                        $query->where([['is_delete','0'],['status',1],])
                                            ->orwhere([['is_delete','0'],['status',2],]);
                                    })
                                    ->get();
    return $data;

    }
    public function show_calendar($day){
        $now = Carbon::now('Asia/Ho_Chi_Minh')->toDateString();
        $daylate =  Carbon::now('Asia/Ho_Chi_Minh')->addDay($day)->toDateString();
        if(!$day || empty($day)){
              return $this->order_list->where([['status', 1],['is_delete','0'],])
                                      ->orWhere([['is_delete', '=', '0'],['status', 2],])
                                      ->orderBy('id','desc')
                                      ->paginate(10);
        }elseif($day == 'now'){
            $schedule = $this->clinic_schedules->where('day','=',$now)->get();
            $array = array();
            foreach($schedule as $key =>$value){
            $array[] = $value->id;
            }
            $data = $this->order_list->whereIn('clinic_schedule_id',$array)
                                    ->where(function($query){
                                        $query->where([['is_delete','0'],['status',1],])
                                            ->orwhere([['is_delete','0'],['status',2],]);
                                    })
                                    ->paginate(10);
            return $data;
        }else{
            $schedule = $this->clinic_schedules->whereBetween('day',[$now, $daylate])->get();
            $array = array();
            foreach($schedule as $key =>$value){
                $array[] = $value->id;
            }
            $data = $this->order_list->whereIn('clinic_schedule_id',$array)
                                    ->where(function($query){
                                        $query->where([['is_delete','0'],['status',1],])
                                            ->orwhere([['is_delete','0'],['status',2],]);
                                    })
                                    ->paginate(10);
            return $data;
        }
    }

    public function show_order($kw,$day){
            $now = Carbon::now('Asia/Ho_Chi_Minh')->toDateString();
            $sevendaylate =  Carbon::now('Asia/Ho_Chi_Minh')->addDay(7)->toDateString();
            $daylate =  Carbon::now('Asia/Ho_Chi_Minh')->addDay($day)->toDateString();
            if($kw){
                $data =  $this->order_list->orderBy('id','desc')
                                          ->paginate(10);
                return $data;
            }else{
                return $this->order_list->where([['status', 0],['is_delete','0'],])
                                        ->orderBy('id','desc')
                                        ->paginate(10);                        
                
            }
    }
    public function showIndexByCustomerID($id){
        return $this->order_list->where([['customer_id', $id],['is_delete','0'],])->orderBy('id','desc')->paginate(6);
    }
    public function getAll_order(){
        return $this->order_list->where([['status', 0],['is_delete','0'],])->get();
    }
    public function getAll_calendar(){
        return $this->order_list->where([['status', 1],['is_delete','0'],])->orwhere([['status', 2],['is_delete','0'],])->get();
    }

    public function find($id){
		    return $this->order_list->find($id);
    }

    public function store($data){
      return $data->save();
    }
    
   
}
?>