<?php
namespace App\Http\Services;

use App\Models\histories;
use Illuminate\Support\Facades\DB;

class HistoriesService{
    function __construct(histories $histories){
        $this->histories = $histories;
    }

    public function show($kw,$showMore){
        if(!$kw || empty($kw)){
            if ($showMore > 0) {
                return $this->histories->where('is_delete','=',0)->orderBy('id','desc')->paginate($showMore);
            }else{
                return $this->histories->where('is_delete','=',0)->orderBy('id','desc')->paginate(10);
            }      
        }else{
            $data =  $this->histories->where([['name', 'like', "%$kw%"],
                                  ['is_delete', '=', '0'],])
                                  ->orWhere([['cmt', 'like', "%$kw%"],
                                    ['is_delete', '=', '0'],])
                                  ->orWhere([['email', 'like', "%$kw%"],
                                    ['is_delete', '=', '0'],])
                                  ->orderBy('id','desc')
                                  ->paginate(10);
            return $data;
        }
      }

    public function getAll(){
        return $this->histories->where('is_delete', '0')->get();
    }

    public function store($data){
        return $data->save();
    }
    public function findByCustomerId($customer_id){
        return $this->histories->where([['customer_id', $customer_id],
                                    ['is_delete', '=', '0'],])->get();
    }
    public function find($id){
        return $this->histories->find($id);
    }
    public function findnew_id(){
        return $this->histories->where([['id', '>', 0],['is_delete', '=', '0'],])
                             ->orderBy('id','desc')
                             ->first();
    }

    public function apiv2($id){
        return $this->histories->where('order_list_id',$id)->first();
    }

    // public function delete($id){
    //     $data = $this->histories->find($id);
    //     $data->status = -1;
    //     $data->save();
    // }
}