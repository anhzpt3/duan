<?php
namespace App\Http\Services;

use App\Models\customer;
use Illuminate\Support\Facades\DB;

class CustomerService{
    function __construct(customer $customer){
        $this->customer = $customer;
    }

    public function show($kw,$showMore){
      if(!$kw || empty($kw)){
          if ($showMore > 0) {
              return $this->customer->where('is_delete','=',0)->orderBy('id','desc')->paginate($showMore);
          }else{
              return $this->customer->where('is_delete','=',0)->orderBy('id','desc')->paginate(10);
          }      
		  }else{
            $data =  $this->customer->where([['name', 'like', "%$kw%"],
                                ['is_delete', '=', '0'],])
                                ->orWhere([['cmt', 'like', "%$kw%"],
                                  ['is_delete', '=', '0'],])
                                ->orWhere([['email', 'like', "%$kw%"],
                                  ['is_delete', '=', '0'],])
                                ->orderBy('id','desc')
                                ->paginate(10);
            return $data;
		  }
    }
    public function getAll(){
        return $this->customer->where('is_delete','=',0)->get();
    }

    public function find($id){
		    return $this->customer->find($id);
    }

    public function store($data){
      return $data->save();
    }
    
    public function checkExists($id){
        return $this->customer->where([['user_id', '=', $id],['is_delete', '=', '0'],])
        ->exists();
    }

    public function findUser_id($id){
		    return $this->customer->where([['user_id', '=', $id],['is_delete', '=', '0'],])->first();
    }
    public function findnew_id(){
      return $this->customer->where([['id', '>', 0],['is_delete', '=', '0'],])
                           ->orderBy('id','desc')
                           ->first();
  }
}
?>