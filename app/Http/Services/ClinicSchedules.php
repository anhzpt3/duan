<?php
namespace App\Http\Services;

use App\Models\ClinicSchedule;
use App\Models\clinic_schedule_timeclass;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ClinicSchedules{
    function __construct(ClinicSchedule $clinicSchedule, clinic_schedule_timeclass $clinic_schedule_timeclass){
        $this->clinicSchedule = $clinicSchedule;
        $this->clinic_schedule_timeclass = $clinic_schedule_timeclass;
    }
    public function show($kw){
        $now = Carbon::now('Asia/Ho_Chi_Minh')->toDateString();
        $sevendaylate =  Carbon::now('Asia/Ho_Chi_Minh')->addDay(7)->toDateString();
        $daylate =  Carbon::now('Asia/Ho_Chi_Minh')->addDay($kw)->toDateString();
        if(!$kw || empty($kw)){
            $data= $this->clinicSchedule->whereBetween('day',[$now, $sevendaylate])
                                        ->orderBy('day','asc')
                                        ->paginate(8);
            return $data;
		}else{
            $data= $this->clinicSchedule->whereBetween('day',[$now, $daylate])
                                        ->orderBy('day','asc')
                                        ->paginate($kw);
                                       
            return $data;
		}
    }
    public function findClinicScheduleTimeByid($id){
        return $this->clinic_schedule_timeclass->where('clinic_schedule_id',$id)->get();
    }

    public function findClinicScheduleTimeBooking($id){
        return $this->clinic_schedule_timeclass->where([['clinic_schedule_id', $id],['is_booking','1'],])->get();
    }
    public function store($data){
       return $data->save();
    }
    public function findnew_id(){
        return $this->clinicSchedule->where('id', '>', 0)
                             ->orderBy('id','desc')
                             ->first();
    }

    public function findclass_id($id){
        return $this->clinic_schedule_timeclass->find($id);                        
    }

    public function get_7day(){
        $now = Carbon::now('Asia/Ho_Chi_Minh')->toDateString();
        $sevendaylate =  Carbon::now('Asia/Ho_Chi_Minh')->addDay(6)->toDateString();
        $data= $this->clinicSchedule->whereBetween('day',[$now, $sevendaylate])
                                        ->orderBy('day','asc')->get();
        return $data;
    }
    public function get_30day(){
        $now = Carbon::now('Asia/Ho_Chi_Minh')->toDateString();
        $sevendaylate =  Carbon::now('Asia/Ho_Chi_Minh')->addDay(30)->toDateString();
        $data= $this->clinicSchedule->whereBetween('day',[$now, $sevendaylate])
                                        ->orderBy('day','desc')->get();
        return $data;
    }

    public function get_clinicSchedules_class_byID($id){
       return $this->clinic_schedule_timeclass->where([['clinic_schedule_id', $id],['is_booking', '0'],])->orderBy('timeclass_id','asc')->get();
    }
    public function unbooking($time_id,$clinic_schedule_id){
        $data = $this->clinic_schedule_timeclass->where([['timeclass_id', $time_id],['clinic_schedule_id', $clinic_schedule_id],])->first();
        return $data;
    }
}