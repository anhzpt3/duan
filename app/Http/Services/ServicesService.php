<?php
namespace App\Http\Services;

use App\Models\services;
use Illuminate\Support\Facades\DB;

class ServicesService{
    function __construct(services $services){
        $this->services = $services;
    }

    public function show($kw){
        if(!$kw || empty($kw)){
            return $this->services->where('is_delete','=',0)->orderBy('id','desc')->paginate(10);
		}else{
            $data =  $this->services->where([['name', 'like', "%$kw%"],
                                ['is_delete', '=', '0'],])
                            ->orderBy('id','desc')
                            ->paginate(10);
            $data->withPath("?keyword=$kw");
            return $data;
		}
    }

    public function getAll(){
        return $this->services->where('is_delete','=',0)->get();
    }

    public function find($id){
        return $this->services->find($id);
    }

    public function store($data){
        if(!empty($data->image)){
          $filename = $data->image->getClientOriginalName();
          $filename = str_replace(' ', '-', $filename);
          $filename = uniqid() . '-' . $filename;
          $path = $data->image->storeAs('service', $filename);
          $data->image = "images/$path";
        }
        return $data->save();
    }

    public function update($data,$img){
        if(!empty($img)){
            $filename = $img->getClientOriginalName();
            $filename = str_replace(' ', '-', $filename);
            $filename = uniqid() . '-' . $filename;
            $path = $img->storeAs('service', $filename);
            $data->image = "images/$path";
        }
        return  $data->save();
    }
    public function delete($id){
		$data = $this->services->find($id);
        $data->is_delete = 1;
        $data->save();
	}
}
?>