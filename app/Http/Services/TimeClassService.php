<?php
namespace App\Http\Services;

use App\Models\time_calendar;
use Illuminate\Support\Facades\DB;

class TimeClassService{
    function __construct(time_calendar $time_calendar){
        $this->time_calendar = $time_calendar;
    }
    public function getAll(){
        return $this->time_calendar->orderBy('time_start','asc')->get();
    }

    public function show(){
            return $this->time_calendar->orderBy('time_start','asc')->paginate(10);
    }

    public function find($id){
        return $this->time_calendar->find($id);
    }
    public function store($data){
        return $data->save();
    }
    public function findnew_id(){
        return $this->user->where([['id', '>', 0],['is_delete', '=', '0'],])
                        ->orderBy('id','desc')
                        ->first();
    }

    public function destroy($id){
        $data = $this->time_calendar->where('id',$id)->delete();
        if ($data == 1){
            return true;
        }else {
            return false;
        }
    }
}