<?php
namespace App\Http\Services;

use App\Models\posts;
use Illuminate\Support\Facades\DB;

class PostsService{
    function __construct(posts $posts){
        $this->posts = $posts;
    }

    public function show($kw){
        if(!$kw || empty($kw)){
            return $this->posts->where('is_delete','=',0)->orderBy('id','desc')->paginate(6);
		}else{
            $data =  $this->posts->where([['title', 'like', "%$kw%"],
                                ['is_delete', '=', '0'],])
                            ->orderBy('id','desc')
                            ->paginate(10);
            $data->withPath("?keyword=$kw");
            return $data;
		}
    }

    public function show4post(){
        return $this->posts->where('is_delete','=',0)
                            ->orderBy('id', 'desc')
                            ->limit(4)
                            ->get();
    }

    public function getAll(){
        return $this->posts->where('is_delete','=',0)->get();
    }

    public function find($id){
        return $this->posts->find($id);
    }

    public function store($data){
        if(!empty($data->feature_image)){
          $filename = $data->feature_image->getClientOriginalName();
          $filename = str_replace(' ', '-', $filename);
          $filename = uniqid() . '-' . $filename;
          $path = $data->feature_image->storeAs('posts', $filename);
          $data->feature_image = "images/$path";
        }
        return $data->save();
    }

    public function update($data,$img){
        if(!empty($img)){
            $filename = $img->getClientOriginalName();
            $filename = str_replace(' ', '-', $filename);
            $filename = uniqid() . '-' . $filename;
            $path = $img->storeAs('posts', $filename);
            $data->feature_image = "images/$path";
        }
        return  $data->save();
    }

}

?>