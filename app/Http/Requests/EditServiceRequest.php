<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     // * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100|min:5',
            'price' => 'required|digits_between:3,12',
            'short_desc' => 'required|min:10',
            'content' => 'required|min:60',
            'image' => 'file|mimes:jpeg,png,jpg',
        ];
    }
    public function messages(){
        return[
            'name.required' => "Tên dịch vụ: Nhập dữ liệu",
            'name.max' => "Tên dịch vụ: Không quá 100 kí tự",
            'name.min' => "Tên dịch vụ: Nhập nhiều hơn 5 kí tự",
            'price.required' => "Giá: Hãy nhập giá",
            'price.digits_between' => "Giá: Nhiều hơn 3 số và ít hơn 12 số",
            'short_desc.required' => "Mô tả ngắn: Nhập dữ liệu",
            'short_desc.min' => "Mô tả ngắn: Nhập nhiều hơn 10 kí tự",
            'content.required' => "Chi tiết: Nhập dữ liệu",
            'content.min' => "Chi tiết: Nhập nhiều hơn 60 kí tự",
            'image.file' => "Ảnh: Hãy chọn file ảnh",
            'image.mimes' => "Ảnh: Chọn ảnh có định dạng jpeg - png - jpg",
        ];
    }
}
