<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     // * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:150|min:10',
            'slug' => 'required|unique:posts',
            'feature_image' => 'required|file|mimes:jpeg,png,jpg',
            'content' => 'required|min:150',
        ];
    }
    public function messages(){
        return[
            'title.required' => "Tiêu đề: Hãy nhập tiêu đề",
            'title.max' => "Tiêu đề: Không quá 150 kí tự",
            'title.min' => "Tiêu đề: Nhập nhiều hơn 10 kí tự",
            'slug.unique' => "Đường dẫn: Dữ liệu đã tồn tại",
            'slug.required' => "Đường dẫn: Hãy nhập đường dẫn",
            'content.required' => "Nội dung: Hãy nhập nội dung",
            'content.min' => "Nội dung: Nhập nhiều hơn 150 kí tự",
            'feature_image.required' => "Ảnh: Hãy chọn ảnh",
            'feature_image.file' => "Ảnh: Hãy chọn file ảnh",
            'feature_image.mimes' => "Ảnh: Chọn ảnh có định dạng jpeg - png - jpg",
        ];
    }
}
