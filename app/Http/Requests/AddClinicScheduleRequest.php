<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddClinicScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     // * @return array
     */
    public function rules()
    {
        return [
            // 'name' => 'required|max:60|min:5',
            'day' => 'required',
        ];
    }
    public function messages(){
        return[
            // 'name.required' => "Tên: Hãy nhập dữ liệu",
            // 'name.max' => "Tên dịch vụ: Không quá 60 kí tự",
            // 'name.min' => "Tên dịch vụ: Nhập nhiều hơn 5 kí tự",
            'day.required' => "Ngày: Hãy chọn ngày",
        ];
    }
}