<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddDoctorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     // * @return array
     */
    public function rules()
    {
        return [
            'name' => "required",
            'email' => 'required',
            'phone' => 'unique:users|digits_between:8,11|unique:users,doctor',     
            'info' => 'required|min:20',
            'image' => 'required|file|mimes:jpeg,png,jpg',
        ];
    }
    public function messages(){
        return[
            'name.required' => "Tên bác sĩ: Hãy nhập tên bác sĩ",
            'email.required' => "Email: Hãy nhập Email....",
            'email.unique' => "Email: Dữ liệu đã tồn tại",
            'phone.digits_between' => "Số điện thoại: Nhiều hơn 8 số và ít hơn 11 số",
            'phone.unique' => "Số điện thoại: Dữ liệu đã tồn tại",
            'info.required' => "Chi tiết: Nhập dữ liệu",
            'info.min' => "Chi tiết: Nhập nhiều hơn 20 kí tự",
            'image.required' => "Ảnh: Hãy chọn ảnh",
            'image.file' => "Ảnh: Hãy chọn file ảnh",
            'image.mimes' => "Ảnh: Chọn ảnh có định dạng jpeg - png - jpg",     
        ];
    }
}
