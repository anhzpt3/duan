<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddOderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     // * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'digits_between:8,11|unique:customer,phone|unique:users',
            'date' => 'required',
            'cmt' => 'min:9|max:12',
            'service_id' => 'required',    
            'clinic_schedule_id' => 'required',     
        ];
    }
    public function messages(){
        return[
            'name.required' => "Tên: Hãy nhập tên khách hàng",
            'email.required' => "Email: Hãy nhập Email....",
            'email.email' => "Email: Nhập đúng định dạng email",
            'phone.digits_between' => "Số điện thoại: Nhiều hơn 8 số và ít hơn 11 số",
            'phone.unique' => "Số điện thoại: Số điện thoại trùng lặp",
            'date.required' => "Ngày sinh: Hãy nhập ngày sinh",
            'cmt.min' => "Chứng minh thư: Số chứng minh thư có 9 số",
            'cmt.max' => "Chứng minh thư: Số chứng minh thư hoặc thẻ căn cước có 12 số",
            'service_id.required' => "Dịch vụ: hãy chọn dịch vụ",
            'clinic_schedule_id.required' => "Ngày khám: hãy chọn ngày khám",
        ];
    }
}
