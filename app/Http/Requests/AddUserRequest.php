<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     // * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:users',
            'phone' => 'unique:users|digits_between:8,11',
            'role' => 'required',
            'password' => 'min:6|required_with:re-password|same:re-password',
            're-password' => 'min:6',         
        ];
    }
    public function messages(){
        return[
            'email.required' => "Email: Hãy nhập Email....",
            'email.unique' => "Email: Dữ liệu đã tồn tại",
            'phone.digits_between' => "Số điện thoại: Nhiều hơn 8 số và ít hơn 11 số",
            'phone.unique' => "Số điện thoại: Dữ liệu đã tồn tại",
            'password.required_with' => "Nhập mật khẩu: mật khẩu không trùng khớp",
            'password.min' => "Nhập mật khẩu: cần nhập từ 6 kí tự trở lên",
            'password.same' => "Nhập lại mật khẩu: Mật khẩu không chính xác",
            're-password.min' => "Nhập lại mật khẩu: cần nhập từ 6 kí tự trở lên",
            'role.required' => "Quyền hạn: Hãy chọn quyền hạn",
        ];
    }
}
