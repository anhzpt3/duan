<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     */
    public function rules()
    {
        return [
            'name' => 'required|min:6',
            'email' => 'required|email',
            'phone' => 'digits_between:8,11|unique:customer,phone|unique:users',
            'date' => 'required',
            'cmt' => 'required|min:9|max:12|unique:customer',
        ];
    }
    public function messages(){
        return[
            'name.required' => "Tên khách hàng: Nhập dữ liệu",
            'name.min' => "Tên khách hàng: Nhập nhiều hơn 6 kí tự",
            'phone.digits_between' => "Số điện thoại: Nhiều hơn 9 số và ít hơn 12 số",
            'phone.unique' => "Số điện thoại: Số điện thoại trùng lặp",
            'email.required' => "Email: Nhập dữ liệu",
            'email.email' => "Email: Nhập đúng định dạng email",
            'date.required' => "Ngày sinh: Hãy chọn ngày",
            'cmt.required' => "Chứng minh thư: Hãy nhập số chứng minh thư",
            'cmt.min' => "Chứng minh thư: Số chứng minh thư có 9 số",
            'cmt.max' => "Chứng minh thư: Số chứng minh thư hoặc thẻ căn cước có 12 số",
            'cmt.unique' => "Chứng minh thư: Số chứng minh thư hoặc thẻ căn cước trùng lặp",
        ];
    }
}
