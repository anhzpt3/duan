<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\OrderService;
use App\Http\Services\HistoriesService;
use App\Http\Services\CustomerService;
use App\Http\Services\ClinicSchedules;
use App\Models\ClinicSchedule;
use App\Models\clinic_schedule_timeclass;
use App\Models\histories;
use App\Models\order_list;
use Session;


class HistoriesController extends Controller
{
    public function __construct(
        CustomerService $customerService,
        OrderService $orderService,
        HistoriesService $historiesService,
        ClinicSchedules $clinicSchedules
      ){
        $this->customerService = $customerService;
        $this->orderService = $orderService;
        $this->historiesService = $historiesService;
        $this->clinicSchedules = $clinicSchedules;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kw = $request->keyword;
        $showMore = (int)$request->showMore;
        $histories = $this->historiesService->show($kw,$showMore);
        $total = $this->historiesService->getAll()->count();
        return view('backend.histories.index', compact('histories','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $model =  $this->orderService->find($id);
        $schedule = $this->clinicSchedules->get_30day();
        $customer = $this->customerService->find($model->customer_id);
        return view('backend.histories.create',compact('model','customer','schedule'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id){
            $data = new histories;
            $data->fill($request->all());
            $check = $this->historiesService->store($data);
            $beta = $this->orderService->find($id);
            $beta->status = 2;
            $beta=$this->orderService->store($beta);
            if ($check == true && $request->clinic_schedule_id !== 0 && (int)$request->time){
                $customer_id = $request->customer_id;
                $data2 = $this->clinicSchedules->findclass_id($request->time);
                $data2->is_booking = 1;
                $this->clinicSchedules->store($data2);
                $order = new order_list;
                $order->fill($request->all());
                $order->time_id = $data2->timeclass_id;
                $order->customer_id = $customer_id;
                $order->message = 'lịch tái khám ';
                $order->status = 1 ;
                $check4 = $this->orderService->store($order);
                Session::flash('success','    Thêm hồ sơ và đặt lịch tái khám thành công !');
                return redirect()->route('calendar');
            }
            Session::flash('success','   Thêm hồ sơ thành công !');
            return redirect()->route('calendar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $histories = $this->historiesService->find($id);
        $customer = $this->customerService->find($histories->customer_id);
        return view('backend.histories.update', compact('histories','customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $histories = $this->historiesService->find($id);
        $histories->fill($request->all());
        $check = $this->historiesService->store($histories);
        if ($check == true) {
            Session::flash('success','  Sửa hồ sơ thành công !');
            return redirect()->route('histories');
        }else{
            Session::flash('success','   Sửa hồ sơ thất bại!');
            return redirect()->route('histories');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
