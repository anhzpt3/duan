<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\CustomerService;
use App\Http\Services\UserService;
use App\Models\customer;
use App\Models\User;
use App\Http\Requests\AddCustomerRequest;
use App\Http\Requests\EditCustomerRequest;
use Session;

class CustomerController extends Controller
{
    public function __construct(
        CustomerService $customerService,
        UserService $userService
      ){
        $this->customerService = $customerService; 
        $this->userService = $userService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kw = $request->keyword;
        $showMore = (int)$request->showMore;
        $customers = $this->customerService->show($kw,$showMore);
        $total = $this->customerService->getAll()->count();
        return view('backend.customer.index', compact('customers','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddCustomerRequest $request)
    {
        $data1 = new User;
        $data1->fill($request->all());
        $data1->role == 1;
        $checkUsers = $this->userService->store($data1);
        if ($checkUsers == true) {
            $newUser = $this->userService->findnew_id();
            $data2 = new customer;
            $data2->fill($request->all());
            $data2->user_id = $newUser->id;
            $check = $this->customerService->store($data2);
            if ($check == true) {
                Session::flash('success',' Thêm mới khách hàng thành công !');
                return redirect()->route('customer');
            }
        }else {
            Session::flash('erroisMess','Thêm mới thất bại =((');
            return redirect()->route('customer.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
