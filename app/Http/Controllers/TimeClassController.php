<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\TimeClassService;
use App\Http\Services\ClinicSchedules;
use App\Models\time_calendar;
use App\Models\clinic_schedule_timeclass;

class TimeClassController extends Controller
{
    public function __construct(
        TimeClassService $timeClassService,
        ClinicSchedules $clinicSchedules
      ){
        $this->timeClassService = $timeClassService; 
        $this->clinicSchedules = $clinicSchedules;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kw = (int)$request->keyword;
        $timeclass = $this->timeClassService->show();
        $clinicSchedule = $this->clinicSchedules->show($kw);
        foreach ($clinicSchedule as $key => $value) {
            $data = $this->clinicSchedules->findClinicScheduleTimeByid($value->id);
            $deta = $this->clinicSchedules->findClinicScheduleTimeBooking($value->id);
            $array = array();
            $brray = array();
            foreach ($data as $key => $i) {
               $array[] = $i->time->name;
            }
            foreach ($deta as $key => $i) {
                $brray[] = $i->time->name;
            }
            $value->time_name = implode(" ,", $array);
            $value->booking = implode(" ,",$brray);
            
            $value->count = $data->count();
        }
        return view('backend.timeclass.index', compact('timeclass','clinicSchedule'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
