<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Services\PostsService;
use App\Models\posts;
use App\Http\Requests\AddPostRequest;
use App\Http\Requests\EditPostRequest;
use Session;

class PostController extends Controller
{
    public function __construct(
        PostsService $postsService
      ){
        $this->postsService = $postsService; 
    }

    public function index(Request $request)
    {
        $kw = $request->keyword;
        $listPost = $this->postsService->show($kw);
        return view('backend.post.index', compact('listPost'));
    }

    public function destroy($id){
		$data = posts::find($id);
        $data->is_delete = 1;
        $data->save();
        return back();
    }
    public function create(Request $request){
        return view ('backend.post.create');
    }
    public function store(AddPostRequest $request)
    {
         $posts = new posts();
         $posts->fill($request->all());
         if(!empty($posts->feature_image)){
            $filename = $posts->feature_image->getClientOriginalName();
            $filename = str_replace(' ', '-', $filename);
            $filename = uniqid() . '-' . $filename;
            $path = $posts->feature_image->storeAs('posts', $filename);
            $posts->feature_image = "images/$path";
          }
         $posts->save();
         Session::flash('success',' Thêm mới bài viết thành công !');
         return redirect()->route('post');
    }
    public function edit($id)
    {
        $model = posts::find($id);
        return view('backend.post.update' ,compact('model'));
    }
    public function update(EditPostRequest $request, $id){
        $img = $request->feature_image;
        $data = $this->postsService->find($id);
        $data->fill($request->all());
        $check = $this->postsService->update($data,$img);
        Session::flash('success',' Sửa bài viết thành công !');
        return redirect()->route('post');
    }

}