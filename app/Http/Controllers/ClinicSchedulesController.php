<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\TimeClassService;
use App\Http\Services\ClinicSchedules;
use App\Models\time_calendar;
use App\Models\ClinicSchedule;
use App\Models\clinic_schedule_timeclass;
use App\Http\Requests\AddClinicScheduleRequest;
use Session;
use Carbon\Carbon;

class ClinicSchedulesController extends Controller
{
    public function __construct(
        TimeClassService $timeClassService,
        ClinicSchedules $clinicSchedules
      ){
        $this->timeClassService = $timeClassService;
        $this->clinicSchedules = $clinicSchedules;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $timeclass = $this->timeClassService->getAll();
        return view('backend.clincSchedules.create',compact('timeclass'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddClinicScheduleRequest $request)
    {
        $data = new ClinicSchedule;
        $data->fill($request->all());
        $data->name = Carbon::parse($request->day)->format('d-m-Y');
        $check = $this->clinicSchedules->store($data);
        if ($check== true) {
            $newId = $this->clinicSchedules->findnew_id();
            $timeclass = $this->timeClassService->getAll();
            foreach ($timeclass as $key => $value) {
                $checkbox = $value->id;
                if ($request->$checkbox == 1) {
                   $data2 = new clinic_schedule_timeclass;
                   $data2->clinic_schedule_id = $newId->id;
                   $data2->timeclass_id = $value->id;
                   $this->clinicSchedules->store($data2);
                }
            }
            Session::flash('success',' Thêm mới thành công !');
            return redirect()->route('timeclass');
        }else{
            Session::flash('erroisMess','Thêm mới thất bại =((');
            return redirect()->route('clinicschedules.create');
        }
       
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function apiv1(Request $request)
    {
        $data = $this->clinicSchedules->get_clinicSchedules_class_byID($request->id);
        foreach ($data as $key => $value) {
            $value->name = $value->time->name;
        }
        return Response()->json($data);
    }
}
