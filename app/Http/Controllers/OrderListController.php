<?php

namespace App\Http\Controllers;

use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\CustomerService;
use App\Http\Services\UserService;
use App\Http\Services\OrderService;
use App\Http\Services\ServicesService;
use App\Http\Services\ClinicSchedules;
use App\Http\Services\HistoriesService;
use App\Http\Services\TimeClassService;

use App\Models\customer;
use App\Models\User;
use App\Models\order_list;
use App\Models\ClinicSchedule;
use App\Models\clinic_schedule_timeclass;
use App\Http\Requests\AddOderRequest;
use Illuminate\Support\Facades\Auth;
use Session;

class OrderListController extends Controller
{   
    public function __construct(
        CustomerService $customerService,
        UserService $userService,
        OrderService $orderService,
        ServicesService $servicesService,
        ClinicSchedules $clinicSchedules,
        HistoriesService $historiesService,
        TimeClassService $timeClassService
      ){
        $this->customerService = $customerService;
        $this->userService = $userService;
        $this->orderService = $orderService;
        $this->servicesService = $servicesService;
        $this->clinicSchedules = $clinicSchedules;
        $this->historiesService = $historiesService;
        $this->timeClassService = $timeClassService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kw = $request->keyword;
        $day = (int)$request->day;
        $orders = $this->orderService->show_order($kw,$day);
        $total = $this->orderService->getAll_order()->count();
        return view('backend.order.index', compact('orders','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $customer = $this->customerService->getAll();
        $service = $this->servicesService->getAll();
        $schedule = $this->clinicSchedules->get_7day();
        return view('backend.order.create',compact('service','schedule','customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post(AddOderRequest $request){
        $data = $this->clinicSchedules->findclass_id($request->time);
        $data->is_booking = 1;
        $check = $this->clinicSchedules->store($data);
        if ($check == true){
            $user = new user;
            $user->fill($request->all());
            $check2 = $this->userService->store($user);
            if ($check2 == true){
                $customer = new customer;
                $customer->fill($request->all());
                $customer->user_id = $this->userService->findnew_id()->id;
                $check3 = $this->customerService->store($customer);
                if ($check3 == true) {
                    $order = new order_list;
                    $order->fill($request->all());
                    $order->time_id = $data->timeclass_id;
                    $order->customer_id = $this->customerService->findnew_id()->id;
                    $check4 = $this->orderService->store($order);
                    if ($check4 == true) {
                        Session::flash('success',' Đặt lịch thành công !');
                        return redirect()->route('order');
                    }else {
                        Session::flash('erroisMess','Đặt lịch thất bại =((');
                        return redirect()->route('order.create');
                    }
                }
    
            }     
        }
        Session::flash('erroisMess','Đặt lịch thất bại =((');
        return redirect()->route('order.create');
    }

    public function postWCustomer(Request $request){
        $data = $this->clinicSchedules->findclass_id($request->time);
        $data->is_booking = 1;
        $check = $this->clinicSchedules->store($data);
        if ($check == true){
            $order = new order_list;
            $order->fill($request->all());
            $order->time_id = $data->timeclass_id;
            $order->customer_id = $request->customer_id;
            $check2 = $this->orderService->store($order);
            if ($check2 == true) {
                Session::flash('success',' Đặt lịch thành công !');
                return redirect()->route('order');
            }else {
                Session::flash('erroisMess','Đặt lịch thất bại =((');
                return redirect()->route('order.create');
            }
        }
    }
            

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $day = $request->day;
        $calendar = $this->orderService->show_calendar_day($day);
        foreach ($calendar as $key => $value) {
            $data = $this->historiesService ->findByCustomerId($value->customer_id);
            $value->histories = $data;
        }
        $calendar7day = $this->orderService->show_calendar7day();
        $schedules = $this->clinicSchedules->get_7day();
        $timeclass = $this->timeClassService->getAll();
        return view('backend.calendar.index', compact('calendar','day','calendar7day','schedules','timeclass'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status($id){
        $data = $this->orderService->find($id);
        $data->status = 1;
        $check = $this->orderService->store($data);
        if ($check == true) {
            Session::flash('success',' Đã xác nhận thành công !');
            return redirect()->route('order');
        }else {
            Session::flash('erroisMess','Xác nhận thất bại =((');
            return redirect()->route('order');
        }
    }

    public function destroyOrder($id){
        $data = $this->orderService->find($id);
        $data->status = 4;
        $check = $this->orderService->store($data);
        if($check == true){
            $data2 = $this->clinicSchedules->unBooking($data->time_id,$data->clinic_schedule_id);
            $data2->is_booking = 0;
            $checkbooking = $this->clinicSchedules->store($data2);
            if ($checkbooking == true) {
                Session::flash('success',' Đã hủy lịch thành công !');
                return redirect()->route('order');
            }else {
                Session::flash('erroisMess','Hủy lịch thất bại =((');
                return redirect()->route('order');
            }
        }
    }

    public function destroycalendar($id){
        $data = $this->orderService->find($id);
        $data->status = 4;
        $check = $this->orderService->store($data);
        if($check == true){
            $data2 = $this->clinicSchedules->unBooking($data->time_id,$data->clinic_schedule_id);
            $data2->is_booking = 0;
            $checkbooking = $this->clinicSchedules->store($data2);
            if ($checkbooking == true) {
                Session::flash('success',' Đã hủy lịch thành công !');
                return redirect()->route('calendar');
            }else {
                Session::flash('erroisMess','Hủy lịch thất bại =((');
                return redirect()->route('calendar');
            }
        }
    }

    public function createForm($id){
        $data = $this->servicesService->find($id);
        $schedule = $this->clinicSchedules->get_7day();
        $service = $this->servicesService->find($id);
        return view('frontend.datlich',compact('service','schedule','data'));
    }

    public function saveform(AddOderRequest $request)
    {
        $data = $this->clinicSchedules->findclass_id($request->time);
        $data->is_booking = 1;
        $check = $this->clinicSchedules->store($data);
        if ($check == true){
            $user = new user;
            $user->fill($request->all());
            $check2 = $this->userService->store($user);
            if ($check2 == true){
                $customer = new customer;
                $customer->fill($request->all());
                $customer->user_id = $this->userService->findnew_id()->id;
                $check3 = $this->customerService->store($customer);
                if ($check3 == true) {
                    $order = new order_list;
                    $order->fill($request->all());
                    $order->time_id = $data->timeclass_id;
                    $order->customer_id = $this->customerService->findnew_id()->id;
                    $check4 = $this->orderService->store($order);
                    if ($check4 == true) {
                        Session::flash('success',' Đặt lịch thành công !, bạn có thể xem lại lịch sử sau khi đăng nhập, tên đăng nhập là số điện thoại bạn đăng ký
                         mật khẩu mặc định là "123456" ');
                        return redirect()->route('home');
                    }else {
                        Session::flash('erroisMess','Đặt lịch thất bại =((');
                        return redirect()->route('datlich');
                    }
                }
            }     
        }
        Session::flash('erroisMess','Đặt lịch thất bại =((');
        return redirect()->route('datlich');
    }
    public function saveUserform(Request $request){
        if (Auth::user()->id) {
            $customer = $this->customerService->findUser_id(Auth::user()->id);
            if ($customer !== null) {
            $data = $this->clinicSchedules->findclass_id($request->time);
            $data->is_booking = 1;
            $this->clinicSchedules->store($data);
            $order = new order_list;
            $order->fill($request->all());
            $order->time_id = $data->timeclass_id;
            $order->customer_id = $customer->id;
            $check4 = $this->orderService->store($order);
            if ($check4 == true){
                Session::flash('success',' Đặt lịch thành công !, bạn có thể xem lại lịch sử sau khi đăng nhập');
                return redirect()->route('home');
                }else{
                    Session::flash('erroisMess','Đặt lịch thất bại =((');
                    return redirect()->route('datlich');
                }
            }else{
                Session::flash('erroisMess','Tài khoản không hợp lệ =((');
                return redirect()->route('datlich');
            }
        }
        Session::flash('erroisMess','Tài khoản không hợp lệ , Hãy đăng nhập lại');
        return redirect()->route('datlich');
    }
}
