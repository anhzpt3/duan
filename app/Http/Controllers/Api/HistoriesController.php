<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\OrderService;
use App\Http\Services\HistoriesService;
use App\Http\Services\CustomerService;
use App\Models\histories;
use App\Models\order_list;




class HistoriesController extends Controller
{
    public function __construct(
        CustomerService $customerService,
        OrderService $orderService

      ){
        $this->customerService = $customerService;
        $this->orderService = $orderService;
        $this->historiesService = $historiesService;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $order = $this->orderService->find($id);
        $data = new histories;
        $data->message = $request->message;
        $data->order_list_id = $id;
        $data->customer_id = $order->customer_id;
        $check = $this->historiesService->store($data);
        if ($check == true && $request->name) {
            $newHistori = $this->historiesService->findnew_id();
            $incurred = new incurred;
            $incurred->fill($request->all());
            $incurred->histories_id = $newHistori->id;
            $this->incurredService->store($incurred);
        }
        if ($check == true) {
            $order->status = 2;
            $check2 = $this->orderService->store($order);
            return Response()->json($check2);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
