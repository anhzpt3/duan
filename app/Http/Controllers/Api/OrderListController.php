<?php

namespace App\Http\Controllers\Api;

use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\CustomerService;
use App\Http\Services\UserService;
use App\Http\Services\OrderService;
use App\Models\customer;
use App\Models\User;
use App\Models\order_list;
use App\Models\ClinicSchedule;

class OrderListController extends Controller
{
    use ResponseTrait;

    protected $orderModel;

    public function __construct(
        CustomerService $customerService,
        UserService $userService,
        order_list $orderModel,
        OrderService $orderService
      ){
        $this->customerService = $customerService;
        $this->userService = $userService;
        $this->orderModel = $orderModel;
        $this->orderService = $orderService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = $this->orderModel->newQuery()->with(["time", "customer", "schedule" => function($q) {
            $q->with("service");
        }])->get();
        return $this->setResponse($services);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $kw = null;
        $data = $this->orderService->show_calendar($kw);
        foreach($data as $i){
            $i->time_id = $i->time->name;
            $i->customer_id = $i->customer->name;
            $i->clinic_schedule_id = $i->schedule->day;
            $i->service_id = $i->schedule->service->name;
            //$i->service_id = $i->service->name;
            unset($i->time);
            unset($i->customer);
            unset($i->schedule);
            unset($i->service);
        }
        return Response()->json(array('data' => $data));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status($id){
        $data = $this->orderService->find($id);
        $data->status = 1;
        $check = $this->orderService->store($data);
        return Response()->json($check);
    }
}
