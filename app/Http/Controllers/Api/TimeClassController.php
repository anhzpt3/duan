<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\TimeClassService;
use App\Models\time_calendar;

class TimeClassController extends Controller
{
    public function __construct(
        TimeClassService $timeClassService
      ){
        $this->timeClassService = $timeClassService; 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kw = $request->keyword;
        $data = $this->timeClassService->show($kw);
        return Response()->json(array('data' => $data));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new time_calendar;
        $data->fill($request->all());
        $check = $this->timeClassService->store($data);
        return Response()->json($check);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->timeClassService->find($id);
        $data->fill($request->all());
        $check = $this->timeClassService->store($data);
        return Response()->json($check);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check = $this->timeClassService->destroy($id);
        return Response()->json($check);
    }
}
