<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\UserService;
use App\Models\User;
use App\Http\Requests\AddUserRequest;
use Illuminate\Support\Facades\Auth;
use Hash;
use Session;


class UserController extends Controller
{
    public function __construct(
        UserService $userService
      ){
        $this->userService = $userService; 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kw = $request->keyword;
        $users = $this->userService->show($kw);
        $total = $this->userService->getAll()->count();
        return view('backend.user.index', compact('users','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddUserRequest $request)
    {
          $user = new User();
          $user->fill($request->all());
          $user->password = Hash::make($request->all()['password']);

            $user->save();
            Session::flash('success',' Thêm mới tài khoản thành công !');
         return redirect()->route('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeP(){
        return view('backend.page.repass');
    }
    public function ChangePassSave(Request $request){
        $user = $this->userService->find(Auth::user()->id);
        $user->fill($request->all());
        if(Hash::check($request->oldpass,Auth::user()->password)) {
            $check = $this->userService->store($user);
            if ($check == true) {
                Session::flash('success',' Đổi mật khẩu thành công !');
                return redirect()->route('logout');
            }else{
                Session::flash('erroisMess','Có lỗi, đổi mật khẩu thất bại');
                return redirect()->route('doimatkhau');
            }
        } else {
            Session::flash('erroisMess','Mật khẩu cũ không chính xác');
            return redirect()->route('doimatkhau');
     }
    }
}
