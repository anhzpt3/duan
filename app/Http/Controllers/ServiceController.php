<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\ServicesService;
use App\Models\services;
use App\Http\Requests\AddServiceRequest;
use App\Http\Requests\EditServiceRequest;
use Session;

class ServiceController extends Controller
{
    public function __construct(
        ServicesService $servicesService
      ){
        $this->servicesService = $servicesService; 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kw = $request->keyword;
        $services = $this->servicesService->show($kw);
        $total = $this->servicesService->getAll()->count();
        return view('backend.service.index', compact('services','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddServiceRequest $request)
    {
        $data = new services;
        $data->fill($request->all());
        $check = $this->servicesService->store($data);
        if ($check == true) {
            Session::flash('success',' Thêm mới dịch vụ thành công !');
            return redirect()->route('service');
        }else{
            Session::flash('erroisMess','Thêm mới thất bại =((');
            return redirect()->route('service.create');
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->servicesService->find($id);
        return view('backend.service.update' ,compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditServiceRequest $request, $id)
    {
        $img = $request->image;
        $data = $this->servicesService->find($id);
        $data->fill($request->all());
        $check = $this->servicesService->update($data,$img);
        if ($check == true) {
            Session::flash('success',' Sửa dịch vụ thành công !');
            return redirect()->route('service');
        }else{
            Session::flash('erroisMess','Thêm mới thất bại =((');
            return redirect()->route('service.edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->servicesService->find($id);
        $data->is_delete = 1;$img = null;
        $check = $this->servicesService->update($data,$img);
        if ($check == true) {
            Session::flash('success',' Xóa dịch vụ thành công !');
            return redirect()->route('service');
        }else{
            Session::flash('erroisMess','Xóa thất bại =((');
            return redirect()->route('service');
        }
    }
}
