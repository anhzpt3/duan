<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    public function phone()
    {
        return 'phone';
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public function loginForm(){
        if(Auth::user()!== null && Auth::user()->role >= 100){
            return redirect()->route('dashboard');
        }elseif (Auth::user()!== null && Auth::user()->role < 100) {
            Session::flash('success', ' Bạn đã Đăng nhập bằng tài khoản khách hàng');
            return redirect()->route('home');
        }
        return view('frontend.Login');
    }

    public function postLogin(Request $request){
        if(Auth::attempt(['phone' => $request->phone, 
                          'password' => $request->password,
                          'IS_DELETE' => 0,
        ])){
            if (Auth::user()->role >= 100 ) {
                return redirect()->route('dashboard');
            }elseif (Auth::user()->role < 100) {
                return redirect()->route('home');
            }
        }else {
            Session::flash('loginFalse', ' wrong username or password');
            return view('frontend.login');
        }
        
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }
}
