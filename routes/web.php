<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// 
    Route::get('/', 'HomeController@index')->name('home'); 
    Route::get('/baiviet', 'PostClientController@index')->name('baiviet');
    Route::post('/baiviet', 'PostClientController@index')->name('baiviet.search');
    Route::get('/intro', 'IntroController@index')->name('intro');
    Route::get('/contact', 'ContactController@index')->name('contact');
    Route::get('/servicenew', 'ServiceNewController@index')->name('servicenew');
    Route::get('/datlich/{id}','OrderListController@createForm')->name('datlich');
    Route::post('/datlich/postpostpo','OrderListController@saveform')->name('datlich.post');
    Route::get('/baiviet/{id}', 'DetailPostController@index')->name('baiviet');
    // login 
    Route::get('/login', 'Auth\LoginController@loginForm')->name('login');
    Route::post('/login','Auth\LoginController@postLogin')->name('post.login');
    Route::get('/logout', function(){ Auth::logout(); return redirect()->route('login');})->name('logout');

Route::group(['middleware' => 'auth'], function() {
    //403
    Route::get('/Historyuser', 'HistoryuserController@index')->name('Historyuser');
    Route::get('/403/forbidden/403', function(){ return view('backend.page.403');})->name('forbidden');
    // datlich user
    Route::post('/datlich/postpostpoxaxa','OrderListController@saveUserform')->name('datlichUser.post');
    // doi mat khau
    Route::get('/doi-mat-khau','HomeController@ChangePass')->name('doimatkhau');
    Route::post('/doi-mat-khau','UserController@ChangePassSave')->name('doimatkhau.post');
});
//end auth

Route::group(['middleware' => 'admin.check','auth'], function() {

    Route::get('/dashboard', 'OrderListController@show')->name('dashboard');
    /// bai viet
    Route::get('/post', 'PostController@index')->name('post');
    Route::get('/post/create', 'PostController@create')->name('post.create');
    Route::post('/post/create', 'PostController@store')->name('post.store');
    Route::get('/post/edit/{id}', 'PostController@edit')->name('post.edit');
    Route::post('/post/edit/{id}', 'PostController@update')->name('post.update');
    Route::get('/post/delete/{id}', 'PostController@destroy')->name('post.destroy');
    // bác sĩ
    Route::get('/doctor', 'DoctorController@index')->name('doctor');
    Route::get('/doctor/create', 'DoctorController@create')->name('doctor.create');// 
    Route::post('/doctor/store','DoctorController@store')->name('doctor.store');
    // khách hàng- bệnh nhân
    Route::get('/customer', 'CustomerController@index')->name('customer');
    Route::get('/customer/create', 'CustomerController@create')->name('customer.create');
    Route::post('/customer/create', 'CustomerController@store')->name('customer.store');
    // tài khoản
    Route::get('/user', 'UserController@index')->name('user');
    Route::get('/user/create', 'UserController@create')->name('user.create');
    Route::post('/user/create', 'UserController@store')->name('user.store');

    Route::get('/user/chanerpass','UserController@changeP')->name('user.pass');
    Route::post('user/chanerpass','UserController@ChangePassSave')->name('user.passpost');
            //x : cần tạo admin
    // dịch vụ
    Route::get('/service', 'ServiceController@index')->name('service');
    Route::get('/service/create', 'ServiceController@create')->name('service.create');
    Route::post('/service/create', 'ServiceController@store')->name('service.store');
    Route::get('/service/edit/{id}', 'ServiceController@edit')->name('service.edit');
    Route::post('/service/edit/{id}', 'ServiceController@update')->name('service.update');
    Route::get('/service/delete/{id}', 'ServiceController@destroy')->name('service.destroy');
    // ca khám timeclass-clinic_schedules
    Route::get('/timeclass', 'TimeClassController@index')->name('timeclass');
    // clinic_schedules
    Route::get('/clinic_schedules/create', 'ClinicSchedulesController@create')->name('clinicschedules.create');
    Route::post('/clinic_schedules/create', 'ClinicSchedulesController@store')->name('clinicschedules.store');
    //danh sách đặt lịch khám admin
    Route::get('/order','OrderListController@index')->name('order');
    Route::get('/order/create','OrderListController@create')->name('order.create');
    Route::post('/order/create','OrderListController@post')->name('order.store');// new customer
    Route::post('/order/createe','OrderListController@postWCustomer')->name('order.storev2'); // old man
    // lịch khám
    Route::get('/calendar','OrderListController@show')->name('calendar');
    Route::get('/calendar/sumbitSta/{id}/tus212121','OrderListController@status')->name('calendar.status');// xác nhận lịch
    Route::get('/calendar/sumbitStatus/323/{id}/delete','OrderListController@destroyOrder')->name('calendar.destroy');//hủy lịch 
    Route::get('/calendar/sumbitStatus/{id}/v2','OrderListController@destroycalendar')->name('calendar.destroyv2');//hủy lịch ở chỗ khác =((
    // hồ sơ
    Route::get('/histories' ,'HistoriesController@index')->name('histories');
    Route::get('/histories/create/{id}' ,'HistoriesController@create')->name('histories.create');
    Route::post('/histories/create/{id}' ,'HistoriesController@store')->name('histories.store');
    Route::get('/histories/chitet/{id}' ,'HistoriesController@edit')->name('histories.edit');
    Route::post('/histories/chitet/{id}' ,'HistoriesController@update')->name('histories.update');
});
//end auth.adminrole

//ajax
route::post('/set_booking','ClinicSchedulesController@apiv1')->name('apiv1');
route::post('/find_history','HistoryuserController@apiv2')->name('apiv2');







