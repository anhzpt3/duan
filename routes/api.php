<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// customer:khach hang ->>>>
Route::post('customer', 'Api\CustomerController@index');
Route::post('customer/store', 'Api\CustomerController@store');// them rai khoan user tu dong pass 123456
Route::post('customer/update/{id}', 'Api\CustomerController@update');// update  email lan sdt ben bang user va customer
Route::post('customer/destroy/{id}', 'Api\CustomerController@destroy');// is_Delete = 1 (bang user va customer)

// account user( ko dc xài cai mục có dấu "x" )
Route::post('user', 'Api\UserController@index');
Route::post('user/store', 'Api\UserController@store'); // x
Route::post('user/update/{id}', 'Api\UserController@update'); // x
Route::post('user/destroy/{id}', 'Api\UserController@destroy');// x

// post
Route::post('post','Api\PostController@index');
Route::post('post/store','Api\PostController@store');// luu tru
Route::post('post/update/{id}','Api\PostController@update');// update
Route::post('post/destroy/{id}','Api\PostController@destroy');// xoa
// baiviet - id 
Route::get('post/{id}','Api\PostController@show');// show id
Route::get('4postnew','Api\PostController@show4post');// 4post

// service
Route::post('service', 'Api\ServiceController@index');
Route::post('service/store', 'Api\ServiceController@store');
Route::post('service/update/{id}', 'Api\ServiceController@update');
Route::post('service/destroy/{id}', 'Api\ServiceController@destroy');

// time-class (khung h dat lich)
Route::get('timecalender', 'Api\TimeClassController@index');
Route::post('timecalender/store', 'Api\TimeClassController@store');
Route::post('timecalender/update/{id}', 'Api\TimeClassController@update');
Route::post('timecalender/destroy/{id}', 'Api\TimeClassController@destroy');

// datlich v1
Route::group([
    'prefix' => 'v1',
    'namespace' => 'Api'
], function () {
    Route::get('clinic-schedules', 'PublicController@onGetListClinicSchedules');
    Route::post('create-orders', 'PublicController@onCreateCustomer');
    Route::get('list-services', 'PublicController@onGetListServices');
    Route::get('orders', 'OrderListController@index');
    //
});

// bacsi
Route::get('doctor', 'Api\DoctorController@index');
Route::post('doctor/store', 'Api\DoctorController@store');// them rai khoan user tu dong pass 123456
Route::post('doctor/update/{id}', 'Api\DoctorController@update');// update  email lan sdt ben bang user va doctor
Route::post('doctor/destroy/{id}', 'Api\DoctorController@destroy');// is_Delete = 1 (bang user va doctor)

// dat lich
Route::get('orders-update-status/{id}','Api\OrderListController@status');//update trang thai sang da xac nhan

// man hinh` danh sach lich kham
Route::post('list-calendar', 'Api\OrderListController@show');
// them hồ sơ khi chuyển trạng thái chờ khám sang đã khám : id- lịch khám
Route::post('history/post/{id}','Api\HistoriesController@store'); // thêm hồ sơ

